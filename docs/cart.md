## Cart

---

### Get Current Cart

- Endpoint : `/api/cart/current-cart`
- HTTP Method : `GET`
- Requires Auth : true
- Request Param : -
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": {
    "user": {
      "id": 1,
      "email": "user@xyresto.com",
      "fullName": "Example User",
      "roles": ["USER"]
    },
    "summary": {
      "totalPrice": 100000,
      "quantity": 10
    },
    "products": [
      {
        "product": {
          "id": 1,
          "name": "Teriyaki Sushi",
          "price": 50000,
          "traits" : ["SPICY", "SWEET", "SALTY"],
          "description": "Sushi termantap sedunia no debat",
          "imageUrl" : "www.google.com",
          "category": {
            "id": 1,
            "name": "Food"
          },
          "recommended" : true,
          "purchaseCount": 1
        },
        "quantity": 1,
        "served" : false
      },
      ...
    ]
  }
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Edit Cart

- Endpoint : `/api/cart/current-cart`
- HTTP Method : `PUT`
- Requires Auth : true
- Request Param : -
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
{
  "productId": 1,
  "quantity": 1
}
```

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": {
    "user": {
      "id": 1,
      "email": "user@xyresto.com",
      "fullName": "Example User",
      "roles": ["USER"]
    },
    "summary": {
      "totalPrice": 100000,
      "quantity": 10
    },
    "products": [
      {
        "product": {
          "id": 1,
          "name": "Teriyaki Sushi",
          "price": 50000,
          "traits" : ["SPICY", "SWEET", "SALTY"],
          "description": "Sushi termantap sedunia no debat",
          "imageUrl" : "www.google.com",
          "category": {
            "id": 1,
            "name": "Food"
          },
          "recommended" : true,
          "purchaseCount": 1
        },
        "quantity": 1,
        "served" : false
      },
      ...
    ]
  }
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```
