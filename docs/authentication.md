## Authentication

---

### Login

- Endpoint : `/api/user/_login`
- HTTP Method : `POST`
- Requires Auth : false
- Request Param : -
- Path Variable : -
- Response Header :
  - Set-Cookie : `xy_user_id=user_id`
  - Set-Cookie : `xy_session_id=session`

#### Request Body

```json
{
  "email": "user@xyresto.com",
  "password": "examplePassword"
}
```

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": {
    "token": "[JWT token]",
    "email": "user@xyresto.com",
    "fullName": "Example User",
    "roles": ["USER", "ADMIN", ...]
  }
}
```

---

### Register

- Endpoint : `/api/user/_register`
- HTTP Method : `POST`
- Requires Auth : false
- Request Param : -
- Path Variable : -

#### Request Body

```json
{
  "email": "user@xyresto.com",
  "password": "examplePassword",
  "fullName": "Example User"
}
```

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": {
    "id": 1,
    "email": "user@xyresto.com",
    "fullName": "Example User",
    "roles": ["USER"]
  }
}
```

#### Error Response Body

```json
{
  "code": 400,
  "status": "Bad Request",
  "errors": [
    ...
  ]
}
```

---

### Logout

- Endpoint : `/api/user/_logout`
- HTTP Method : `POST`
- Requires Auth : true
- Request Param : -
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": null
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Change Password

- Endpoint : `/api/user/_change-password`
- HTTP Method : `POST`
- Requires Auth : true
- Request Param : -
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
{
  "oldPassword": "oldPassword",
  "newPassword": "newPassword"
}
```

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": null
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "error": [
    ...
  ]
}
```
