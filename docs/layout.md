## Layout

---

### Get Layout

- Endpoint : `/api/layout/tables`
- HTTP Method : `GET`
- Request Param :
  - `timestamp={date in epoch millis}`
- Path Variable : -

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": [
    {
      "table": {
        "tableId": 1,
        "x": 3,
        "y": 4
      },
      "booked" : true
    },
    ...
  ]
}

```

#### Error Response Body

```json
{
  "code": 400,
  "status": "Bad Request",
  "errors": [
    ...
  ]
}
```

---

---

### Get Layout (ADMIN)

- Endpoint : `/api/admin/layout/tables`
- HTTP Method : `GET`
- Requires Auth : true
- Request Param : -
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": [
    {
      "table": {
        "tableId": 1,
        "x": 3,
        "y": 4
      },
      "booked" : true
    },
    ...
  ]
}
```

#### Error Response Body

```json
{
  "code": 400,
  "status": "Bad Request",
  "errors": [
    ...
  ]
}
```

---


### Edit Layout

- Endpoint : `/api/admin/layout/tables`
- HTTP Method : `PUT`
- Requires Auth : true
- Request Param : -
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
[
  {
    "tableId": 1,
    "x": 4,
    "y": 5
  },
  ...
]
```

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": [
    {
      "tableId": 1,
      "x": 4,
      "y": 5
    },
    ...
  ]
}

```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```