## Reservation

---

### Get User Reservation

- Endpoint : `/api/reservation/reservations`
- HTTP Method : `GET`
- Requires Auth: true
- Request Param : -
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": [
    {
      "id": 1,
      "timestamp": 1616317415,
      "tableId": 10,
      "status": "PENDING",
      "email" : "admin@gmail.com",
      "fullName" : "Admin",
      "orders" : [
        {
          "product": {
            "id": 1,
            "name": "Teriyaki Sushi",
            "price": 50000,
            "traits" : ["SPICY", "SWEET", "SALTY"],
            "description": "Sushi termantap sedunia no debat",
            "imageUrl" : "www.google.com",
            "category": {
              "id": 1,
              "name": "Food"
              },
            "recommended" : true,
            "purchaseCount": 1
            },
          "quantity": 1,
          "served" : false
        },
        ...
      ]
    },
    ...
  ]
}

```

#### Error Response Body

```json
{
  "code": 403,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Get All Reservations (Admin)

- Endpoint : `/api/admin/reservation/reservations`
- HTTP Method : `GET`
- Requires Auth: true
- Request Param :
  - `status={status}`
  - `timestamp={date in epoch millis}`
  - `page={page}`
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": [
    {
      "id": 1,
      "timestamp": 1616317415,
      "tableId": 10,
      "status": "PENDING",
      "email" : "admin@gmail.com",
      "fullName" : "Admin",
      "orders" : [
        {
          "product": {
            "id": 1,
            "name": "Teriyaki Sushi",
            "price": 50000,
            "traits" : ["SPICY", "SWEET", "SALTY"],
            "description": "Sushi termantap sedunia no debat",
            "imageUrl" : "www.google.com",
            "category": {
              "id": 1,
              "name": "Food"
            },
            "recommended" : true,
            "purchaseCount": 1
          },
          "quantity": 1,
          "served" : false
        },
        ...
      ]
    },
    ...
  ],
  "pagination":{
    "currentPage":2,
    "itemsPerPage": 10,
    "totalItems": 12
  }
}

```

#### Error Response Body

```json
{
  "code": 403,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Get Reservation Details

- Endpoint : `/api/reservation/reservations/{id}`
- HTTP Method : `GET`
- Requires Auth: true
- Request Param : -
- Path Variable :
  - `{id}`
- Request Header :
  - Authorization : `Bearer [token]`

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": {
    "id": 1,
    "timestamp": 1616317415,
    "tableId": 10,
    "status": "PENDING",
    "email" : "admin@gmail.com",
    "fullName" : "Admin",
    "orders" : [
      {
        "product": {
          "id": 1,
          "name": "Teriyaki Sushi",
          "price": 50000,
          "traits" : ["SPICY", "SWEET", "SALTY"],
          "description": "Sushi termantap sedunia no debat",
          "imageUrl" : "www.google.com",
          "category": {
            "id": 1,
            "name": "Food"
          },
          "recommended" : true,
          "purchaseCount": 1
        },
        "quantity": 1,
        "served" : false
      },
      ...
    ]
  }
}
```

#### Error Response Body

```json
{
  "code": 403,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Create Reservation

- Endpoint : `/api/reservation/reservations`
- HTTP Method : `POST`
- Requires Auth: true
- Request Param : -
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
{
  "timestamp": 1616317415,
  "tableId": 10,
  "orders": [
    {
      "productId": 1,
      "quantity": 1
    },
    ...
  ]
}
```

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": {
    "id": 1,
    "timestamp": 1616317415,
    "tableId": 10,
    "status": "PENDING",
    "email" : "admin@gmail.com",
    "fullName" : "Admin",
    "orders" : [
      {
        "product": {
          "id": 1,
          "name": "Teriyaki Sushi",
          "price": 50000,
          "traits" : ["SPICY", "SWEET", "SALTY"],
          "description": "Sushi termantap sedunia no debat",
          "imageUrl" : "www.google.com",
          "category": {
            "id": 1,
            "name": "Food"
          },
          "recommended" : true,
          "purchaseCount": 1
        },
        "quantity": 1,
        "served" : false
      },
      ...
    ]
  }
}
```

#### Error Response Body

```json
{
  "code": 403,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Edit Reservation Status (Admin)

- Endpoint : `/api/admin/reservation/reservations/{id}`
- HTTP Method : `PUT`
- Requires Auth: true
- Request Param : -
- Path Variable :
  - `{id}`
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
{
  "status": "APPROVED"
}
```

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": null
}
```

#### Error Response Body

```json
{
  "code": 403,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

### Cancel Reservation (User)

- Endpoint : `/api/reservation/reservations/{id}`
- HTTP Method : `PUT`
- Requires Auth: true
- Request Param : -
- Path Variable :
  - `{id}`
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
{
  "canceled": true
}
```

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": null
}
```

#### Error Response Body

```json
{
  "code": 403,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Add Order To Reservation

- Endpoint : `/api/reservation/reservations/{reservation id}/orders`
- HTTP Method : `PUT`
- Requires Auth: true
- Request Param : -
- Path Variable : `{reservation id}`
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
[
  {
    "productId": 1,
    "quantity": 1
  },
  ...
]

```

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": {
    "id": 1,
    "timestamp": 1616317415,
    "tableId": 10,
    "status": "dining",
    "email" : "admin@gmail.com",
    "fullName" : "Admin",
    "orders" : [
      {
        "product": {
          "id": 1,
          "name": "Teriyaki Sushi",
          "price": 50000,
          "traits" : ["SPICY", "SWEET", "SALTY"],
          "description": "Sushi termantap sedunia no debat",
          "imageUrl" : "www.google.com",
          "recommended" : true,
          "category": {
            "id": 1,
            "name": "Food"
            },
          "purchaseCount": 1
          },
        "quantity": 1,
        "served" : false
      },
      ...
    ]
  }
}

```

#### Error Response Body

```json
{
  "code": 403,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Edit Orders Status Reservation

- Endpoint : `/api/admin/reservation/reservations/{reservation id}/orders`
- HTTP Method : `PUT`
- Requires Auth: true
- Request Param : -
- Path Variable : `{reservation id}`
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
{
  "productIds": [2, 4, 5, ...]
}
```

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": null
}
```

#### Error Response Body

```json
{
  "code": 403,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```
