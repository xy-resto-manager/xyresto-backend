## Category

---

### Get All Categories

- Endpoint : `/api/product/categories`
- HTTP Method : `GET`
- Request Param : -
- Path Variable : -

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": [
    {
      "id": 1,
      "name": "Food"
    },
    ...
   ]
}
```

#### Error Response Body

```json
{
  "code": 400,
  "status": "Bad Request",
  "errors": [
    ...
  ]
}
```

---

### Create Category

- Endpoint : `/api/admin/product/categories`
- HTTP Method : `POST`
- Requires Auth : true
- Request Param : -
- Path Variable : -
- Request Header :
  - Accept : `application/json`
  - Authorization : `Bearer [token]`

#### Request Body

```json
{
  "name": "Food"
}
```

#### Success Response Body

```json
{
  "code": 201,
  "status": "Created",
  "data": {
    "id": 1,
    "name": "Food"
  }
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Edit Category

- Endpoint : `/api/admin/product/categories/{id}`
- HTTP Method : `PUT`
- Requires Auth : true
- Request Param : -
- Path Variable :
  - `{id}`
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
{
  "name": "[category name]"
}
```

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": {
    "id": 1,
    "name": "Food"
  }
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized"
}
```

---

### Delete Category

- Endpoint : `/api/admin/product/categories/{id}`
- HTTP Method : `DELETE`
- Requires Auth : true
- Request Param : -
- Path Variable :
  - `{id}`
- Request Header :
  - Accept : `application/json`
  - Authorization : `Bearer [token]`

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": null
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```
