## Banner

---

### Get All Banners

- Endpoint : `/api/banner/banners`
- HTTP Method : `GET`
- Request Param : -
- Path Variable : -

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": [
    {
      "id": 1,
      "imageUrl": "/files/banners/1"
    },
    {
      "id": 3,
      "imageUrl": "/files/banners/3"
    },
    ...
  ]
}
```

#### Error Response Body

```json
{
  "code": 400,
  "status": "Bad Request",
  "errors": [
    ...
  ]
}
```

---

### Get Banner

- Endpoint : `/api/banner/{banner id}`
- HTTP Method : `GET`
- Request Param : -
- Path Variable :
  - `{banner id}`

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": {
    "id": 1,
    "imageUrl": "/files/banners/1"
  }
}
```

#### Error Response Body

```json
{
  "code": 400,
  "status": "Bad Request",
  "errors": [
    ...
  ]
}
```

---

### Create Banner

- Endpoint : `/api/admin/banner/banners`
- HTTP Method : `POST`
- Request Param : -
- Path Variable : -

#### Request Body

*multipart/form-data*

| Key   | Value          |
| ------|----------------|
| file  | {banner image} |

#### Success Response Body

```json
{
  "code": 201,
  "status": "Created",
  "data": {
    "id": 1,
    "imageUrl": "/files/banners/1"
  }
}
```

#### Error Response Body

```json
{
  "code": 400,
  "status": "Bad Request",
  "errors": [
    ...
  ]
}
```

---

### Delete Banner

- Endpoint : `/api/admin/banner/{bannerId}`
- HTTP Method : `DELETE`
- Request Param : -
- Path Variable :
  - `{banner id}`

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": null
}
```

#### Error Response Body

```json
{
  "code": 400,
  "status": "Bad Request",
  "errors": [
    ...
  ]
}
```