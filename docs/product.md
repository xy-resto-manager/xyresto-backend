## product

---

### Get products

- Endpoint : `/api/product/products`
- HTTP Method : `GET`
- Requires Auth : false
- Request Param :
  - `page={page}`
  - `sort=popular/recommended/{categoryName}`
  - `query={query}`
- Path Variable : -

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": [
    {
      "id": 1,
      "name": "Teriyaki Sushi",
      "price": 50000,
      "traits" : ["SPICY", "SWEET", "SALTY"],
      "description": "A Sushi that blows ur mind s3 marketing",
      "imageUrl": "[image url]",
      "category": {
          "id" : 1,
          "name" : "Food"
      },
      "recommended": true,
      "purchaseCount" : 1
    },
    ...
  ],
  "pagination":{
    "currentPage":2,
    "itemsPerPage": 10,
    "totalItems": 12
  }
}
```

#### Error Response Body

```json
{
  "code": 400,
  "status": "Bad Request",
  "errors": [
    ...
  ]
}
```

---

### Get product Detail

- Endpoint : `/api/product/{id}`
- HTTP Method : `GET`
- Request Param : -
- Path Variable :
  - `{id}`

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": {
    "id": 1,
    "name": "Teriyaki Sushi",
    "price": 50000,
    "traits": ["spicy", "sweet", "salty"],
    "description": "A Sushi that blows ur mind s3 marketing ",
    "imageUrl": "[image url]",
    "category": {
      "id": 1,
      "name": "Food"
    },
    "recommended": true,
    "purchaseCount": 1
  }
}
```

#### Error Response Body

```json
{
  "code": 400,
  "status": "Bad Request",
  "errors": [
    ...
  ]
}
```

---

### Upload product Image

- Endpoint : `/api/admin/product/{id}/image`
- HTTP Method : `POST`
- Requires Auth: true
- Request Param : -
- Path Variable :
  - `{id}`
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

_multipart/form-data_

| Key  | Value           |
| ---- | --------------- |
| file | {product image} |

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": {
    "id": 1,
    "name": "Teriyaki Sushi",
    "price": 50000,
    "traits": ["spicy", "sweet", "salty"],
    "description": "A Sushi that blows ur mind s3 marketing ",
    "imageUrl": "[image url]",
    "category": {
      "id": 1,
      "name": "Food"
    },
    "recommended": true,
    "purchaseCount": 1
  }
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Add product

- Endpoint : `/api/admin/product/products`
- HTTP Method : `POST`
- Requires Auth: true
- Request Param : -
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
{
  "name": "Teriyaki Sushi",
  "price": 50000,
  "traits": ["spicy", "sweet", "salty"],
  "description": "A Sushi that blows ur mind s3 marketing ",
  "category": 1,
  "recommended": true
}
```

#### Success Response Body

```json
{
  "code": 201,
  "status": "Created",
  "data": {
    "id": 1,
    "name": "Teriyaki Sushi",
    "price": 50000,
    "traits": ["spicy", "sweet", "salty"],
    "description": "A Sushi that blows ur mind s3 marketing ",
    "imageUrl": null,
    "category": {
      "id": 1,
      "name": "Food"
    },
    "recommended": true,
    "purchaseCount": 0
  }
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Edit product

- Endpoint : `/api/admin/product/{id}`
- HTTP Method : `PUT`
- Requires Auth: true
- Request Param : -
- Path Variable :
  - `{id}`
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
{
  "name": "Teriyaki Sushi",
  "price": 50000,
  "traits": ["spicy", "sweet", "salty"],
  "description": "A Sushi that blows ur mind s3 marketing ",
  "category": 1,
  "recommended": true
}
```

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": {
    "id": 1,
    "name": "Teriyaki Sushi",
    "price": 50000,
    "traits": ["spicy", "sweet", "salty"],
    "description": "A Sushi that blows ur mind s3 marketing ",
    "imageUrl": "[image url]",
    "category": {
      "id": 1,
      "name": "Food"
    },
    "recommended": true,
    "purchaseCount": 1
  }
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Delete product

- Endpoint : `/api/admin/product/{id}`
- HTTP Method : `DELETE`
- Requires Auth: true
- Request Param : -
- Path Variable :
  - `{id}`
- Request Header :
  - Authorization : `Bearer [token]`

#### Success Response Body

```json
{
  "code": 200,
  "status": "OK",
  "data": null
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized"
}
```
