## Report

---

### Get Daily Report

- Endpoint : `/api/admin/report/reports`
- HTTP Method : `GET`
- Request Param :
  - `timestamp={date in epoch millis}`
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": {
    "total": 1000000,
    "reservations": [
      {
        "id": 1,
        "timestamp": 1616317415,
        "tableId": 10,
        "status": "PENDING",
        "email": "[email address]",
        "fullName": "[full name]",
        "orders": [
          {
            "product": {
              "id": 1,
              "name": "Teriyaki Sushi",
              "price": 50000,
              "traits" : ["SPICY", "SWEET", "SALTY"],
              "description": "Sushi termantap sedunia no debat",
              "imageUrl": "[image url]",
              "category": {
                "id": 1,
                "name": "Food"
              },
              "recommended": false,
              "purchaseCount": 1
            },
            "quantity": 1,
            "served": true
          },
          ...
        ]
      },
      ...
    ]
  }
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```
