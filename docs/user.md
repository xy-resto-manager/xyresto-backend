## User

---

### Get Users

- Endpoint : `/api/admin/user/users`
- HTTP Method : `GET`
- Requires Auth: true
- Request Param :
  - `role={role}`
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": [
    {
      "id": 1,
      "email": "user@xyresto.com",
      "fullName": "Example User",
      "roles": ["USER", "ADMIN", ...]
    },
    ...
  ]
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Edit Roles

- Endpoint : `/api/admin/user/users/{user id}/roles`
- HTTP Method : `PUT`
- Requires Auth: true
- Request Param : -
- Path Variable : `{user id}`
- Request Header :
  - Authorization : `Bearer [token]`

#### Request Body

```json
{
  "roles": ["USER", "ADMIN", ...]
}
```

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": {
    "id": 1,
    "email": "user@xyresto.com",
    "fullName": "Example User",
    "roles": ["USER", "ADMIN", ...]
  }
}
```

#### Error Response Body

```json
{
  "code": 401,
  "status": "Unauthorized",
  "errors": [
    ...
  ]
}
```

---

### Get Logged-in User Profile

- Endpoint : `/api/user/current-user`
- HTTP Method : `GET`
- Requires Auth: true
- Request Param : -
- Path Variable : -
- Request Header :
  - Authorization : `Bearer [token]`

#### Success Response Body

```json
{
  "code": 200,
  "status" : "OK",
  "data": {
    "id": 1,
    "email": "user@xyresto.com",
    "fullName": "Example User",
    "roles": ["USER", "ADMIN", ...]
  }
}
```

#### Error Response Body

```json
{
  "code": 400,
  "status": "Bad Request",
  "errors": [
    ...
  ]
}
```
