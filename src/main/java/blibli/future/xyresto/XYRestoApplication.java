package blibli.future.xyresto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XYRestoApplication {

	public static void main(String[] args) {
		SpringApplication.run(XYRestoApplication.class, args);
	}

}
