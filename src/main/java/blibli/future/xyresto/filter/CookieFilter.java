package blibli.future.xyresto.filter;

import blibli.future.xyresto.service.UserDetailsServiceImpl;
import blibli.future.xyresto.util.CookieUtil;
import blibli.future.xyresto.util.JWTUtil;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CookieFilter extends OncePerRequestFilter {

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    JWTUtil jwtUtil;

    @Autowired
    CookieUtil cookieUtil;

    String jwt = null;
    String email = null;
    UserDetails userDetails;

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws ServletException,
            IOException {
        Cookie[] cookies = req.getCookies();
        if(cookies != null){
            for (Cookie ck : cookies) {
                if(cookieUtil.TOKEN_KEY.equals(ck.getName())){
                    jwt = ck.getValue();
                    try{
                        jwtUtil.extractUserName(jwt);
                    }catch (ExpiredJwtException e){
                        cookieUtil.clearCookie(res);
                        email = null;
                        chain.doFilter(req,res);
                    }
                    email = jwtUtil.extractUserName(jwt);
                    if(email != null && SecurityContextHolder.getContext().getAuthentication() == null){
                        try{
                            userDetails = userDetailsService.loadUserByUsername(email);
                        }catch(UsernameNotFoundException e){
                            cookieUtil.clearCookie(res);
                        }
                        if(jwtUtil.validateToken(jwt,userDetails) == Boolean.TRUE) {
                            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                                    userDetails.getUsername(), null, userDetails.getAuthorities()
                            );
                            usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(
                                req));
                            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                        }
                    }
                }

            }
        }
        chain.doFilter(req,res);
    }
}
