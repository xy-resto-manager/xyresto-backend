package blibli.future.xyresto.exception;

public class ExpiredDateException extends BaseException{

    public ExpiredDateException() {
        super("Invalid Date");
    }
}
