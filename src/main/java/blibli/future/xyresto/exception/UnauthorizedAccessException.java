package blibli.future.xyresto.exception;

public class UnauthorizedAccessException extends BaseException {

    public UnauthorizedAccessException() {
        super();
        setMessage("You do not have access to this object.");
    }
}
