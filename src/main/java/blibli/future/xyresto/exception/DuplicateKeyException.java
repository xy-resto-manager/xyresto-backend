package blibli.future.xyresto.exception;

public class DuplicateKeyException extends BaseException{

   public DuplicateKeyException() { super("Duplicate Id Exist"); }

}
