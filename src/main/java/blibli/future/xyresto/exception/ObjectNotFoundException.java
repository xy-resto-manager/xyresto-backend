package blibli.future.xyresto.exception;

public class ObjectNotFoundException extends BaseException {

    public ObjectNotFoundException(Class object) {
        super();
        setMessage(object.getSimpleName() + " not found.");
    }
}
