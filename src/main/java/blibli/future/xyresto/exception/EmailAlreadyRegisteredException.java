package blibli.future.xyresto.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

public class EmailAlreadyRegisteredException extends BaseException {

    public EmailAlreadyRegisteredException() {
        super("Email is already registered. Please log in instead.");
    }
}
