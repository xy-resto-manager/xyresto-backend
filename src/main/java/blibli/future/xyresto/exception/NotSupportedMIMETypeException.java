package blibli.future.xyresto.exception;

public class NotSupportedMIMETypeException extends BaseException{

    public NotSupportedMIMETypeException(){
        super("MIME Type Not Supported");
    }

}
