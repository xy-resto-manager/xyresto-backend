package blibli.future.xyresto.exception;

public class IsBookedException extends BaseException{

    public IsBookedException() {
        super("Table is already booked");
    }
}
