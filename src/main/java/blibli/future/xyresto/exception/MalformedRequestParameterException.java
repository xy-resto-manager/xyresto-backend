package blibli.future.xyresto.exception;

public class MalformedRequestParameterException extends BaseException {

    public MalformedRequestParameterException(String requestParameter) {
        super("Request parameter " + requestParameter + "is invalid.");
    }
}
