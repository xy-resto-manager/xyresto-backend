package blibli.future.xyresto.exception;

public class FailedAuthenticationException extends BaseException{

    public FailedAuthenticationException() {
        super("Authenticated Failed");
    }

}
