package blibli.future.xyresto.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class BaseException extends RuntimeException {

    private String error;
    private String message;

    public BaseException(String message) {
        error = getClass().getSimpleName();
        this.message = message;
    }

    public BaseException() {
        error = getClass().getSimpleName();
    }
}
