package blibli.future.xyresto.service;

import blibli.future.xyresto.model.Counter;
import blibli.future.xyresto.repository.CounterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CounterService {

    @Autowired
    private CounterRepository counterRepository;

    public Counter getCounterByEntity(String entity) {
        return counterRepository.findFirstByEntity(entity).orElse(new Counter(entity));
    }

    public long getNextId(String entity) {
        return getCounterByEntity(entity).getNextId();
    }

    public void incrementId(String entity) {
        Counter counter = getCounterByEntity(entity);
        counter.setNextId(counter.getNextId() + 1);
        counterRepository.save(counter);
    }

    public long getNextIdAndIncrement(String entity) {
        long nextId = getCounterByEntity(entity).getNextId();
        incrementId(entity);
        return nextId;
    }
}
