package blibli.future.xyresto.service;

import blibli.future.xyresto.dto.OrderedProduct;
import blibli.future.xyresto.dto.request.OrderRequest;
import blibli.future.xyresto.dto.response.CartResponse;
import blibli.future.xyresto.dto.response.OrderedProductResponse;
import blibli.future.xyresto.dto.response.ProductResponse;
import blibli.future.xyresto.dto.response.UserResponse;
import blibli.future.xyresto.exception.ObjectNotFoundException;
import blibli.future.xyresto.model.Cart;
import blibli.future.xyresto.model.Product;
import blibli.future.xyresto.model.User;
import blibli.future.xyresto.repository.CartRepository;
import blibli.future.xyresto.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Optionals;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class CartService {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductRepository productRepository;

    public CartResponse getUserCart(User user) {
        Cart cart = getOrCreateCart(user);

        return CartResponse.builder()
                .user(new UserResponse(user))
                .summary(new CartResponse.SummaryResponse(cart.getSummary()))
                .products(toOrderedProductResponseList(cart.getProducts()))
                .build();
    }

    public CartResponse editCart(User user, OrderRequest orderRequest) throws ObjectNotFoundException {
        Cart cart = getOrCreateCart(user);

        if (cart.getProducts().isEmpty()) {
            cart.setProducts(new ArrayList<>());
        }

        Optional<OrderedProduct> matchedProduct = cart.getProducts().stream().filter(orderedProduct -> orderedProduct.getProduct().getProductId() == orderRequest.getProductId()).findFirst();
        Optionals.ifPresentOrElse(matchedProduct,
                orderedProduct -> {
                    if (orderRequest.getQuantity() > 0) {
                        orderedProduct.setQuantity(orderRequest.getQuantity());
                        cart.getProducts().get(cart.getProducts().indexOf(orderedProduct)).setQuantity(orderRequest.getQuantity());
                    } else {
                        cart.getProducts().remove(orderedProduct);
                    }
                }, () -> {
                    if (orderRequest.getQuantity() > 0) {
                        cart.getProducts().add(OrderedProduct.builder()
                                .product(productRepository.findFirstByProductId(orderRequest.getProductId()).orElseThrow(() -> new ObjectNotFoundException(Product.class)))
                                .quantity(orderRequest.getQuantity())
                                .build());
                    }
                });
        cart.calculateSummary();
        cartRepository.save(cart);

        return CartResponse.builder()
                .user(new UserResponse(user))
                .summary(new CartResponse.SummaryResponse(cart.getSummary()))
                .products(toOrderedProductResponseList(cart.getProducts()))
                .build();
    }


    public void clearCart(User user){
        if(user != null) {
            Cart cart = cartRepository.findFirstByUser(user).orElse(null);
            if (cart != null) {
                cart.setProducts(new ArrayList<>());
                cartRepository.save(cart);
            }
        }
    }

    private Cart getOrCreateCart(User user) {
        Cart cart = cartRepository.findFirstByUser(user).orElse(null);

        if (cart == null) {
            cart = Cart.builder()
                    .user(user)
                    .build();
            cart.calculateSummary();
            cartRepository.save(cart);
        }
        return cart;
    }
    List<OrderedProductResponse> toOrderedProductResponseList (List<OrderedProduct> orderedProducts){

        List<OrderedProductResponse> orderedProductResponses = new ArrayList<>();

        for (OrderedProduct product: orderedProducts) {
            OrderedProductResponse orderedProductResponse = OrderedProductResponse.builder().build();
            orderedProductResponse.setProduct(new ProductResponse(product.getProduct()));
            orderedProductResponse.setQuantity(product.getQuantity());
            orderedProductResponse.setServed(false);
            orderedProductResponses.add(orderedProductResponse);
        }
        return orderedProductResponses;
    }
}
