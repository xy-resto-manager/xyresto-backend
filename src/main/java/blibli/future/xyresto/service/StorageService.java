package blibli.future.xyresto.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class StorageService {

    @Value("${storageLocation:#{null}}")
    private String STORAGE_LOCATION;

    public StorageService() {
        if (STORAGE_LOCATION == null) STORAGE_LOCATION = Paths.get("storage").toAbsolutePath().normalize().toString();
    }

    public String storeFile(MultipartFile file, StorageType type, String fileName) throws IOException {
        Path completePath = Paths.get(STORAGE_LOCATION, type.getLabel(), fileName);
        Files.createDirectories(completePath.getParent());
        Files.copy(file.getInputStream(), completePath, StandardCopyOption.REPLACE_EXISTING);
        return String.format("/files/%s/%s", type.getLabel(), fileName);
    }

    public boolean deleteFile(StorageType type, String fileName) throws IOException {
        Path completePath = Paths.get(STORAGE_LOCATION, type.getLabel(), fileName);
        return Files.deleteIfExists(completePath);
    }

    public enum StorageType {
        BANNER_IMAGE("banners"),
        PRODUCT_IMAGE("products");

        private String label;

        StorageType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }
}
