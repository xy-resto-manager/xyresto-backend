package blibli.future.xyresto.service;

import blibli.future.xyresto.dto.UserDetailsImpl;
import blibli.future.xyresto.model.User;
import blibli.future.xyresto.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;



    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user =
                userRepository.findByEmail(s).orElseThrow(() -> new UsernameNotFoundException("Username not found"));
        // For
        // whatever
        // reason gk
        // bisa @Indexed g bs dayum

        // Mau memasukkan roles kedalam my UserDetails, tpi roles minta
        // supaya dalam bentuk SimpleGrantedAuthorities, jdi harus convert
        // String --> SimpleGranted

        UserDetailsImpl userDetails = UserDetailsImpl
                .builder()
                .username(user.getEmail())
                .password(user.getPassword())
                .roles(stringToRoles(user.getRoles()))
                .active(true)
                .build();

        return userDetails;
    }

    // Ini buat authorities bang
    public Set<SimpleGrantedAuthority> stringToRoles(List<String> stringRoles){

        Set<SimpleGrantedAuthority> roles = stringRoles.stream()
                .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
                .collect(Collectors.toSet());
        return roles;

    }

}
