package blibli.future.xyresto.service;

import blibli.future.xyresto.dto.OrderedProduct;
import blibli.future.xyresto.dto.response.ReportResponse;
import blibli.future.xyresto.dto.response.ReservationResponse;
import blibli.future.xyresto.model.Reservation;
import blibli.future.xyresto.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportService {

    @Value("${timeZone:GMT+7}")
    private String timezone;

    @Autowired
    private ReservationRepository reservationRepository;

    public ReportResponse getDailyReport(Instant timestamp) {
        Instant startDate = timestamp.atZone(ZoneId.of(timezone)).with(ChronoField.MILLI_OF_DAY, 0).toInstant();
        Instant endDate = startDate.plus(1, ChronoUnit.DAYS).minusMillis(1);

        List<Reservation> reservationList = reservationRepository.findAllByTimestampBetweenAndStatus(startDate, endDate, Reservation.Status.FINISHED);
        int total = reservationList.stream().mapToInt(
                reservation -> {
                    if (reservation.getProducts() == null) return 0;
                    return reservation.getProducts().stream().mapToInt(
                            orderedProduct -> orderedProduct.getProduct().getPrice() * orderedProduct.getQuantity()
                    ).sum();
                }
        ).sum();

        return ReportResponse.builder()
                .total(total)
                .reservations(reservationList.stream().map(ReservationResponse::new).collect(Collectors.toList()))
                .build();
    }
}
