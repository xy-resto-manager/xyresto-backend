package blibli.future.xyresto.service;

import blibli.future.xyresto.dto.request.LoginRequest;
import blibli.future.xyresto.dto.request.RegisterRequest;
import blibli.future.xyresto.dto.response.LoginResponse;
import blibli.future.xyresto.dto.response.UserResponse;
import blibli.future.xyresto.exception.EmailAlreadyRegisteredException;
import blibli.future.xyresto.exception.FailedAuthenticationException;
import blibli.future.xyresto.exception.ObjectNotFoundException;
import blibli.future.xyresto.model.User;
import blibli.future.xyresto.repository.UserRepository;
import blibli.future.xyresto.util.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;

@Service
public class UserService {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JWTUtil jwtUtil;
    @Autowired
    UserDetailsServiceImpl userDetailsService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CounterService counterService;
    @Autowired
    PasswordEncoder passwordEncoder;

    public LoginResponse login(LoginRequest loginRequest) throws AuthenticationException{

        String email = loginRequest.getEmail();
        String password = loginRequest.getPassword();
        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email,password));
        }catch(AuthenticationException e){
            throw new FailedAuthenticationException();
        }



        String token =
                jwtUtil.generateToken(userDetailsService.loadUserByUsername(email));

        User user =
                userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Username not found"));

        LoginResponse loginResponse = LoginResponse.builder()
                .email(user.getEmail())
                .fullName(user.getFullName())
                .roles(user.getRoles())
                .token(token)
                .build();

        return loginResponse;
    }

    public UserResponse register(RegisterRequest registerRequest) throws EmailAlreadyRegisteredException {
        String email = registerRequest.getEmail();
        String password = registerRequest.getPassword();
        String fullName = registerRequest.getFullName();

        String encodedPw = passwordEncoder.encode(password);

        User user = User.builder().userId(counterService.getNextId("USER"))
                .fullName(fullName)
                .password(encodedPw)
                .email(email)
                .roles(Arrays.asList("USER"))
                .build();

        try {
            userRepository.save(user);
            counterService.incrementId("USER");
        } catch (DuplicateKeyException e) {
            throw new EmailAlreadyRegisteredException();
        }

        return new UserResponse(user);
    }

    public void changePassword(String oldPassword,String newPassword) throws FailedAuthenticationException{

        String email =
               SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email,
                    oldPassword));
        }catch(AuthenticationException e){
            throw new FailedAuthenticationException();
        }

        User user =
                userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Username not found"));
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);

    }

    public User getUserByEmail(String email) throws ObjectNotFoundException {
        return userRepository.findByEmail(email).orElseThrow(() -> new ObjectNotFoundException(User.class));
    }

    public UserResponse getCurrentUser() throws UsernameNotFoundException {
        String email = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        User user = userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Username not found"));
        return UserResponse.builder()
                .id(user.getUserId())
                .email(user.getEmail())
                .fullName(user.getFullName())
                .roles(new ArrayList<>(user.getRoles()))
                .build();
    }
}
