package blibli.future.xyresto.service;

import blibli.future.xyresto.dto.TableDTO;
import blibli.future.xyresto.dto.response.TableResponse;
import blibli.future.xyresto.exception.DuplicateKeyException;
import blibli.future.xyresto.model.Reservation;
import blibli.future.xyresto.model.Table;
import blibli.future.xyresto.repository.ReservationRepository;
import blibli.future.xyresto.repository.TableRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TableService {

    @Autowired
    TableRepository tableRepository;

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    CounterService counterService;

    public void editLayout(List<TableDTO> newLayout) throws DuplicateKeyException {
        List<Table> newTableList = new ArrayList<>();
        for (TableDTO currTable: newLayout) {
            Table table = toTable(currTable);
            newTableList.add(table);
        }
        List<Long> savedTableIds =
                tableRepository.findAll().stream().map(Table::getTableId).collect(Collectors.toList());
        try{
            for (Table table : newTableList) {
                Table currTable;
                if(savedTableIds.contains(table.getTableId())){
                    currTable = tableRepository.findFirstByTableId(table.getTableId()).orElse(null);
                    currTable.setX(table.getX());
                    currTable.setY(table.getY());
                    savedTableIds.remove(table.getTableId());
                    tableRepository.save(currTable);
                }else{
                    tableRepository.save(table);
                }
            }
            for (Long removed: savedTableIds) {
                Table removedTable = tableRepository.findFirstByTableId(removed).orElse(null);
                tableRepository.delete(removedTable);
                // Decline Reservation which has deleted table
                List<Reservation> toBeCancelledReservations =
                        reservationRepository.findAllByTableTableIdAndStatus(removed, Reservation.Status.PENDING);
                for (Reservation reservation: toBeCancelledReservations) {
                    reservation.setStatus(Reservation.Status.DECLINED);
                }
                reservationRepository.saveAll(toBeCancelledReservations);
            }
        }catch(DataAccessException e){
            throw new DuplicateKeyException();
        }

    }

    public List<TableResponse> getLayoutsForAdmin(){

        List<Table> tableList = tableRepository.findAll();
        List<TableResponse> tableResponses =
                tableList.stream().map(this::toTableResponse).collect(Collectors.toList());

        tableResponses.forEach(this::hasAnyBooking);

        return tableResponses;
    }

    public List<TableResponse> getLayoutsForUser(long timestamp){

        Instant dateHourNext = Instant.ofEpochMilli(timestamp).plus(1, ChronoUnit.HOURS).plusMillis(1);
        Instant dateHourBefore = Instant.ofEpochMilli(timestamp).minus(1, ChronoUnit.HOURS).minusMillis(1);

        List<Reservation> reservations = reservationRepository.findAllByTimestampBetweenAndStatus(dateHourBefore,
                dateHourNext,
                Reservation.Status.APPROVED);

        List<Long> bookedTable = reservations.stream().map(reservation -> reservation.getTable().getTableId()).collect(Collectors.toList());

        List<Table> tableList = tableRepository.findAll();
        List<TableResponse> responseList = new ArrayList<>();

        for (Table table: tableList) {
            TableResponse tableResponse = TableResponse.builder().build();
            tableResponse.setTable(toTableDTO(table));
            tableResponse.setBooked(bookedTable.contains(table.getTableId()));
            responseList.add(tableResponse);
        }
        return responseList;
    }

    private Table toTable(TableDTO dto){
        Table table = Table.builder().build();
        BeanUtils.copyProperties(dto,table);
        return table;
    }
    private TableDTO toTableDTO(Table table){
        TableDTO tableDTO = TableDTO.builder().build();
        BeanUtils.copyProperties(table,tableDTO);
        return tableDTO;
    }

    private TableResponse toTableResponse(Table table){
        TableResponse tableResponse = TableResponse.builder().build();
        tableResponse.setTable(TableDTO.builder().build());
        tableResponse.getTable().setX(table.getX());
        tableResponse.getTable().setY(table.getY());
        tableResponse.getTable().setTableId(table.getTableId());
        return tableResponse;
    }

    private void hasAnyBooking(TableResponse tableResponse){

        Reservation approvedReservation =
                reservationRepository.findFirstByTableTableIdAndStatus(tableResponse.getTable().getTableId(),
                        Reservation.Status.APPROVED).orElse(null);
        Reservation diningReservation =
                reservationRepository.findFirstByTableTableIdAndStatus(tableResponse.getTable().getTableId(),
                        Reservation.Status.DINING).orElse(null);

        if(approvedReservation != null || diningReservation != null){
            tableResponse.setBooked(true);
        }
    }

}
