package blibli.future.xyresto.service;

import blibli.future.xyresto.dto.EditOrderStatusRequest;
import blibli.future.xyresto.dto.OrderedProduct;
import blibli.future.xyresto.dto.request.EditReservationStatusRequest;
import blibli.future.xyresto.dto.request.OrderRequest;
import blibli.future.xyresto.dto.request.ReservationRequest;
import blibli.future.xyresto.dto.response.ReservationResponse;
import blibli.future.xyresto.exception.ExpiredDateException;
import blibli.future.xyresto.exception.IsBookedException;
import blibli.future.xyresto.exception.ObjectNotFoundException;
import blibli.future.xyresto.exception.UnauthorizedAccessException;
import blibli.future.xyresto.model.*;
import blibli.future.xyresto.repository.*;
import blibli.future.xyresto.util.EmailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class ReservationService {

    @Value("${itemsPerPage:#{10}}")
    private int ITEMS_PER_PAGE;

    @Value("${timeZone:GMT+7}")
    private String timeZone;

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    TableRepository tableRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductService productService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    CounterService counterService;

    @Autowired
    CartService cartService;

    @Autowired
    EmailUtil emailUtil;


    public ReservationResponse createReservation(ReservationRequest request) throws IsBookedException,
            ExpiredDateException,
            ObjectNotFoundException{
        Instant requestTimestamp = Instant.ofEpochMilli(request.getTimestamp());

        if(requestTimestamp.isBefore(Instant.now())){
            throw new ExpiredDateException();
        }

        ReservationResponse response;

        List<Reservation> reservationOnTimestamp =
                reservationRepository.findAllByTimestampAndStatus(Instant.ofEpochMilli(request.getTimestamp()),Reservation.Status.APPROVED);
        List<Long> bookedTable =
                reservationOnTimestamp.stream().map(reservation -> reservation.getTable().getTableId()).collect(Collectors.toList());


        boolean isAdmin = SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"));

        if(bookedTable.contains(request.getTableId())){
            throw new IsBookedException();
        }else{
            List<OrderedProduct> orders = new ArrayList<>();
            String email = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
            Reservation newReservation = Reservation.builder()
                    .reservationId(counterService.getNextIdAndIncrement("RESERVATION"))
                    .timestamp(Instant.ofEpochMilli(request.getTimestamp()))
                    .table(tableRepository.findFirstByTableId(request.getTableId())
                            .orElseThrow(() -> new ObjectNotFoundException(Table.class)))
                    .user(userRepository.findByEmail(email).orElseThrow(() -> new ObjectNotFoundException(User.class)))
                    .status(isAdmin ? Reservation.Status.APPROVED : Reservation.Status.PENDING)
                    .build();

            if(request.getOrders() != null && !request.getOrders().isEmpty()){
                for (OrderRequest order: request.getOrders()) {
                    OrderedProduct currentProduct = OrderedProduct.builder().build();
                    currentProduct.setProduct(productRepository.findFirstByProductId(order.getProductId())
                            .orElseThrow(() -> new ObjectNotFoundException(Product.class)));
                    currentProduct.setQuantity(order.getQuantity());
                    currentProduct.getProduct().setId(null);
                    orders.add(currentProduct);
                }
                newReservation.setProducts(orders);
            }
            reservationRepository.save(newReservation);

            //Delete Cart

            cartService.clearCart(userRepository.findByEmail(email).orElse(null));


            response = toReservationResponse(newReservation);
            return response;
        }

    }

    public ReservationResponse getReservationByReservationId(long reservationId){

        ReservationResponse response;
        Reservation reservation =
                reservationRepository.findByReservationId(reservationId)
                        .orElseThrow(() -> new ObjectNotFoundException(Reservation.class));

        List<OrderedProduct> updatedOrderedProduct =
                reservation.getProducts().stream().map(this::checkIfCategoryIsNull).collect(Collectors.toList());

        reservation.setProducts(updatedOrderedProduct);
        reservationRepository.save(reservation);

        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
        List<String> roles = currentUser.getAuthorities().stream().map(Object::toString).collect(Collectors.toList());
        String email = currentUser.getPrincipal().toString();

        boolean isAdmin = false;
        for (String role: roles) {
            if (role.equals("ROLE_ADMIN")) {
                isAdmin = true;
                break;
            }
        }

        if(isAdmin){
            response = toReservationResponse(reservation);
        }else{
            if(reservation.getUser().getEmail().equals(email)){
                response = toReservationResponse(reservation);
            }else{
                throw new UnauthorizedAccessException();
            }
        }
        return response;

    }

    public void cancelReservation(long reservationId) throws UnauthorizedAccessException,ObjectNotFoundException{

        Reservation reservation = reservationRepository.findByReservationId(reservationId)
                .orElseThrow(() -> new ObjectNotFoundException(Reservation.class));

        String currentEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        if(reservation.getUser().getEmail().equals(currentEmail)){
            reservation.setStatus(Reservation.Status.CANCELED);
            reservationRepository.save(reservation);
        }else{
            throw new UnauthorizedAccessException();
        }
    }

    public List<ReservationResponse> getAllReservations(){
        String userEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        List<Reservation> reservationsList = reservationRepository.findAllByUserEmail(userEmail);

        return reservationsList.stream().map(this::toReservationResponse).collect(Collectors.toList());
    }

    public ReservationResponse addOrders(long reservationId, List<OrderRequest> request) throws UnauthorizedAccessException{

        Reservation reservation =
                reservationRepository.findByReservationId(reservationId).orElseThrow(() -> new ObjectNotFoundException(Reservation.class));

        String currentUserEmail = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

        if(currentUserEmail.equals(reservation.getUser().getEmail())){
            Map<Boolean, List<OrderRequest>> requests = request.stream().collect(
                Collectors.partitioningBy(
                    orderRequest -> reservation.getProducts().stream().anyMatch(
                        orderedProduct -> orderedProduct.getProduct().getProductId() == orderRequest.getProductId()
                    )
                )
            );

            requests.get(true).forEach(orderRequest -> {
                OrderedProduct reservationProduct = reservation.getProducts().stream().filter(
                    orderedProduct -> orderedProduct.getProduct().getProductId() == orderRequest.getProductId()
                ).findFirst().get();

                reservationProduct.setQuantity(reservationProduct.getQuantity() + orderRequest.getQuantity());
                reservationProduct.setServed(false);
            });

            requests.get(false).forEach(orderRequest -> reservation.getProducts().add(
                OrderedProduct.builder()
                    .product(productRepository.findFirstByProductId(orderRequest.getProductId()).orElseThrow(() -> new ObjectNotFoundException(Product.class)))
                    .quantity(orderRequest.getQuantity())
                    .served(false)
                    .build()
            ));

            reservationRepository.save(reservation);

            Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
            String email = currentUser.getPrincipal().toString();
            cartService.clearCart(userRepository.findByEmail(email).orElse(null));

            return toReservationResponse(reservation);
        }else{
            throw new UnauthorizedAccessException();
        }
    }

    public Pair<Long, List<ReservationResponse>> getAllReservations(String status, Instant timestamp, int page){

        Page<Reservation> reservations;
        page = Math.max(page, 0);
        Pageable pageable = PageRequest.of(page,ITEMS_PER_PAGE);

        long totalReservations;

        if(status == null && timestamp == null){
            reservations = reservationRepository.findAllOrderByTimestampDescReservationIdAsc(pageable);
            totalReservations = reservations.getTotalElements();
        }else if(status == null){
            reservations = reservationRepository.findAllByTimestampOrderByTimestampDescReservationIdAsc(timestamp,pageable);
            totalReservations = reservations.getTotalElements();
        }else if(timestamp == null){
            reservations = reservationRepository.findAllByStatusOrderByTimestampDesc(status,pageable);
            totalReservations = reservations.getTotalElements();
        }else{
            reservations = reservationRepository.findAllByStatusAndTimestampOrderByTimestampDesc(status,timestamp,pageable);
            totalReservations = reservations.getTotalElements();
        }

        for (Reservation reservation: reservations) {
            List<OrderedProduct> updatedOrderedProduct =
                    reservation.getProducts().stream().map(this::checkIfCategoryIsNull).collect(Collectors.toList());

            reservation.setProducts(updatedOrderedProduct);
        }

        return Pair.of(totalReservations, reservations.stream()
                .map(this::toReservationResponse)
                .collect(Collectors.toList()));
    }

    public void editReservationStatus(long reservationId, EditReservationStatusRequest request) {

        // Change to new status
        Reservation reservation =
                reservationRepository.findByReservationId(reservationId).orElseThrow(()->  new ObjectNotFoundException(Reservation.class));
        reservation.setStatus(request.getStatus());

        // Decline All Pending Reservation On the same table at the same timestamp
        long bookedTableId = reservation.getTable().getTableId();
        Instant beforeHourTimestamp = reservation.getTimestamp().minus(1,ChronoUnit.HOURS).minusMillis(1);
        Instant nextHourTimestamp = reservation.getTimestamp().plus(1, ChronoUnit.HOURS).plusMillis(1);
        if(request.getStatus() == Reservation.Status.APPROVED){
            List<Reservation> reservationOnTimestamp = reservationRepository.findAllByTimestampBetweenAndStatus(beforeHourTimestamp,
                    nextHourTimestamp, Reservation.Status.PENDING);
            for (Reservation pendingReservation: reservationOnTimestamp) {
                if(pendingReservation.getTable().getTableId() == bookedTableId){
                    pendingReservation.setStatus(Reservation.Status.DECLINED);
                }
            }
            reservationRepository.saveAll(reservationOnTimestamp);
        }
        reservationRepository.save(reservation);

        if (request.getStatus() == Reservation.Status.FINISHED) {
            reservation.getProducts().forEach(orderedProduct -> {
                productService.incrementPurchaseCount(orderedProduct.getProduct().getProductId(), orderedProduct.getQuantity());
            });

            String formattedDate = Instant.now().atZone(ZoneId.of(timeZone)).format(DateTimeFormatter.ofPattern("dd MMMM uuuu"));
            NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale("en", "ID"));

            int maxLength = reservation.getProducts().stream().mapToInt(
                    orderedProduct -> orderedProduct.getProduct().getName().length()
                            + String.valueOf(orderedProduct.getQuantity()).length()
                            + numberFormat.format((long) orderedProduct.getProduct().getPrice() * orderedProduct.getQuantity()).length()
                            + 13
            ).max().orElse(13);

            HashMap<String, String> replacements = new HashMap<>();
            replacements.put("fullName", reservation.getUser().getFullName());
            replacements.put("reservationId", String.valueOf(reservationId));
            replacements.put("reservationDate", formattedDate);
            replacements.put("totalPrice", numberFormat.format(
                reservation.getProducts().stream().mapToInt(
                    orderedProduct -> orderedProduct.getProduct().getPrice() * orderedProduct.getQuantity()
                ).sum()
            ));
            replacements.put("line", IntStream.range(0, maxLength).mapToObj(i -> "-").collect(Collectors.joining()));
            replacements.put("menuList", reservation.getProducts().stream().map(
                orderedProduct -> {
                    String prod = orderedProduct.getQuantity() + " x " + orderedProduct.getProduct().getName();
                    String price = numberFormat.format((long) orderedProduct.getProduct().getPrice() * orderedProduct.getQuantity());
                    return prod + IntStream.range(0, maxLength - prod.length() - price.length()).mapToObj(i -> " ").collect(Collectors.joining()) + price + "\n\n";
                }
            ).collect(Collectors.joining()));

            try {
                String template = String.join("\n", Files.readAllLines(
                    new ClassPathResource("mail-template/receipt.txt").getFile().toPath()));
                for (Map.Entry<String, String> entry : replacements.entrySet()) {
                    template = template.replaceAll("\\{" + entry.getKey() + "\\}", entry.getValue());
                }

                emailUtil.sendEmail(reservation.getUser().getEmail(), String.format("Your bill - %s - XY Resto", formattedDate), template);
            } catch (IOException ignored) {
                // No template, don't send email
            }
        }
    }

    public void editOrderStatus(long reservationId, EditOrderStatusRequest request){

        Reservation reservation =
                reservationRepository.findByReservationId(reservationId).orElseThrow(()->  new ObjectNotFoundException(Reservation.class));
        List<OrderedProduct> orderedProducts = reservation.getProducts();
        for (OrderedProduct product : orderedProducts) {
            if(request.getProductIds().contains(product.getProduct().getProductId())){
              product.setServed(true);
            }
        }
        reservationRepository.save(reservation);
    }

    private ReservationResponse toReservationResponse(Reservation reservation){

        return new ReservationResponse(reservation);
    }

    private OrderedProduct checkIfCategoryIsNull(OrderedProduct orderedProduct){

        if(orderedProduct.getProduct().getCategory() != null){
            Category category =
                    categoryRepository.findFirstByCategoryId(orderedProduct.getProduct().getCategory().getCategoryId()).orElse(null);
            if(category == null){
                orderedProduct.getProduct().setCategory(null);
            }
        }
        return orderedProduct;
    }

}
