package blibli.future.xyresto.service;

import blibli.future.xyresto.dto.response.BannerResponse;
import blibli.future.xyresto.exception.NotSupportedMIMETypeException;
import blibli.future.xyresto.exception.ObjectNotFoundException;
import blibli.future.xyresto.model.Banner;
import blibli.future.xyresto.repository.BannerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BannerService {

    @Autowired
    private CounterService counterService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private BannerRepository bannerRepository;

    public List<BannerResponse> getAllBanners() {
        return bannerRepository.findAll(Sort.by(Sort.Direction.DESC, "bannerId")).stream().map(BannerResponse::new).collect(Collectors.toList());
    }

    public BannerResponse getBanner(long bannerId) throws ObjectNotFoundException {
        return new BannerResponse(bannerRepository.findFirstByBannerId(bannerId).orElseThrow(() -> new ObjectNotFoundException(Banner.class)));
    }

    public BannerResponse createBanner(MultipartFile file) throws IOException, NotSupportedMIMETypeException {

        String mimeType = file.getContentType();
        if(mimeType != null && (mimeType.equals(MimeTypeUtils.IMAGE_PNG_VALUE) || mimeType.equals(MimeTypeUtils.IMAGE_JPEG_VALUE))) {
            String url = storageService.storeFile(file, StorageService.StorageType.BANNER_IMAGE, String.valueOf(counterService.getNextId("BANNER")));
            Banner banner = Banner.builder()
                    .bannerId(counterService.getNextIdAndIncrement("BANNER"))
                    .imageUrl(url)
                    .build();
            bannerRepository.save(banner);
            return new BannerResponse(banner);
        }
        else {
            throw new NotSupportedMIMETypeException();
        }
    }

    public void deleteBanner(long bannerId) throws ObjectNotFoundException, IOException {
        Banner banner = bannerRepository.findFirstByBannerId(bannerId).orElseThrow(() -> new ObjectNotFoundException(Banner.class));
        bannerRepository.delete(banner);
        storageService.deleteFile(StorageService.StorageType.BANNER_IMAGE, String.valueOf(bannerId));
    }
}
