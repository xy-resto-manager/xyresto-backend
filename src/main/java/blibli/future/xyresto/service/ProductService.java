package blibli.future.xyresto.service;

import blibli.future.xyresto.controller.ProductController;
import blibli.future.xyresto.dto.request.CategoryRequest;
import blibli.future.xyresto.dto.request.ProductRequest;
import blibli.future.xyresto.dto.response.CategoryResponse;
import blibli.future.xyresto.dto.response.ProductResponse;
import blibli.future.xyresto.exception.MalformedRequestParameterException;
import blibli.future.xyresto.exception.NotSupportedMIMETypeException;
import blibli.future.xyresto.exception.ObjectNotFoundException;
import blibli.future.xyresto.model.Category;
import blibli.future.xyresto.model.Product;
import blibli.future.xyresto.repository.CategoryRepository;
import blibli.future.xyresto.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Value("${itemsPerPage:#{10}}")
    private int ITEMS_PER_PAGE;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CounterService counterService;

    @Autowired
    private StorageService storageService;

    public List<CategoryResponse> getAllCategories() {
        return categoryRepository.findAll(Sort.by(Sort.Direction.ASC, "categoryId")).stream().map(CategoryResponse::new).collect(Collectors.toList());
    }

    public CategoryResponse createCategory(CategoryRequest categoryRequest) {
        Category category = Category.builder()
                .categoryId(counterService.getNextIdAndIncrement("CATEGORY"))
                .name(categoryRequest.getName())
                .build();
        categoryRepository.save(category);
        return new CategoryResponse(category);
    }

    public CategoryResponse editCategory(long categoryId, CategoryRequest categoryRequest) throws ObjectNotFoundException {
        Category category = categoryRepository.findFirstByCategoryId(categoryId).orElseThrow(() -> new ObjectNotFoundException(Category.class));
        category.setName(categoryRequest.getName());
        categoryRepository.save(category);
        return new CategoryResponse(category);
    }

    public void deleteCategory(long categoryId) throws ObjectNotFoundException {
        Category category = categoryRepository.findFirstByCategoryId(categoryId).orElseThrow(() -> new ObjectNotFoundException(Category.class));
        List<Product> products = productRepository.findAllByCategory(category);
        for (Product product: products) {
            product.setCategory(null);
        }
        productRepository.saveAll(products);
        categoryRepository.delete(category);
    }

    public List<ProductResponse> getProducts(String query, String sort, int page) throws ObjectNotFoundException, MalformedRequestParameterException {
        Page<Product> products;
        page = page < 0 ? 0 : page;
        Pageable pageable = PageRequest.of(page, ITEMS_PER_PAGE);

        if (sort.equalsIgnoreCase("popular")) {
            if (query == null) {
                products = productRepository.findAllByOrderByPurchaseCountDescNameAscProductIdAsc(pageable);
                ProductController.totalItems = products.getTotalElements();
            }
            else {
                products = productRepository.findAllByQueryOrderByPurchaseCountDescNameAscProductIdAsc(query, pageable);
                ProductController.totalItems = products.getTotalElements();
            }
        } else if (sort.equalsIgnoreCase("recommended")) {
            if (query == null){
                products = productRepository.findAllByRecommendedTrueOrderByPurchaseCountDescNameAscProductIdAsc(pageable);
                ProductController.totalItems = products.getTotalElements();
            }
            else{
                products = productRepository.findAllByQueryAndRecommendedTrueOrderByPurchaseCountDescNameAscProductIdAsc(query, pageable);
                ProductController.totalItems = products.getTotalElements();
            }
        } else if(sort.equalsIgnoreCase("null")){
            products = productRepository.findAllNullCategoryOrderByPurchaseCountDescNameAscProductIdAsc(pageable);
            ProductController.totalItems = products.getTotalElements();
        }else {
            Category category = null;
            try {
                category = categoryRepository.findFirstByCategoryId(Integer.parseInt(sort)).orElseThrow(() -> new ObjectNotFoundException(Category.class));
            } catch (NumberFormatException e) {
                throw new MalformedRequestParameterException("sort");
            }
            if (query == null){
                products = productRepository.findAllByCategoryOrderByPurchaseCountDescNameAscProductIdAsc(category, pageable);
                ProductController.totalItems = products.getTotalElements();
            }
            else{
                products = productRepository.findAllByQueryAndCategoryOrderByPurchaseCountDescNameAscProductIdAsc(query, category, pageable);
                ProductController.totalItems = products.getTotalElements();
            }
        }

        List<Product> updatedProduct =
                products.stream().map(product -> checkIfCategoryIsNull(product)).collect(Collectors.toList());

        return updatedProduct.stream().map(ProductResponse::new).collect(Collectors.toList());
    }

    public ProductResponse getProduct(long productId) throws ObjectNotFoundException {

        Product product =
                productRepository.findFirstByProductId(productId).orElseThrow(() -> new ObjectNotFoundException(Product.class));

        checkIfCategoryIsNull(product);

        return new ProductResponse(product);
    }

    public ProductResponse createProduct(ProductRequest productRequest) throws ObjectNotFoundException {
        List<String> traits = new ArrayList<>();
        if(productRequest.getTraits() != null) {
            traits = productRequest.getTraits();
        }
        Product product = Product.builder()
                .productId(counterService.getNextIdAndIncrement("PRODUCT"))
                .name(productRequest.getName())
                .description(productRequest.getDescription())
                .price(productRequest.getPrice())
                .traits(traits)
                .recommended(productRequest.isRecommended())
                .purchaseCount(0)
                .build();

        if (productRequest.getCategory() != null) {
            Category category = categoryRepository.findFirstByCategoryId(productRequest.getCategory()).orElseThrow(() -> new ObjectNotFoundException(Category.class));
            product.setCategory(category);
        }

        productRepository.save(product);
        return new ProductResponse(product);
    }

    public ProductResponse editProduct(long productId, ProductRequest productRequest) throws ObjectNotFoundException {
        Product product = productRepository.findFirstByProductId(productId).orElseThrow(() -> new ObjectNotFoundException(Product.class));

        product.setName(productRequest.getName());
        product.setDescription(productRequest.getDescription());
        product.setPrice(productRequest.getPrice());
        product.setTraits(new ArrayList<>(productRequest.getTraits()));
        product.setRecommended(productRequest.isRecommended());

        if (productRequest.getCategory() != null) {
            Category category = categoryRepository.findFirstByCategoryId(productRequest.getCategory()).orElseThrow(() -> new ObjectNotFoundException(Category.class));
            product.setCategory(category);
        } else {
            product.setCategory(null);
        }

        productRepository.save(product);
        return new ProductResponse(product);
    }

    public void deleteProduct(long productId) throws ObjectNotFoundException {
        Product product = productRepository.findFirstByProductId(productId).orElseThrow(() -> new ObjectNotFoundException(Product.class));
        productRepository.delete(product);
    }

    public ProductResponse setProductImage(long productId, MultipartFile file) throws ObjectNotFoundException, IOException, NotSupportedMIMETypeException {
        String mimeType = file.getContentType();
        if(mimeType == null || !mimeType.equals(MimeTypeUtils.IMAGE_PNG_VALUE) && !mimeType.equals(MimeTypeUtils.IMAGE_JPEG_VALUE)) {
            throw new NotSupportedMIMETypeException();
        }

        Product product = productRepository.findFirstByProductId(productId).orElseThrow(() -> new ObjectNotFoundException(Product.class));
        String url = storageService.storeFile(file, StorageService.StorageType.PRODUCT_IMAGE, String.valueOf(productId));
        product.setImageUrl(url);
        productRepository.save(product);
        return new ProductResponse(product);
    }

    public void incrementPurchaseCount(long productId, long increment) {
        Product product = productRepository.findFirstByProductId(productId).orElseThrow(() -> new ObjectNotFoundException(Product.class));
        long purchaseCount = product.getPurchaseCount() + increment;
        product.setPurchaseCount(purchaseCount);
        productRepository.save(product);
    }

    private Product checkIfCategoryIsNull(Product product){

        if (product.getCategory() == null) {
            product.setCategory(null);
            productRepository.save(product);
            return product;
        }
        Category category =
                categoryRepository.findFirstByCategoryId(product.getCategory().getCategoryId()).orElse(null);
        if(category == null){
            product.setCategory(null);
            productRepository.save(product);
        }
        return product;
    }

}
