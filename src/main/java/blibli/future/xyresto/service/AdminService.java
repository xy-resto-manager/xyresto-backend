package blibli.future.xyresto.service;

import blibli.future.xyresto.dto.request.ChangeRoleRequest;
import blibli.future.xyresto.dto.response.UserResponse;
import blibli.future.xyresto.model.User;
import blibli.future.xyresto.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminService {

    @Autowired
    UserRepository userRepository;

    public List<UserResponse> getAllUsers(String role){
        List<User> rawUserList = new ArrayList<>();
        if(role != null){
            role = role.toUpperCase();
            rawUserList = userRepository.findAllByRolesContains(role);
        }else{
            rawUserList = userRepository.findAll();
        }
        List<UserResponse> userResponseList =
                rawUserList.stream().map(user -> toUserResponse(user)).collect(Collectors.toList());

       return userResponseList;
    }

    public UserResponse editRoles(long userId, ChangeRoleRequest request) throws UsernameNotFoundException{

        User user =
                userRepository.findByUserId(userId).orElseThrow(() -> new UsernameNotFoundException("Wrong User Id"));
        user.setRoles(request.getRoles());
        userRepository.save(user);

        return new UserResponse(user);
    }

    public UserResponse toUserResponse(User user){

        UserResponse userResponse = UserResponse.builder().build();

        BeanUtils.copyProperties(user,userResponse);
        userResponse.setId(user.getUserId());

        return  userResponse;
    }
}
