package blibli.future.xyresto.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Service
public class CookieUtil {

  public final String TOKEN_KEY = "jwtToken";

  @Value("${jwt.maxAgeHours}")
  private int cookieMaxAgeHours;

  @Value("${jwtCookie.httpOnly}")
  private boolean httpOnly;

  public void createJwtCookie(HttpServletResponse response,String token) {
    Cookie jwtCookie = new Cookie(TOKEN_KEY, token);
    jwtCookie.setHttpOnly(httpOnly);
    jwtCookie.setMaxAge(1000 * 60 * 60 * cookieMaxAgeHours);
    jwtCookie.setPath("/");
    response.addCookie(jwtCookie);
  }

  public void clearCookie(HttpServletResponse res) {
    Cookie cookie = new Cookie(TOKEN_KEY, null);
    cookie.setPath("/");
    cookie.setHttpOnly(httpOnly);
    cookie.setMaxAge(0);
    res.addCookie(cookie);
  }
}
