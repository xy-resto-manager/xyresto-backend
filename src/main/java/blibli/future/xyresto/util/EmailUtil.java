package blibli.future.xyresto.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

@Service
public class EmailUtil {

    @Value("${email.senderAddress}")
    private String senderAddress;

    @Value("${email.senderName}")
    private String senderName;

    private final Session session;

    public EmailUtil(
            @Value("${email.smtp.host}") String smtpHost,
            @Value("${email.smtp.port}") String smtpPort,
            @Value("${email.smtp.user}") String smtpUser,
            @Value("${email.smtp.password}") String smtpPassword
    ) {
        Properties props = new Properties();
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.port", smtpPort);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(smtpUser, smtpPassword);
            }
        };
        session = Session.getInstance(props, auth);
    }

    @Async
    public void sendEmail(String recipient, String subject, String body) {
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.addHeader("Content-Type", "text/html; charset=utf-8");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress(senderAddress, senderName));
            msg.setReplyTo(InternetAddress.parse(senderAddress, false));

            msg.setSubject(subject);

            msg.setText(body);

            msg.setSentDate(new Date());
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient, false));
            Transport.send(msg);
            System.out.println("message sent");
        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
