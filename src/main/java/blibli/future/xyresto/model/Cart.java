package blibli.future.xyresto.model;

import blibli.future.xyresto.dto.OrderedProduct;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cart {

    @Id
    private String id;

    @Indexed(unique = true)
    private User user;

    @Singular
    private List<OrderedProduct> products;

    private Summary summary;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Summary {
        private int totalPrice;
        private int quantity;
    }

    public void calculateSummary() {
        summary = new Summary(0, 0);
        products.stream().forEach(orderedProduct -> {
            summary.totalPrice += orderedProduct.getProduct().getPrice() * orderedProduct.getQuantity();
            summary.quantity += orderedProduct.getQuantity();
        });
    }
}
