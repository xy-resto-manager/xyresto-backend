package blibli.future.xyresto.model;

import blibli.future.xyresto.dto.OrderedProduct;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.List;

@Document
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Reservation {

    @Id
    private String id;

    @Indexed(unique = true)
    private long reservationId;

    private User user;

    private Instant timestamp;
    private Status status;

    @Singular
    private List<OrderedProduct> products;
    private Table table;

    public enum Status {
        PENDING, APPROVED, DECLINED, DINING, FINISHED,CANCELED
    }
}
