package blibli.future.xyresto.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class Counter {

    @Id
    private String id;

    @Indexed(unique = true)
    private String entity;
    private long nextId;

    public Counter(String entity) {
        this.entity = entity;
        this.nextId = 1;
    }
}
