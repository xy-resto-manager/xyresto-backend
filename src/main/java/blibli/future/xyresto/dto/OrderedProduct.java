package blibli.future.xyresto.dto;

import blibli.future.xyresto.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderedProduct {

    private Product product;
    private int quantity;
    private boolean served;
}
