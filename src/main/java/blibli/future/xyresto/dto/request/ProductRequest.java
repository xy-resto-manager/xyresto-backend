package blibli.future.xyresto.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {

    @NotBlank
    private String name;
    @NotNull
    private int price;
    private List<String> traits;
    private String description;
    private Long category;
    @NotNull
    private boolean recommended;
}
