package blibli.future.xyresto.dto.request;

import blibli.future.xyresto.model.Reservation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EditReservationStatusRequest {

    private Reservation.Status status;

}
