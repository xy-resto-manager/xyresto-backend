package blibli.future.xyresto.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterRequest {

    @NotNull(message = "Email cannot be null")
    private String email;
    @NotNull(message = "fullName cannot be null")
    private String fullName;
    @NotNull(message = "Password cannot be null")
    private String password;

}
