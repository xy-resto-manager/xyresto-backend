package blibli.future.xyresto.dto.response;

import blibli.future.xyresto.model.Category;
import blibli.future.xyresto.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {

    private long id;
    private String name;
    private int price;
    private List<String> traits;
    private String description;
    private String imageUrl;
    private CategoryResponse category;
    private boolean recommended;
    private long purchaseCount;

    public ProductResponse(Product product) {
        this.id = product.getProductId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.traits = new ArrayList<>(product.getTraits());
        this.description = product.getDescription();
        this.imageUrl = product.getImageUrl();
        if (product.getCategory() != null)
            this.category = new CategoryResponse(product.getCategory());
        this.recommended = product.isRecommended();
        this.purchaseCount = product.getPurchaseCount();
    }
}
