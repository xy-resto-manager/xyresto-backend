package blibli.future.xyresto.dto.response;

import blibli.future.xyresto.dto.OrderedProduct;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderedProductResponse {

    private ProductResponse product;
    private int quantity;
    private boolean served;

}
