package blibli.future.xyresto.dto.response;

import blibli.future.xyresto.dto.OrderedProduct;
import blibli.future.xyresto.model.Reservation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReservationResponse {

    private long id;
    private long timestamp;
    private long tableId;
    private Reservation.Status status;
    private String email;
    private String fullName;
    private List<OrderedProductResponse> orders;

    public ReservationResponse(Reservation reservation) {
        this.id = reservation.getReservationId();
        this.timestamp = reservation.getTimestamp().toEpochMilli();
        this.tableId = reservation.getTable().getTableId();
        this.status = reservation.getStatus();
        this.email = reservation.getUser().getEmail();
        this.fullName = reservation.getUser().getFullName();
        this.orders = toOrderedProductResponseList(reservation.getProducts());
    }

    public List<OrderedProductResponse> toOrderedProductResponseList (List<OrderedProduct> orderedProducts){

        List<OrderedProductResponse> orderedProductResponses = new ArrayList<>();

        for (OrderedProduct product: orderedProducts) {
            OrderedProductResponse orderedProductResponse = OrderedProductResponse.builder().build();
            orderedProductResponse.setProduct(new ProductResponse(product.getProduct()));
            orderedProductResponse.setQuantity(product.getQuantity());
            orderedProductResponse.setServed(product.isServed());
            orderedProductResponses.add(orderedProductResponse);
        }
        return orderedProductResponses;
    }
}
