package blibli.future.xyresto.dto.response;

import lombok.*;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class PaginatedResponse<T> extends BaseArrayResponse<T> {

    private Pagination pagination;

    public PaginatedResponse(int code, String status, List<T> data, Pagination pagination) {
        super(code, status, data);
        setPagination(pagination);
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Pagination {
        private int currentPage;
        private int itemsPerPage;
        private long totalItems;
    }
}
