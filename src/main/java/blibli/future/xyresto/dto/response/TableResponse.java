package blibli.future.xyresto.dto.response;

import blibli.future.xyresto.dto.TableDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TableResponse {

    public TableDTO table;
    public boolean booked;

}
