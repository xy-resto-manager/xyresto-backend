package blibli.future.xyresto.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class BaseArrayResponse<T> extends BaseResponse<List<T>> {

    public BaseArrayResponse(int code, String status, List<T> data) {
        this.setCode(code);
        this.setData(data);
        this.setStatus(status);
    }
}
