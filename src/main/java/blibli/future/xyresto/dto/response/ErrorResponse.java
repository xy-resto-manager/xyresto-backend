package blibli.future.xyresto.dto.response;

import blibli.future.xyresto.exception.BaseException;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {

    private int code;
    private String status;
    @Singular
    private List<ExceptionResponse> errors;
}
