package blibli.future.xyresto.dto.response;

import blibli.future.xyresto.exception.BaseException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.naming.AuthenticationException;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionResponse {

    private String error;
    private String message;

    public ExceptionResponse(BaseException e) {
        this.error = e.getError();
        this.message = e.getMessage();
    }
    public ExceptionResponse(Exception e){
        this.error = e.getClass().getSimpleName();
        this.message = e.getMessage();
    }
}
