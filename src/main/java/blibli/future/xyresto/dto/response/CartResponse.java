package blibli.future.xyresto.dto.response;

import blibli.future.xyresto.dto.OrderedProduct;
import blibli.future.xyresto.model.Cart;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartResponse {

    private UserResponse user;
    private SummaryResponse summary;
    private List<OrderedProductResponse> products;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SummaryResponse {
        private int totalPrice;
        private int quantity;

        public SummaryResponse(Cart.Summary summary) {
            this.totalPrice = summary.getTotalPrice();
            this.quantity = summary.getQuantity();
        }
    }
}
