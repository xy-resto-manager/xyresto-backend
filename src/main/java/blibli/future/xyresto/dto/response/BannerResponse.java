package blibli.future.xyresto.dto.response;

import blibli.future.xyresto.model.Banner;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BannerResponse {

    public BannerResponse(Banner banner) {
        this.id = banner.getBannerId();
        this.imageUrl = banner.getImageUrl();
    }

    private long id;
    private String imageUrl;
}
