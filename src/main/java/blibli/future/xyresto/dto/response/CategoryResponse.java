package blibli.future.xyresto.dto.response;

import blibli.future.xyresto.model.Banner;
import blibli.future.xyresto.model.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CategoryResponse {

    private long id;
    private String name;

    public CategoryResponse(Category category) {
        this.id = category.getCategoryId();
        this.name = category.getName();
    }
}
