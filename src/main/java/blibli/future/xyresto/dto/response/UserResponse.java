package blibli.future.xyresto.dto.response;

import blibli.future.xyresto.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {

    public UserResponse(User user) {
        this.id = user.getUserId();
        this.email = user.getEmail();
        this.fullName = user.getFullName();
        this.roles = user.getRoles();
    }

    private long id;
    private String email;
    private String fullName;
    private List<String> roles;
}
