package blibli.future.xyresto.repository;

import blibli.future.xyresto.model.Banner;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface BannerRepository extends MongoRepository<Banner, String> {

    public Optional<Banner> findFirstByBannerId(long bannerId);
}
