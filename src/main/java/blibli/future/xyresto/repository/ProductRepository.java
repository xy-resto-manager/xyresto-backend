package blibli.future.xyresto.repository;

import blibli.future.xyresto.model.Category;
import blibli.future.xyresto.model.Counter;
import blibli.future.xyresto.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends MongoRepository<Product,String> {

    public Page<Product> findAllByOrderByPurchaseCountDescNameAscProductIdAsc(Pageable pageable);

    @Query(value = "{ $text: { $search: '?0', $caseSensitive: false } }", sort = "{'purchaseCount': -1, 'name': 1, 'productId': 1}")
    public Page<Product> findAllByQueryOrderByPurchaseCountDescNameAscProductIdAsc(String query, Pageable pageable);

    public Page<Product> findAllByRecommendedTrueOrderByPurchaseCountDescNameAscProductIdAsc(Pageable pageable);

    @Query(value = "{ $and: [ { $text: { $search: '?0', $caseSensitive: false } }, { 'recommended': true } ] }", sort = "{'purchaseCount': -1, 'name': 1, 'productId': 1}")
    public Page<Product> findAllByQueryAndRecommendedTrueOrderByPurchaseCountDescNameAscProductIdAsc(String query, Pageable pageable);

    public Page<Product> findAllByCategoryOrderByPurchaseCountDescNameAscProductIdAsc(Category category, Pageable pageable);

    @Query(value = "{ $and: [ { $text: { $search: '?0', $caseSensitive: false } }, { 'category': ?1 } ] }", sort = "{'purchaseCount': -1, 'name': 1, 'productId': 1}")
    public Page<Product> findAllByQueryAndCategoryOrderByPurchaseCountDescNameAscProductIdAsc(String query, Category category, Pageable pageable);

    public Optional<Product> findFirstByProductId(long productId);

    public List<Product> findAllByCategory(Category category);

    @Query(value = "{'category': { '$exists' : false }}", sort = "{'purchaseCount': -1, 'name': 1, 'productId': 1}")
    public Page<Product> findAllNullCategoryOrderByPurchaseCountDescNameAscProductIdAsc(Pageable pageable);
}
