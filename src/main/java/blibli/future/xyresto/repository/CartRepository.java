package blibli.future.xyresto.repository;

import blibli.future.xyresto.model.Cart;
import blibli.future.xyresto.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CartRepository extends MongoRepository<Cart, String> {

    public Optional<Cart> findFirstByUser(User user);
}
