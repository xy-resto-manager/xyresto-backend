package blibli.future.xyresto.repository;

import blibli.future.xyresto.model.Category;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends MongoRepository<Category, String> {

    public Optional<Category> findFirstByCategoryId(long categoryId);
}
