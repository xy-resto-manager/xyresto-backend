package blibli.future.xyresto.repository;

import blibli.future.xyresto.model.Table;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface TableRepository extends MongoRepository<Table, String> {

    public Optional<Table> findFirstByTableId(long tableId);
}
