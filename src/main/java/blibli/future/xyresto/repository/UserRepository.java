package blibli.future.xyresto.repository;

import blibli.future.xyresto.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<User,String> {

    public Optional<User> findByEmail(String email);

    public Optional<User> findByUserId(long userId);

    public List<User> findAllByRolesContains(String role);

}
