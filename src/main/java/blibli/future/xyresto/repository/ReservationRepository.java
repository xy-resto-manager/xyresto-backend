package blibli.future.xyresto.repository;

import blibli.future.xyresto.model.Reservation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import javax.swing.text.html.Option;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ReservationRepository extends MongoRepository<Reservation, String> {

    public List<Reservation> findAllByTimestampAndStatus(Instant timestamp,Reservation.Status status);

    public List<Reservation> findAllByTimestampBetweenAndStatus(Instant before, Instant after, Reservation.Status status);

    public Optional<Reservation> findByReservationId(long reservationId);

    public Optional<Reservation> findFirstByTableTableIdAndStatus(long tableId, Reservation.Status status);

    public List<Reservation> findAllByTableTableIdAndStatus(long tableId, Reservation.Status status);

    @Query(value = "{'user.email' : ?0}")
    public List<Reservation> findAllByUserEmail(String email);

    @Query(value = "{}", sort = "{'timestamp': -1, 'reservationId': 1}")
    public Page<Reservation> findAllOrderByTimestampDescReservationIdAsc(Pageable pageable);

    @Query(value = "{'timestamp' : ?0}",sort = "{timestamp : -1, 'reservationId': 1}")
    public Page<Reservation> findAllByTimestampOrderByTimestampDescReservationIdAsc(Instant timestamp, Pageable pageable);

    @Query(value = "{'status' : ?0}",sort = "{timestamp : -1, 'reservationId': 1}")
    public Page<Reservation> findAllByStatusOrderByTimestampDesc(String status, Pageable pageable);

    @Query(value = "{'status' : ?0,'timestamp' : ?1}",sort = "{timestamp : -1, 'reservationId': 1}")
    public Page<Reservation> findAllByStatusAndTimestampOrderByTimestampDesc(String status, Instant timestamp, Pageable pageable);

}
