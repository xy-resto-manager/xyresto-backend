package blibli.future.xyresto.repository;

import blibli.future.xyresto.model.Counter;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CounterRepository extends MongoRepository<Counter,String>{

    public Optional<Counter> findFirstByEntity(String entity);
}
