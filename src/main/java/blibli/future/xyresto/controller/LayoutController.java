package blibli.future.xyresto.controller;

import blibli.future.xyresto.dto.response.BaseArrayResponse;
import blibli.future.xyresto.dto.response.TableResponse;
import blibli.future.xyresto.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/layout")
public class LayoutController {


    @Autowired
    TableService tableService;

    @GetMapping("/tables")
    public BaseArrayResponse<TableResponse> getLayoutForUser(@RequestParam("timestamp") long timestamp){

        List<TableResponse> responses = tableService.getLayoutsForUser(timestamp);

        return new BaseArrayResponse<>(200,"OK",responses);
    }



}
