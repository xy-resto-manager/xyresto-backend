package blibli.future.xyresto.controller;

import blibli.future.xyresto.dto.request.ChangePasswordRequest;
import blibli.future.xyresto.dto.request.LoginRequest;
import blibli.future.xyresto.dto.request.RegisterRequest;
import blibli.future.xyresto.dto.response.BaseResponse;
import blibli.future.xyresto.dto.response.LoginResponse;
import blibli.future.xyresto.dto.response.UserResponse;
import blibli.future.xyresto.exception.EmailAlreadyRegisteredException;
import blibli.future.xyresto.exception.FailedAuthenticationException;
import blibli.future.xyresto.service.UserDetailsServiceImpl;
import blibli.future.xyresto.service.UserService;
import blibli.future.xyresto.util.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    CookieUtil cookieUtil;

    @Autowired
    UserService userService;

    @PostMapping(value = "/_login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<LoginResponse> login(@Validated @RequestBody LoginRequest loginRequest,
                                             HttpServletResponse response) throws FailedAuthenticationException {

        LoginResponse loginResponse = userService.login(loginRequest);
        cookieUtil.createJwtCookie(response,loginResponse.getToken());
        return new BaseResponse<>(200, "OK", loginResponse);
    }

    @PostMapping(value = "/_register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<UserResponse> register (@Validated @RequestBody RegisterRequest registerRequest) throws EmailAlreadyRegisteredException {

        UserResponse userResponse = userService.register(registerRequest);
        return new BaseResponse<>(200, "OK", userResponse);
    }

    @PostMapping(value = "/_change-password")
    public BaseResponse<Object> changePassword(@Validated @RequestBody ChangePasswordRequest request) throws FailedAuthenticationException, UsernameNotFoundException {

        userService.changePassword(request.getOldPassword(),
                request.getNewPassword());

        BaseResponse<Object> response = new BaseResponse<>(200,"OK",
                null);

        return response;
    }

    @PostMapping(value = "/_logout")
    public BaseResponse<Object> logout(HttpServletResponse response){
        cookieUtil.clearCookie(response);

        return new BaseResponse<>(200,"OK",null);
    }

    @GetMapping(value = "/current-user", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<UserResponse> getCurrentUser() throws UsernameNotFoundException {
        return new BaseResponse<>(200, "OK", userService.getCurrentUser());
    }
}
