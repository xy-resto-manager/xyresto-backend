package blibli.future.xyresto.controller;

import blibli.future.xyresto.dto.response.*;
import blibli.future.xyresto.exception.ObjectNotFoundException;
import blibli.future.xyresto.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/banner")
public class BannerController {

    @Autowired
    private BannerService bannerService;

    @GetMapping(value = "/banners", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseArrayResponse<BannerResponse> getAllBanners() {
        return new BaseArrayResponse<>(200, "OK", bannerService.getAllBanners());
    }

    @GetMapping(value = "/{bannerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<BannerResponse> getBanner(@PathVariable("bannerId") int bannerId) throws ObjectNotFoundException {
        return new BaseResponse<>(200, "OK", bannerService.getBanner(bannerId));
    }
}
