package blibli.future.xyresto.controller;

import blibli.future.xyresto.dto.EditOrderStatusRequest;
import blibli.future.xyresto.dto.TableDTO;
import blibli.future.xyresto.dto.request.CategoryRequest;
import blibli.future.xyresto.dto.request.ChangeRoleRequest;
import blibli.future.xyresto.dto.request.EditReservationStatusRequest;
import blibli.future.xyresto.dto.request.ProductRequest;
import blibli.future.xyresto.dto.response.*;
import blibli.future.xyresto.exception.*;
import blibli.future.xyresto.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/admin")
public class AdminController {

    @Value("${itemsPerPage:#{10}}")
    private int ITEMS_PER_PAGE;

    @Value("${timeZone:GMT+7}")
    private String timezone;

    @Autowired
    private AdminService adminService;

    @Autowired
    private BannerService bannerService;

    @Autowired
    private ProductService productService;

    @Autowired
    private TableService tableService;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private ReportService reportService;

    //User
    @GetMapping(value = "/user/users",consumes =
            MediaType.APPLICATION_JSON_VALUE,produces =
            MediaType.APPLICATION_JSON_VALUE)
    public BaseArrayResponse<UserResponse> getAllUsers(@RequestParam(required = false) String role)throws FailedAuthenticationException {

        List<UserResponse> rawUsers = adminService.getAllUsers(role);

        return new BaseArrayResponse<>(200,"OK",rawUsers);

    }

    @PostMapping(value = "/user/users/{userId}/roles",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<UserResponse> editRoles(@PathVariable("userId") int userId,
                                                @Validated @RequestBody ChangeRoleRequest roleRequest) throws UsernameNotFoundException {

        UserResponse user = adminService.editRoles(userId,roleRequest);

        return new BaseResponse<>(200,"OK", user);
    }

    // Banner
    @PostMapping(value = "/banner/banners", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public BaseResponse<BannerResponse> createBanner(@RequestParam("file") MultipartFile file) throws IOException, NotSupportedMIMETypeException {
        BannerResponse bannerResponse = bannerService.createBanner(file);
        return new BaseResponse<>(201, "Created", bannerResponse);
    }

    @DeleteMapping(value = "/banner/{bannerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<Object> deleteBanner(@PathVariable("bannerId") int bannerId) throws ObjectNotFoundException, IOException {
        bannerService.deleteBanner(bannerId);
        return new BaseResponse<>(200, "OK", null);
    }

    // Category
    @PostMapping(value = "/product/categories", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public BaseResponse<CategoryResponse> createCategory(@Valid @RequestBody CategoryRequest categoryRequest) {
        CategoryResponse categoryResponse = productService.createCategory(categoryRequest);
        return new BaseResponse<>(201, "CREATED", categoryResponse);
    }

    @PutMapping(value = "/product/categories/{categoryId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<CategoryResponse> editCategory(@Valid @RequestBody CategoryRequest categoryRequest, @PathVariable("categoryId") int categoryId) throws ObjectNotFoundException {
        CategoryResponse categoryResponse = productService.editCategory(categoryId, categoryRequest);
        return new BaseResponse<>(200, "OK", categoryResponse);
    }

    @DeleteMapping(value = "/product/categories/{categoryId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<Object> editCategory(@PathVariable("categoryId") int categoryId) throws ObjectNotFoundException {
        productService.deleteCategory(categoryId);
        return new BaseResponse<>(200, "OK", null);
    }

    // Product

    @PostMapping(value = "/product/products", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<ProductResponse> createProduct(@Valid @RequestBody ProductRequest productRequest) throws ObjectNotFoundException {
        return new BaseResponse<>(201, "Created", productService.createProduct(productRequest));
    }

    @PutMapping(value = "/product/{productId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<ProductResponse> editProduct(@Valid @RequestBody ProductRequest productRequest, @PathVariable("productId") int productId) throws ObjectNotFoundException {
        return new BaseResponse<>(200, "OK", productService.editProduct(productId, productRequest));
    }

    @DeleteMapping(value = "/product/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<Object> deleteProduct(@PathVariable("productId") int productId) throws ObjectNotFoundException {
        productService.deleteProduct(productId);
        return new BaseResponse<>(200, "OK", null);
    }

    @PostMapping(value = "/product/{productId}/image", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<ProductResponse> uploadProductImage(@PathVariable("productId") int productId, @RequestParam("file") MultipartFile file) throws ObjectNotFoundException, IOException, NotSupportedMIMETypeException {
        return new BaseResponse<>(200, "OK", productService.setProductImage(productId, file));
    }


    //Layout
    @PutMapping(value = "/layout/tables",consumes = MediaType.APPLICATION_JSON_VALUE,produces =
            MediaType.APPLICATION_JSON_VALUE)
    public BaseArrayResponse<TableDTO> editTableLayout(@RequestBody List<TableDTO> newLayout) throws DuplicateKeyException {

        tableService.editLayout(newLayout);

        return new BaseArrayResponse<>(200,"OK",newLayout);
    }



    @GetMapping(value = "layout/tables", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseArrayResponse<TableResponse> getTableLayout(){

        List<TableResponse> tableResponseList = tableService.getLayoutsForAdmin();

        return new BaseArrayResponse<>(200,"OK",tableResponseList);
    }

    // Reservation
    @GetMapping(value = "/reservation/reservations", produces = MediaType.APPLICATION_JSON_VALUE)
    public PaginatedResponse<ReservationResponse> getAllReservationAdmin(
            @RequestParam(required = false) String status,
            @RequestParam(required = false) Long timestamp,
            @RequestParam(defaultValue = "1") int page){

        Instant date = null;
        if (timestamp != null)
            date = Instant.ofEpochMilli(timestamp);

        Pair<Long, List<ReservationResponse>>
            reservations = reservationService.getAllReservations(status,date,page-1);

        return new PaginatedResponse<>(200,"OK", reservations.getSecond(),
                PaginatedResponse.Pagination.builder()
                .currentPage(page)
                .itemsPerPage(ITEMS_PER_PAGE)
                .totalItems(reservations.getFirst())
                .build());
    }

    @PutMapping(value = "/reservation/reservations/{reservationId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<ReservationResponse> editReservationStatus(@PathVariable int reservationId,
                                                     @RequestBody EditReservationStatusRequest request ){

        reservationService.editReservationStatus(reservationId,request);

        return new BaseResponse<>(200,"OK",null);
    }

    @PutMapping(value = "/reservation/reservations/{reservationId}/orders", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<Object> editOrderStatus(@PathVariable int reservationId,
                                                @RequestBody EditOrderStatusRequest request){

        reservationService.editOrderStatus(reservationId,request);

        return new BaseResponse<>(200,"OK",null);
    }

    // Report
    @GetMapping(value = "/report/reports", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<ReportResponse> getDailyReport(@RequestParam(required = false) Long timestamp) {
        Instant date = Instant.now();
        if (timestamp != null) {
            date = Instant.ofEpochMilli(timestamp);
        }
        return new BaseResponse<>(200, "OK", reportService.getDailyReport(date));
    }
}
