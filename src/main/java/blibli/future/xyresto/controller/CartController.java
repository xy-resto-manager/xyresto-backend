package blibli.future.xyresto.controller;

import blibli.future.xyresto.dto.request.OrderRequest;
import blibli.future.xyresto.dto.response.BaseResponse;
import blibli.future.xyresto.dto.response.CartResponse;
import blibli.future.xyresto.exception.ObjectNotFoundException;
import blibli.future.xyresto.model.User;
import blibli.future.xyresto.service.CartService;
import blibli.future.xyresto.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/api/cart")
public class CartController {

    @Autowired
    private UserService userService;

    @Autowired
    private CartService cartService;

    @GetMapping(value = "/current-cart", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<CartResponse> getCurrentCart(Principal principal) throws ObjectNotFoundException {
        User user = userService.getUserByEmail(principal.getName());
        CartResponse cartResponse = cartService.getUserCart(user);
        return new BaseResponse<>(200, "OK", cartResponse);
    }

    @PutMapping(value = "/current-cart", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<CartResponse> editCart(Principal principal, @Valid @RequestBody OrderRequest orderRequest) throws ObjectNotFoundException {
        User user = userService.getUserByEmail(principal.getName());
        return new BaseResponse<>(200, "OK", cartService.editCart(user, orderRequest));
    }
}
