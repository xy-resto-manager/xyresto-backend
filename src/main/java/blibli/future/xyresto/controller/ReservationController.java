package blibli.future.xyresto.controller;

import blibli.future.xyresto.dto.request.CancelReservationRequest;
import blibli.future.xyresto.dto.request.OrderRequest;
import blibli.future.xyresto.dto.request.ReservationRequest;
import blibli.future.xyresto.dto.response.BaseArrayResponse;
import blibli.future.xyresto.dto.response.BaseResponse;
import blibli.future.xyresto.dto.response.ReservationResponse;
import blibli.future.xyresto.exception.ExpiredDateException;
import blibli.future.xyresto.exception.IsBookedException;
import blibli.future.xyresto.exception.UnauthorizedAccessException;
import blibli.future.xyresto.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/reservation")
public class ReservationController {

    @Autowired
    ReservationService reservationService;


    @PostMapping(value = "/reservations",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<ReservationResponse> createReservation(@RequestBody ReservationRequest request) throws IsBookedException, ExpiredDateException {

        ReservationResponse response = reservationService.createReservation(request);
        return new BaseResponse<>(200,"OK",response);
    }

    @GetMapping(value = "/reservations/{reservationId}",produces =
            MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<ReservationResponse> getReservationById(@PathVariable int reservationId){

        ReservationResponse response = reservationService.getReservationByReservationId(reservationId);
        return new BaseResponse<>(200,"OK",response);
    }

    @PutMapping(value = "/reservations/{reservationId}",consumes = MediaType.APPLICATION_JSON_VALUE,produces =
            MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<Object> cancelReservation(@PathVariable int reservationId,
                                                  @RequestBody CancelReservationRequest request) throws UnauthorizedAccessException {
        if(request.isCanceled()){
            reservationService.cancelReservation(reservationId);
        }

        return new BaseResponse<>(200,"OK",null);
    }

    @GetMapping(value = "/reservations",produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseArrayResponse<ReservationResponse> getAllReservation(){

        List<ReservationResponse> reservation = reservationService.getAllReservations();
        return new BaseArrayResponse<>(200,"OK",reservation);

    }


    @PutMapping(value = "/reservations/{reservationId}/orders")
    public BaseResponse<ReservationResponse> addOrderToReservation(@PathVariable int reservationId,
                                                                   @RequestBody List<OrderRequest> request){

        ReservationResponse response = reservationService.addOrders(reservationId,request);
        return new BaseResponse<>(200,"OK",response);
    }





}
