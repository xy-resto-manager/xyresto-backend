package blibli.future.xyresto.controller;

import blibli.future.xyresto.dto.request.ProductRequest;
import blibli.future.xyresto.dto.response.*;
import blibli.future.xyresto.exception.MalformedRequestParameterException;
import blibli.future.xyresto.exception.ObjectNotFoundException;
import blibli.future.xyresto.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Value("${itemsPerPage:#{10}}")
    private int ITEMS_PER_PAGE;

    public static long totalItems;

    @Autowired
    private ProductService productService;

    @GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseArrayResponse<CategoryResponse> getAllCategories() {
        return new BaseArrayResponse<>(200, "OK", productService.getAllCategories());
    }

    @GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public PaginatedResponse<ProductResponse> getProducts(
            @RequestParam(defaultValue = "1") int page,
            @RequestParam(defaultValue = "popular") String sort,
            @RequestParam(required = false) String query
    ) throws ObjectNotFoundException, MalformedRequestParameterException {
        return new PaginatedResponse<>(200, "OK", productService.getProducts(query, sort, page-1), PaginatedResponse.Pagination.builder()
                .currentPage(page)
                .itemsPerPage(ITEMS_PER_PAGE)
                .totalItems(totalItems)
                .build()
        );
    }

    @GetMapping(value = "/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<ProductResponse> getProduct(@PathVariable("productId") int productId) throws ObjectNotFoundException {
        return new BaseResponse<>(200, "OK", productService.getProduct(productId));
    }
}
