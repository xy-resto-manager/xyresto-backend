package blibli.future.xyresto.controller;

import blibli.future.xyresto.dto.response.ErrorResponse;
import blibli.future.xyresto.dto.response.ExceptionResponse;
import blibli.future.xyresto.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.naming.AuthenticationException;

@RestControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(EmailAlreadyRegisteredException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleEmailAlreadyRegisteredException(EmailAlreadyRegisteredException e) {
        return ErrorResponse.builder()
                .code(400)
                .status("Bad Request")
                .error(new ExceptionResponse(e))
                .build();
    }

    @ExceptionHandler(FailedAuthenticationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleFailedAuthenticationException(FailedAuthenticationException e) {
        return ErrorResponse.builder()
                .code(400)
                .status("Bad Request")
                .error(new ExceptionResponse(e))
                .build();
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleObjectNotFoundException(ObjectNotFoundException e) {
        return ErrorResponse.builder()
                .code(404)
                .status("Not Found")
                .error(new ExceptionResponse(e))
                .build();
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleAuthenticationException(AuthenticationException e){
        return ErrorResponse.builder()
                .code(400)
                .status("Bad Request")
                .error(new ExceptionResponse(e))
                .build();
    }


    @ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleUsernameNotFoundException(UsernameNotFoundException e) {
        return ErrorResponse.builder()
                .code(400)
                .status("Bad Request")
                .error(new ExceptionResponse(e)).build();
    }

    @ExceptionHandler(UnauthorizedAccessException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorResponse handleUnauthorizedAccessException(UnauthorizedAccessException e) {
        return ErrorResponse.builder()
                .code(403)
                .status("Forbidden")
                .error(new ExceptionResponse(e))
                .build();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleAuthenticationException(MethodArgumentNotValidException e){
        return ErrorResponse.builder()
                .code(400)
                .status("Bad Request")
                .error(ExceptionResponse.builder()
                    .error("InvalidRequestException")
                    .message("Input data is invalid.")
                    .build())
                .build();


    }

    @ExceptionHandler(NotSupportedMIMETypeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleNotSupportedMIMEType(NotSupportedMIMETypeException e){
        return ErrorResponse.builder()
                .code(400)
                .status("Bad Request")
                .error(new ExceptionResponse(e))
                .build();
    }

    @ExceptionHandler(MalformedRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleMalformedRequestParameterException(MalformedRequestParameterException e) {
        return ErrorResponse.builder()
                .code(400)
                .status("Bad Request")
                .error(new ExceptionResponse(e))
                .build();
    }

    @ExceptionHandler(IsBookedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleIsBookedException(IsBookedException e){
        return ErrorResponse.builder()
                .code(400)
                .status("Bad Request")
                .error(new ExceptionResponse(e))
                .build();
    }

    @ExceptionHandler(DuplicateKeyException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleDuplicateKeyException(DuplicateKeyException e){
        return ErrorResponse.builder()
                .code(400)
                .status("Bad Request")
                .error(new ExceptionResponse(e))
                .build();
    }

    @ExceptionHandler(ExpiredDateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleExpireDateException(ExpiredDateException e){
        return ErrorResponse.builder()
                .code(400)
                .status("Bad Request")
                .error(new ExceptionResponse(e))
                .build();
    }
}
