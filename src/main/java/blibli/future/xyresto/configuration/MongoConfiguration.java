package blibli.future.xyresto.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackages = "blibli.future.xyresto")
@Configuration
public class MongoConfiguration {
}
