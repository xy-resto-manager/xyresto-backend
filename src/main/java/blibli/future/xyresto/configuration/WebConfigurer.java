package blibli.future.xyresto.configuration;

import blibli.future.xyresto.service.StorageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Paths;

@Configuration
@EnableWebMvc
public class WebConfigurer implements WebMvcConfigurer {

    @Value("${storageLocation:#{null}}")
    private String STORAGE_LOCATION;

    public WebConfigurer() {
        if (STORAGE_LOCATION == null) STORAGE_LOCATION = Paths.get("storage").toAbsolutePath().normalize().toString();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/files/**")
                .addResourceLocations(ResourceUtils.FILE_URL_PREFIX + Paths.get(STORAGE_LOCATION, StorageService.StorageType.BANNER_IMAGE.getLabel()).toString())
                .addResourceLocations(ResourceUtils.FILE_URL_PREFIX + Paths.get(STORAGE_LOCATION, StorageService.StorageType.PRODUCT_IMAGE.getLabel()).toString());
    }
}
