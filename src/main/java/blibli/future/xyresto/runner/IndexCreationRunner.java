package blibli.future.xyresto.runner;

import blibli.future.xyresto.repository.BannerRepository;
import blibli.future.xyresto.util.EmailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(value = "indexCreation", havingValue = "true")
public class IndexCreationRunner implements CommandLineRunner {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void run(String... args) {
        mongoTemplate.indexOps("banner").ensureIndex(
                new Index()
                        .named("bannerId")
                        .on("bannerId", Sort.Direction.ASC)
                        .unique()
        );
        mongoTemplate.indexOps("category").ensureIndex(
                new Index()
                        .named("categoryId")
                        .on("categoryId", Sort.Direction.ASC)
                        .unique()
        );
        mongoTemplate.indexOps("product").ensureIndex(
                new Index()
                        .named("productId")
                        .on("productId", Sort.Direction.ASC)
                        .unique()
        );
        mongoTemplate.indexOps("user").ensureIndex(
                new Index()
                        .named("userId")
                        .on("userId", Sort.Direction.ASC)
                        .unique()

        );
        mongoTemplate.indexOps("table").ensureIndex(
                new Index()
                        .named("tableId")
                        .on("tableId", Sort.Direction.ASC)
                        .unique()
        );
        mongoTemplate.indexOps("reservation").ensureIndex(
                new Index()
                        .named("reservationId")
                        .on("reservationId", Sort.Direction.ASC)
                        .unique()
        );
        mongoTemplate.indexOps("user").ensureIndex(
                new Index()
                        .named("email")
                        .on("email", Sort.Direction.ASC)
                        .unique()

        );
        mongoTemplate.indexOps("product").ensureIndex(
                new TextIndexDefinition.TextIndexDefinitionBuilder()
                        .named("productText")
                        .onField("name")
                        .onField("description")
                        .onField("traits")
                        .build()
        );
    }
}
