package blibli.future.xyresto;

import blibli.future.xyresto.dto.request.*;
import blibli.future.xyresto.dto.response.BaseArrayResponse;
import blibli.future.xyresto.dto.response.BaseResponse;
import blibli.future.xyresto.dto.response.ErrorResponse;
import blibli.future.xyresto.dto.response.ReservationResponse;
import blibli.future.xyresto.dto.response.UserResponse;
import blibli.future.xyresto.exception.ExpiredDateException;
import blibli.future.xyresto.model.*;
import blibli.future.xyresto.repository.*;
import blibli.future.xyresto.service.AdminService;
import blibli.future.xyresto.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.*;
import java.util.stream.IntStream;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = XYRestoApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReservationControllerTests {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    TableRepository tableRepository;

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    CounterRepository counterRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Autowired
    AdminService adminService;

    @Value("${local.server.port}")
    public int port;
    public ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());

    //Category
    public static final int CATEGORY_ID_1 = 1;
    public static final String CATEGORY_NAME_1 = "Seafood";

    public static final int CATEGORY_ID_2 = 2;
    public static final String CATEGORY_NAME_2 = "Beverages";
    //Product
    public static final int PRODUCT_ID_1 = 1;
    public static final String PRODUCT_NAME_1 = "Tempe Penyet";
    public static final String PRODUCT_DESCRIPTION_1 = "Tempe dipenyet sama sambel bawang uenak pol";
    public static final int PRODUCT_PRICE_1 = 20000;
    public static final String PRODUCT_IMAGEURL_1 = "/product/1";
    public static final int PRODUCT_PURCHASECOUNT_1 = 32;
    public static final List<String> PRODUCT_TRAITS_1 = Arrays.asList("SPICY", "SALTY");
    public static final boolean PRODUCT_RECOMMENDED_1 = true;
    public static Category PRODUCT_CATEGORY_1;

    public static final int PRODUCT_ID_2 = 2;
    public static final String PRODUCT_NAME_2 = "Haojek sencingpin";
    public static final String PRODUCT_DESCRIPTION_2 = "Laper? Tongfang";
    public static final int PRODUCT_PRICE_2 = 100000;
    public static final String PRODUCT_IMAGEURL_2 = "/procut/234";
    public static final int PRODUCT_PURCHASECOUNT_2 = 999;
    public static final List<String> PRODUCT_TRAITS_2 = Arrays.asList("MANTAP","JIWA");
    public static final boolean PRODUCT_RECOMMENDED_2 = true;
    public static Category PRODUCT_CATEGORY_2;

    // Table
    public static final int TABLE_ID = 3;
    public static final int X = 4;
    public static final int Y = 4;

    //Reservation
    public ReservationRequest reservationRequest;
    public Instant TIMESTAMP = Instant.now().plus(10, ChronoUnit.HOURS);
    public int QUANTITY = 4;

    //User
    public static final String USER_EMAIL = "user@gmail.com";
    public static final String USER_PASSWORD = "password";
    public static final String USER_FULL_NAME = "User";

    public static final String USER_EMAIL_2 = "nintendo@gmail.com";
    public static final String USER_PASSWORD_2 = "123456789";
    public static final String USER_FULL_NAME_2 = "Nintento Japan";

    public static final String ADMIN_EMAIL = "admin@gmail.com";
    public static final String ADMIN_PASSWORD = "1234567";
    public static final String ADMIN_FULL_NAME = "ADMIN";

    public String userJwtToken;
    public String userJwtToken_2;
    public String adminJwtToken;

    @BeforeEach
    public void setup(){
        RestAssured.port = port;

        reservationRepository.deleteAll();
        userRepository.deleteAll();
        categoryRepository.deleteAll();
        productRepository.deleteAll();
        tableRepository.deleteAll();
        counterRepository.deleteAll();


        Category category_1 = Category.builder()
                .categoryId(CATEGORY_ID_1)
                .name(CATEGORY_NAME_1)
                .build();
        Category category_2 = Category.builder()
                .categoryId(CATEGORY_ID_2)
                .name(CATEGORY_NAME_2)
                .build();

        categoryRepository.save(category_1);
        categoryRepository.save(category_2);

        PRODUCT_CATEGORY_1 = category_1;
        PRODUCT_CATEGORY_2 = category_2;

        Product product_1 = Product.builder()
                .productId(PRODUCT_ID_1)
                .name(PRODUCT_NAME_1)
                .traits(PRODUCT_TRAITS_1)
                .purchaseCount(PRODUCT_PURCHASECOUNT_1)
                .imageUrl(PRODUCT_IMAGEURL_1)
                .price(PRODUCT_PRICE_1)
                .recommended(PRODUCT_RECOMMENDED_1)
                .description(PRODUCT_DESCRIPTION_1)
                .category(PRODUCT_CATEGORY_1)
                .build();

        Product product_2 = Product.builder()
                .productId(PRODUCT_ID_2)
                .name(PRODUCT_NAME_2)
                .traits(PRODUCT_TRAITS_2)
                .purchaseCount(PRODUCT_PURCHASECOUNT_2)
                .imageUrl(PRODUCT_IMAGEURL_2)
                .price(PRODUCT_PRICE_2)
                .recommended(PRODUCT_RECOMMENDED_2)
                .description(PRODUCT_DESCRIPTION_2)
                .category(PRODUCT_CATEGORY_2)
                .build();

        productRepository.save(product_1);
        productRepository.save(product_2);

        Table table = Table.builder()
                .x(X)
                .y(Y)
                .tableId(TABLE_ID)
                .build();
        tableRepository.save(table);

        RegisterRequest userRegisterRequest = RegisterRequest.builder()
                .email(USER_EMAIL)
                .password(USER_PASSWORD)
                .fullName(USER_FULL_NAME)
                .build();
        userService.register(userRegisterRequest);
        ValidatableResponse userResponse = RestAssured.given().contentType(ContentType.JSON)
                .body(LoginRequest.builder()
                        .email(USER_EMAIL)
                        .password(USER_PASSWORD)
                        .build())
                .post("/api/user/_login")
                .then();
        userJwtToken = userResponse.extract().cookie("jwtToken");

        // Kenapa kok pakai user2? soalnya kalau ad user lain lancang ingin nambah order user orang lain
        // Kenapa kok ngga pakai admin ae? soale admin ya emang dibuat supaya bisa add order e orang (hp ne user e
        // mati or smthng pas dining)

        RegisterRequest userRegisterRequest_2 = RegisterRequest.builder()
                .email(USER_EMAIL_2)
                .password(USER_PASSWORD_2)
                .fullName(USER_FULL_NAME_2)
                .build();
        userService.register(userRegisterRequest_2);
        ValidatableResponse userResponse_2 = RestAssured.given().contentType(ContentType.JSON)
                .body(LoginRequest.builder()
                        .email(USER_EMAIL_2)
                        .password(USER_PASSWORD_2)
                        .build())
                .post("/api/user/_login")
                .then();
        userJwtToken_2 = userResponse_2.extract().cookie("jwtToken");

        RegisterRequest adminRegisterRequest = RegisterRequest.builder()
            .email(ADMIN_EMAIL)
            .password(ADMIN_PASSWORD)
            .fullName(ADMIN_FULL_NAME)
            .build();
        UserResponse adminUserResponse = userService.register(adminRegisterRequest);
        adminService.editRoles(
            adminUserResponse.getId(),
            ChangeRoleRequest.builder()
                .role("USER")
                .role("ADMIN")
                .build()
        );
        ValidatableResponse adminResponse = RestAssured.given().contentType(ContentType.JSON)
            .body(LoginRequest.builder()
                .email(ADMIN_EMAIL)
                .password(ADMIN_PASSWORD)
                .build())
            .post("/api/user/_login")
            .then();
        adminJwtToken = adminResponse.extract().cookie("jwtToken");


        List<OrderRequest> requestList = new ArrayList<>();
        requestList.add(OrderRequest.builder().productId(PRODUCT_ID_1).quantity(QUANTITY).build());
        requestList.add(OrderRequest.builder().productId(PRODUCT_ID_2).quantity(QUANTITY).build());


        reservationRequest = ReservationRequest.builder()
                .timestamp(TIMESTAMP.toEpochMilli())
                .tableId(TABLE_ID)
                .orders(requestList)
                .build();
    }

    @Test
    public void getAllReservations_success_returnBaseResponse() throws JsonProcessingException {
        int amount = 5;
        IntStream.range(0, amount).forEach(i -> RestAssured.given().contentType(ContentType.JSON)
            .body(reservationRequest)
            .header("Authorization", "Bearer " + userJwtToken)
            .post("/api/reservation/reservations")
            .then()
        );

        ValidatableResponse validatableResponse =
            RestAssured.given().header("Authorization" ,
                "Bearer " + userJwtToken).get("/api/reservation/reservations").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseArrayResponse.class,
            ReservationResponse.class);

        BaseArrayResponse<ReservationResponse> response = objectMapper.readValue(validatableResponse.extract().asString(),
            type);

        Assertions.assertEquals(200, response.getCode());
        Assertions.assertEquals(amount, response.getData().size());
    }

    @Test
    public void createReservation_success_returnBaseResponse() throws JsonProcessingException {

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class,
                ReservationResponse.class);

        BaseResponse<ReservationResponse> response = objectMapper.readValue(validatableResponse.extract().asString(),
                type);

        Assertions.assertEquals(200,response.getCode());
        Assertions.assertEquals(TABLE_ID,response.getData().getTableId());
        Assertions.assertEquals(USER_EMAIL,response.getData().getEmail());
        Assertions.assertEquals(Reservation.Status.PENDING,response.getData().getStatus());
        Assertions.assertEquals(TIMESTAMP.toEpochMilli(),response.getData().getTimestamp());
        Assertions.assertEquals(PRODUCT_NAME_1,response.getData().getOrders().get(0).getProduct().getName());
        Assertions.assertEquals(QUANTITY,response.getData().getOrders().get(0).getQuantity());
        Assertions.assertEquals(PRODUCT_NAME_2,response.getData().getOrders().get(1).getProduct().getName());
        Assertions.assertEquals(QUANTITY,response.getData().getOrders().get(1).getQuantity());

        reservationRequest.setOrders(new ArrayList<>());

        validatableResponse =
            RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                "Bearer " + userJwtToken_2).post("/api/reservation/reservations").then();

        response = objectMapper.readValue(validatableResponse.extract().asString(), type);

        Assertions.assertEquals(200,response.getCode());
        Assertions.assertEquals(TABLE_ID,response.getData().getTableId());
        Assertions.assertEquals(USER_EMAIL_2,response.getData().getEmail());
        Assertions.assertEquals(Reservation.Status.PENDING,response.getData().getStatus());
        Assertions.assertEquals(TIMESTAMP.toEpochMilli(),response.getData().getTimestamp());
        Assertions.assertEquals(0, response.getData().getOrders().size());
    }

    @Test
    public void createReservationAdmin_success_returnBaseResponse() throws JsonProcessingException {

        ValidatableResponse validatableResponse =
            RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                "Bearer " + adminJwtToken).post("/api/reservation/reservations").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class,
            ReservationResponse.class);

        BaseResponse<ReservationResponse> response = objectMapper.readValue(validatableResponse.extract().asString(),
            type);

        Assertions.assertEquals(200,response.getCode());
        Assertions.assertEquals(TABLE_ID,response.getData().getTableId());
        Assertions.assertEquals(ADMIN_EMAIL,response.getData().getEmail());
        Assertions.assertEquals(Reservation.Status.APPROVED,response.getData().getStatus());
        Assertions.assertEquals(TIMESTAMP.toEpochMilli(),response.getData().getTimestamp());
        Assertions.assertEquals(PRODUCT_NAME_1,response.getData().getOrders().get(0).getProduct().getName());
        Assertions.assertEquals(QUANTITY,response.getData().getOrders().get(0).getQuantity());
        Assertions.assertEquals(PRODUCT_NAME_2,response.getData().getOrders().get(1).getProduct().getName());
        Assertions.assertEquals(QUANTITY,response.getData().getOrders().get(1).getQuantity());

    }

    @Test
    public void createReservation_failed_notAuthenticated_returnErrorResponse() throws JsonProcessingException {

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest)
                        .post("/api/reservation/reservations").then();

        ErrorResponse errorResponse = objectMapper.readValue(validatableResponse.extract().asString(),
                ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());

    }

    @Test
    public void createReservation_failed_duplicateKey_returnErrorResponse() throws JsonProcessingException {

        ValidatableResponse addDuplicate =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        Reservation reservation = reservationRepository.findByReservationId(1).orElse(null);
        if(reservation != null){
            reservation.setStatus(Reservation.Status.APPROVED);
            reservationRepository.save(reservation);
        }

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        ErrorResponse errorResponse = objectMapper.readValue(validatableResponse.extract().asString(),
                ErrorResponse.class);
        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, errorResponse.getCode());

    }

    @Test
    public void createReservation_failed_expiredDate_returnErrorResponse() throws JsonProcessingException {
        reservationRequest.setTimestamp(Instant.now().minus(1, ChronoUnit.HOURS).toEpochMilli());

        ValidatableResponse validatableResponse =
            RestAssured.given().contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + userJwtToken)
                .body(reservationRequest)
                .post("/api/reservation/reservations").then();

        ErrorResponse errorResponse = objectMapper.readValue(validatableResponse.extract().asString(),
            ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, errorResponse.getCode());

        boolean hasExpiredTimeException = errorResponse.getErrors().stream()
            .anyMatch(exceptionResponse -> exceptionResponse
                .getError()
                .equals(ExpiredDateException.class.getSimpleName())
            );

        Assertions.assertTrue(hasExpiredTimeException);
    }

    @Test
    public void cancelReservation_success_returnBaseResponse() throws JsonProcessingException {

        CancelReservationRequest request = CancelReservationRequest.builder().canceled(true).build();

        ValidatableResponse addReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        ValidatableResponse validatableResponse = RestAssured.given().contentType(ContentType.JSON).header("Authorization" ,
                "Bearer " + userJwtToken).body(request).put("/api" +
                "/reservation/reservations/1").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class,
                Object.class);
        BaseResponse<Object> response = objectMapper.readValue(validatableResponse.extract().asString(),
                type);

        Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());
    }

    @Test
    public void cancelReservation_failed_wrongUser_returnErrorResponse() throws JsonProcessingException {

        CancelReservationRequest request = CancelReservationRequest.builder().canceled(true).build();

        ValidatableResponse addReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON).header("Authorization" ,
                "Bearer " + userJwtToken_2).body(request).put("/api" +
                "/reservation/reservations/1").then();

        ErrorResponse response = objectMapper.readValue(validatableResponse.extract().asString(),ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, response.getCode());
    }

    @Test
    public void addOrderToReservation_success_returnBaseResponse() throws JsonProcessingException {
        reservationRequest.getOrders().remove(0);

        ValidatableResponse addReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        List<OrderRequest> orderRequests = new ArrayList<>();
        orderRequests.add(OrderRequest.builder().productId(PRODUCT_ID_1).quantity(QUANTITY).build());
        orderRequests.add(OrderRequest.builder().productId(PRODUCT_ID_2).quantity(QUANTITY).build());

        ValidatableResponse validatableResponse = RestAssured.given().contentType(ContentType.JSON).body(orderRequests).header(
                "Authorization", "Bearer "+ userJwtToken).put("/api/reservation/reservations" +
                "/1/orders").then();


        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class,
                ReservationResponse.class);

        BaseResponse<ReservationResponse> response = objectMapper.readValue(validatableResponse.extract().asString(),
                type);

        Assertions.assertEquals(HttpStatus.SC_OK,response.getCode());
        Assertions.assertEquals(2, response.getData().getOrders().size());
        Assertions.assertEquals(QUANTITY, response.getData().getOrders().stream().filter(
            orderedProductResponse -> orderedProductResponse.getProduct().getId() == PRODUCT_ID_1
        ).findFirst().get().getQuantity());
        Assertions.assertEquals(2 * QUANTITY, response.getData().getOrders().stream().filter(
            orderedProductResponse -> orderedProductResponse.getProduct().getId() == PRODUCT_ID_2
        ).findFirst().get().getQuantity());
    }

    @Test
    public void addOrderToReservation_failed_wrongUser_returnErrorResponse() throws JsonProcessingException {

        ValidatableResponse addReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        List<OrderRequest> orderRequests = new ArrayList<>();
        orderRequests.add(OrderRequest.builder().productId(PRODUCT_ID_1).quantity(QUANTITY).build());
        orderRequests.add(OrderRequest.builder().productId(PRODUCT_ID_2).quantity(QUANTITY).build());


        ValidatableResponse validatableResponse = RestAssured.given().contentType(ContentType.JSON).body(orderRequests).header(
                "Authorization", "Bearer "+ userJwtToken_2).put("/api/reservation/reservations" +
                "/1/orders").then();

        ErrorResponse errorResponse = objectMapper.readValue(validatableResponse.extract().asString(),
                ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN,errorResponse.getCode());
    }

    @Test
    public void getReservationById_success_userRolesIsUser_returnBaseResponse() throws JsonProcessingException {

        ValidatableResponse addReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        ValidatableResponse validatableResponse = RestAssured.given().contentType(ContentType.JSON).header(
                "Authorization", "Bearer "+ userJwtToken).get("/api/reservation/reservations/1").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class,
                ReservationResponse.class);

        BaseResponse<ReservationResponse> response = objectMapper.readValue(validatableResponse.extract().asString(),
                type);

        Assertions.assertEquals(HttpStatus.SC_OK,response.getCode());
    }

    @Test
    public void getReservationById_success_userRolesIsUserAdmin_returnBaseResponse() throws JsonProcessingException {

        ValidatableResponse addReservation =
            RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        User secondUser = userRepository.findByEmail(USER_EMAIL_2).orElse(null);
        Assertions.assertNotNull(secondUser);

        secondUser.setRoles(Arrays.asList("USER", "ADMIN"));
        userRepository.save(secondUser);

        ValidatableResponse validatableResponse = RestAssured.given().contentType(ContentType.JSON).header(
            "Authorization", "Bearer "+ userJwtToken_2).get("/api/reservation/reservations/1").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class,
            ReservationResponse.class);

        BaseResponse<ReservationResponse> response = objectMapper.readValue(validatableResponse.extract().asString(),
            type);

        Assertions.assertEquals(HttpStatus.SC_OK,response.getCode());
    }

    @Test
    public void getReservationById_failed_wrongUser_returnBaseResponse() throws JsonProcessingException {

        ValidatableResponse addReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        ValidatableResponse validatableResponse = RestAssured.given().contentType(ContentType.JSON).header(
                "Authorization", "Bearer "+ userJwtToken_2).get("/api/reservation/reservations/1").then();

        ErrorResponse response = objectMapper.readValue(validatableResponse.extract().asString(),ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN,response.getCode());
    }

}