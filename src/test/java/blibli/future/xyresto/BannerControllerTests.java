package blibli.future.xyresto;

import blibli.future.xyresto.dto.response.BannerResponse;
import blibli.future.xyresto.dto.response.BaseArrayResponse;
import blibli.future.xyresto.dto.response.BaseResponse;
import blibli.future.xyresto.dto.response.ReportResponse;
import blibli.future.xyresto.repository.BannerRepository;
import blibli.future.xyresto.repository.CounterRepository;
import blibli.future.xyresto.service.BannerService;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.MultiPartSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = XYRestoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BannerControllerTests {

    @Autowired
    BannerService bannerService;

    @Autowired
    CounterRepository counterRepository;
    @Autowired
    BannerRepository bannerRepository;

    @Value("${local.server.port}")
    private int port;
    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setUp() throws IOException {
        RestAssured.port = port;

        counterRepository.deleteAll();
        bannerRepository.deleteAll();
    }

    @Test
    public void getAllBanners_success_returnBaseArrayResponse() throws IOException {
        File file = new File(getClass().getClassLoader().getResource("uploadtest.jpeg").getFile());
        Assertions.assertTrue(file.exists());

        bannerService.createBanner(new MockMultipartFile("test.jpeg", "test.jpeg", "image/jpeg", Files.readAllBytes(file.toPath())));

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON).get("/api/banner/banners").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseArrayResponse.class, BannerResponse.class);
        BaseArrayResponse<BannerResponse> bannerResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, bannerResponse.getCode());
        Assertions.assertEquals(1, bannerResponse.getData().size());
        Assertions.assertEquals(1, bannerResponse.getData().get(0).getId());
        Assertions.assertEquals("/files/banners/1", bannerResponse.getData().get(0).getImageUrl());

        byte[] image = RestAssured.given()
                .get(bannerResponse.getData().get(0).getImageUrl())
                .asByteArray();

        Assertions.assertArrayEquals(Files.readAllBytes(file.toPath()), image);
    }

    @Test
    public void getBanner_success_returnBaseResponse() throws IOException {
        File file = new File(getClass().getClassLoader().getResource("uploadtest.jpeg").getFile());
        Assertions.assertTrue(file.exists());

        bannerService.createBanner(new MockMultipartFile("test.jpeg", "test.jpeg", "image/jpeg", Files.readAllBytes(file.toPath())));

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON).get("/api/banner/1").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, BannerResponse.class);
        BaseResponse<BannerResponse> bannerResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, bannerResponse.getCode());
        Assertions.assertEquals(1, bannerResponse.getData().getId());
        Assertions.assertEquals("/files/banners/1", bannerResponse.getData().getImageUrl());

        byte[] image = RestAssured.given()
                .get(bannerResponse.getData().getImageUrl())
                .asByteArray();

        Assertions.assertArrayEquals(Files.readAllBytes(file.toPath()), image);
    }
}
