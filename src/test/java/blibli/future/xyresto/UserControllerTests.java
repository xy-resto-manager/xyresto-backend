package blibli.future.xyresto;

import blibli.future.xyresto.dto.request.ChangePasswordRequest;
import blibli.future.xyresto.dto.request.LoginRequest;
import blibli.future.xyresto.dto.request.RegisterRequest;
import blibli.future.xyresto.dto.response.BaseResponse;
import blibli.future.xyresto.dto.response.ErrorResponse;
import blibli.future.xyresto.dto.response.LoginResponse;
import blibli.future.xyresto.dto.response.UserResponse;
import blibli.future.xyresto.repository.CounterRepository;
import blibli.future.xyresto.repository.UserRepository;
import blibli.future.xyresto.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Cookie;
import com.jayway.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = XYRestoApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTests {

    public static final String FULL_NAME = "Nintendo";
    public static final String EMAIL = "nintendojp@gmail.com";
    public static final String PASSWORD = "123456";

    private RegisterRequest registerRequest;
    private LoginRequest loginRequest;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Value("${local.server.port}")
    private int port;
    @Autowired
    UserService userService;

    @Autowired
    CounterRepository counterRepository;

    @Autowired
    UserRepository userRepository;


    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
        registerRequest =
                RegisterRequest.builder().fullName(FULL_NAME).email(EMAIL).password(PASSWORD).build();
        loginRequest =
                LoginRequest.builder().email(EMAIL).password(PASSWORD).build();

        counterRepository.deleteAll();
        userRepository.deleteAll();

    }

    @Test
    public void _register_success_returnBaseResponse() throws JsonProcessingException {


        ValidatableResponse res = RestAssured.given().contentType("application/json").body(registerRequest).post(
                "/api/user/_register").then();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, UserResponse.class);
        BaseResponse<UserResponse> userResponse = objectMapper.readValue(res.extract().asString(),type);
        Assertions.assertEquals(FULL_NAME, userResponse.getData().getFullName());

    }


    @Test
    public void _register_failed_returnEmailAlreadyExistException() throws  JsonProcessingException{

        userService.register(registerRequest);

        ValidatableResponse res =
                RestAssured.given().contentType("application/json").body(registerRequest).post("/api/user" +
                        "/_register").then();
        ErrorResponse errorResponse = objectMapper.readValue(res.extract().asString(),ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, errorResponse.getCode());

    }

    @Test
    public void _login_success_returnBaseResponse() throws JsonProcessingException {

        userService.register(registerRequest);
        ValidatableResponse res =
                RestAssured.given().contentType("application/json").body(loginRequest).post("api/user/_login").then();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class,LoginResponse.class);
        BaseResponse<LoginResponse> userResponse = objectMapper.readValue(res.extract().asString(),type);
        Assertions.assertEquals(EMAIL, userResponse.getData().getEmail());

    }

    @Test
    public void _login_failed_returnFailedAuthenticationException() throws JsonProcessingException{

        userService.register(registerRequest);
        loginRequest.setEmail("nintendo-123@gmail.com");
        ValidatableResponse res = RestAssured.given().contentType("application/json").body(loginRequest).post("/api" +
                "/user/_login").then();
        ErrorResponse errorResponse = objectMapper.readValue(res.extract().asString(),ErrorResponse.class);
        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST,errorResponse.getCode());
    }

    @Test
    public void _changePassword_success_returnBaseResponse() throws JsonProcessingException {

        userService.register(registerRequest);

        //Login to get the token
        ValidatableResponse validLoginRes = RestAssured.given().contentType("application/json").body(loginRequest).post(
                "/api/user/_login").then();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class,LoginResponse.class);
        BaseResponse<LoginResponse> loginRes = objectMapper.readValue(validLoginRes.extract().asString(),type);
        String jwtToken = loginRes.getData().getToken();

        // Try to change password
        ChangePasswordRequest changePasswordRequest =
                ChangePasswordRequest.builder().oldPassword(PASSWORD).newPassword("1234567").build();
        ValidatableResponse res =
                RestAssured.given().contentType("application/json").body(changePasswordRequest).header("Authorization","Bearer " + jwtToken).post(
                        "/api/user" +
                        "/_change-password").then();
        BaseResponse finalResponse = objectMapper.readValue(res.extract().asString(),BaseResponse.class);

        Assertions.assertEquals(HttpStatus.SC_OK,finalResponse.getCode());
    }

    @Test
    public void _changePassword_failed_returnFailedAuthenticationException() throws JsonProcessingException{

        final String WRONG_PASSWORD = "123456789";

        userService.register(registerRequest);
        //Login to get the token
        ValidatableResponse validLoginRes = RestAssured.given().contentType("application/json").body(loginRequest).post(
                "/api/user/_login").then();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class,LoginResponse.class);
        BaseResponse<LoginResponse> loginRes = objectMapper.readValue(validLoginRes.extract().asString(),type);
        String jwtToken = loginRes.getData().getToken();

        ChangePasswordRequest changePasswordRequest =
                ChangePasswordRequest.builder().oldPassword(WRONG_PASSWORD).newPassword("1234567").build();
        ValidatableResponse res =
                RestAssured.given().contentType("application/json").body(changePasswordRequest).header("Authorization","Bearer " + jwtToken).post(
                        "/api/user" +
                                "/_change-password").then();
        ErrorResponse errorResponse = objectMapper.readValue(res.extract().asString(),ErrorResponse.class);
        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST,errorResponse.getCode());
    }

    @Test
    public void _logout_success_returnBaseResponse() throws JsonProcessingException {

        final String TOKEN_KEY = "jwtToken";

        userService.register(registerRequest);
        //Login to get the token
        ValidatableResponse validLoginRes = RestAssured.given().contentType("application/json").body(loginRequest).post(
                "/api/user/_login").then();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class,LoginResponse.class);
        BaseResponse<LoginResponse> loginRes = objectMapper.readValue(validLoginRes.extract().asString(),type);
        String jwtToken = loginRes.getData().getToken();

        Cookie cookie = new Cookie.Builder(TOKEN_KEY,jwtToken).build();


        ValidatableResponse res =
                RestAssured.given().contentType("application/json").cookie(cookie).post("/api/user/_logout").then();

        BaseResponse baseResponse = objectMapper.readValue(res.extract().asString(),BaseResponse.class);

        Assertions.assertEquals(HttpStatus.SC_OK,baseResponse.getCode());
    }

    @Test
    public void getCurrentUser_success_returnBaseResponse() throws JsonProcessingException {
        userService.register(registerRequest);
        //Login to get the token
        ValidatableResponse validLoginRes = RestAssured.given().contentType("application/json").body(loginRequest).post(
                "/api/user/_login").then();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, LoginResponse.class);
        BaseResponse<LoginResponse> loginRes = objectMapper.readValue(validLoginRes.extract().asString(), type);
        String jwtToken = loginRes.getData().getToken();

        ValidatableResponse res = RestAssured.given()
                .contentType("application/json")
                .cookie("jwtToken", jwtToken)
                .cookie("somethingElse", "randomCookie")
                .get("/api/user/current-user")
                .then();
        type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, UserResponse.class);
        BaseResponse<UserResponse> finalResponse = objectMapper.readValue(res.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, finalResponse.getCode());
        Assertions.assertEquals(EMAIL, finalResponse.getData().getEmail());
    }

    @Test
    public void getCurrentUser_failed_notLoggedIn_returnErrorResponse() throws JsonProcessingException {
        ValidatableResponse res = RestAssured.given()
            .contentType("application/json")
            .get("/api/user/current-user")
            .then();
        ErrorResponse finalResponse = objectMapper.readValue(res.extract().asString(), ErrorResponse.class);
        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, finalResponse.getCode());
        boolean hasError = finalResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(UsernameNotFoundException.class.getSimpleName()));
        Assertions.assertTrue(hasError);
    }

}