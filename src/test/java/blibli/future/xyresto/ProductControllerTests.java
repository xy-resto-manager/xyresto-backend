package blibli.future.xyresto;

import blibli.future.xyresto.dto.response.*;
import blibli.future.xyresto.exception.MalformedRequestParameterException;
import blibli.future.xyresto.exception.ObjectNotFoundException;
import blibli.future.xyresto.model.Category;
import blibli.future.xyresto.model.Product;
import blibli.future.xyresto.repository.CategoryRepository;
import blibli.future.xyresto.repository.ProductRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = XYRestoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerTests {

    public static final int CATEGORY_ID_1 = 1;
    public static final String CATEGORY_NAME_1 = "Seafood";

    public static final int CATEGORY_ID_2 = 2;
    public static final String CATEGORY_NAME_2 = "Beverages";

    public static final String PRODUCT_QUERY = "tempe";

    public static final int PRODUCT_ID_1 = 1;
    public static final String PRODUCT_NAME_1 = "Tempe Penyet";
    public static final String PRODUCT_DESCRIPTION_1 = "Tempe dipenyet sama sambel bawang uenak pol";
    public static final int PRODUCT_PRICE_1 = 20000;
    public static final String PRODUCT_IMAGEURL_1 = "/product/1";
    public static final int PRODUCT_PURCHASECOUNT_1 = 32;
    public static final List<String> PRODUCT_TRAITS_1 = Arrays.asList("SPICY", "SALTY");
    public static final boolean PRODUCT_RECOMMENDED_1 = true;
    public static Category PRODUCT_CATEGORY_1;

    public static final int PRODUCT_ID_2 = 2;
    public static final String PRODUCT_NAME_2 = "Ayam Geprek";
    public static final String PRODUCT_DESCRIPTION_2 = "Ayam digeprek wkwk - tambah keyword tempe";
    public static final int PRODUCT_PRICE_2 = 35000;
    public static final String PRODUCT_IMAGEURL_2 = "/product/2";
    public static final int PRODUCT_PURCHASECOUNT_2 = 12;
    public static final List<String> PRODUCT_TRAITS_2 = Arrays.asList("SPICY");
    public static final boolean PRODUCT_RECOMMENDED_2 = false;
    public static Category PRODUCT_CATEGORY_2;

    public static final int PRODUCT_ID_3 = 3;
    public static final String PRODUCT_NAME_3 = "Coba lainnya";
    public static final String PRODUCT_DESCRIPTION_3 = "Bla bla blalalsda";
    public static final int PRODUCT_PRICE_3 = 35000;
    public static final String PRODUCT_IMAGEURL_3 = "/product/3";
    public static final int PRODUCT_PURCHASECOUNT_3 = 22;
    public static final List<String> PRODUCT_TRAITS_3 = Arrays.asList("TEMPE");
    public static final boolean PRODUCT_RECOMMENDED_3 = false;
    public static Category PRODUCT_CATEGORY_3;

    public static final int PRODUCT_ID_4 = 4;
    public static final String PRODUCT_NAME_4 = "Tanpa keyword";
    public static final String PRODUCT_DESCRIPTION_4 = "Bla bla blalalsda";
    public static final int PRODUCT_PRICE_4 = 11000;
    public static final String PRODUCT_IMAGEURL_4 = "/product/4";
    public static final int PRODUCT_PURCHASECOUNT_4 = 14;
    public static final List<String> PRODUCT_TRAITS_4 = new ArrayList<>();
    public static final boolean PRODUCT_RECOMMENDED_4 = true;
    public static Category PRODUCT_CATEGORY_4;

    @Value("${local.server.port}")
    private int port;
    private ObjectMapper objectMapper;

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductRepository productRepository;

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
        objectMapper = new ObjectMapper();

        categoryRepository.deleteAll();
        productRepository.deleteAll();

        Category category1 = Category.builder()
                .categoryId(CATEGORY_ID_1)
                .name(CATEGORY_NAME_1)
                .build();
        Category category2 = Category.builder()
                .categoryId(CATEGORY_ID_2)
                .name(CATEGORY_NAME_2)
                .build();

        categoryRepository.save(category1);
        categoryRepository.save(category2);

        PRODUCT_CATEGORY_1 = category1;
        PRODUCT_CATEGORY_2 = category1;
        PRODUCT_CATEGORY_3 = category1;
        PRODUCT_CATEGORY_4 = category2;

        Product product1 = Product.builder()
                .productId(PRODUCT_ID_1)
                .name(PRODUCT_NAME_1)
                .description(PRODUCT_DESCRIPTION_1)
                .price(PRODUCT_PRICE_1)
                .imageUrl(PRODUCT_IMAGEURL_1)
                .purchaseCount(PRODUCT_PURCHASECOUNT_1)
                .traits(PRODUCT_TRAITS_1)
                .recommended(PRODUCT_RECOMMENDED_1)
                .category(PRODUCT_CATEGORY_1)
                .build();
        Product product2 = Product.builder()
                .productId(PRODUCT_ID_2)
                .name(PRODUCT_NAME_2)
                .description(PRODUCT_DESCRIPTION_2)
                .price(PRODUCT_PRICE_2)
                .imageUrl(PRODUCT_IMAGEURL_2)
                .purchaseCount(PRODUCT_PURCHASECOUNT_2)
                .traits(PRODUCT_TRAITS_2)
                .recommended(PRODUCT_RECOMMENDED_2)
                .category(PRODUCT_CATEGORY_2)
                .build();
        Product product3 = Product.builder()
                .productId(PRODUCT_ID_3)
                .name(PRODUCT_NAME_3)
                .description(PRODUCT_DESCRIPTION_3)
                .price(PRODUCT_PRICE_3)
                .imageUrl(PRODUCT_IMAGEURL_3)
                .purchaseCount(PRODUCT_PURCHASECOUNT_3)
                .traits(PRODUCT_TRAITS_3)
                .recommended(PRODUCT_RECOMMENDED_3)
                .category(PRODUCT_CATEGORY_3)
                .build();
        Product product4 = Product.builder()
                .productId(PRODUCT_ID_4)
                .name(PRODUCT_NAME_4)
                .description(PRODUCT_DESCRIPTION_4)
                .price(PRODUCT_PRICE_4)
                .imageUrl(PRODUCT_IMAGEURL_4)
                .purchaseCount(PRODUCT_PURCHASECOUNT_4)
                .traits(PRODUCT_TRAITS_4)
                .recommended(PRODUCT_RECOMMENDED_4)
                .category(PRODUCT_CATEGORY_4)
                .build();

        productRepository.save(product1);
        productRepository.save(product2);
        productRepository.save(product3);
        productRepository.save(product4);
    }

    @Test
    public void getAllCategories_success_returnBaseArrayResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .get("/api/product/categories")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseArrayResponse.class, CategoryResponse.class);
        BaseArrayResponse<CategoryResponse> categoryResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, categoryResponse.getCode());

        CategoryResponse category1 = categoryResponse.getData().get(0);
        Assertions.assertNotNull(category1);
        Assertions.assertEquals(CATEGORY_ID_1, category1.getId());
        Assertions.assertEquals(CATEGORY_NAME_1, category1.getName());

        CategoryResponse category2 = categoryResponse.getData().get(1);
        Assertions.assertNotNull(category2);
        Assertions.assertEquals(CATEGORY_ID_2, category2.getId());
        Assertions.assertEquals(CATEGORY_NAME_2, category2.getName());
    }

    @Test
    public void getProducts_all_success_returnPaginatedResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .get("/api/product/products")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(PaginatedResponse.class, ProductResponse.class);
        PaginatedResponse<ProductResponse> productResponse = objectMapper.readValue(response.extract().asString(), type);
        System.out.println(response.extract().asString());

        Assertions.assertEquals(HttpStatus.SC_OK, productResponse.getCode());
        Assertions.assertEquals(4, productResponse.getData().size());

        ProductResponse response1 = productResponse.getData().get(0);
        Assertions.assertEquals(PRODUCT_ID_1, response1.getId());
        Assertions.assertEquals(PRODUCT_NAME_1, response1.getName());
        Assertions.assertEquals(PRODUCT_PRICE_1, response1.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_1, response1.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_1, response1.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_1, response1.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_1, response1.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_1.getCategoryId(), response1.getCategory().getId());

        ProductResponse response2 = productResponse.getData().get(1);
        Assertions.assertEquals(PRODUCT_ID_3, response2.getId());
        Assertions.assertEquals(PRODUCT_NAME_3, response2.getName());
        Assertions.assertEquals(PRODUCT_PRICE_3, response2.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_3, response2.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_3, response2.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_3, response2.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_3, response2.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_3.getCategoryId(), response2.getCategory().getId());

        ProductResponse response3 = productResponse.getData().get(2);
        Assertions.assertEquals(PRODUCT_ID_4, response3.getId());
        Assertions.assertEquals(PRODUCT_NAME_4, response3.getName());
        Assertions.assertEquals(PRODUCT_PRICE_4, response3.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_4, response3.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_4, response3.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_4, response3.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_4, response3.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_4.getCategoryId(), response3.getCategory().getId());

        ProductResponse response4 = productResponse.getData().get(3);
        Assertions.assertEquals(PRODUCT_ID_2, response4.getId());
        Assertions.assertEquals(PRODUCT_NAME_2, response4.getName());
        Assertions.assertEquals(PRODUCT_PRICE_2, response4.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_2, response4.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_2, response4.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_2, response4.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_2, response4.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_2.getCategoryId(), response4.getCategory().getId());
    }

    // Needs index:
    // db.getCollection("product").createIndex({ "name": "text", "description": "text", "traits": "text" })
    @Test
    public void getProducts_allWithQuery_success_returnPaginatedResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .get("/api/product/products?query=" + PRODUCT_QUERY)
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(PaginatedResponse.class, ProductResponse.class);
        PaginatedResponse<ProductResponse> productResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, productResponse.getCode());
        Assertions.assertEquals(3, productResponse.getData().size());

        ProductResponse response1 = productResponse.getData().get(0);
        Assertions.assertEquals(PRODUCT_ID_1, response1.getId());
        Assertions.assertEquals(PRODUCT_NAME_1, response1.getName());
        Assertions.assertEquals(PRODUCT_PRICE_1, response1.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_1, response1.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_1, response1.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_1, response1.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_1, response1.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_1.getCategoryId(), response1.getCategory().getId());

        ProductResponse response2 = productResponse.getData().get(1);
        Assertions.assertEquals(PRODUCT_ID_3, response2.getId());
        Assertions.assertEquals(PRODUCT_NAME_3, response2.getName());
        Assertions.assertEquals(PRODUCT_PRICE_3, response2.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_3, response2.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_3, response2.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_3, response2.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_3, response2.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_3.getCategoryId(), response2.getCategory().getId());

        ProductResponse response3 = productResponse.getData().get(2);
        Assertions.assertEquals(PRODUCT_ID_2, response3.getId());
        Assertions.assertEquals(PRODUCT_NAME_2, response3.getName());
        Assertions.assertEquals(PRODUCT_PRICE_2, response3.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_2, response3.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_2, response3.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_2, response3.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_2, response3.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_2.getCategoryId(), response3.getCategory().getId());
    }

    @Test
    public void getProducts_recommended_success_returnPaginatedResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .get("/api/product/products?sort=recommended")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(PaginatedResponse.class, ProductResponse.class);
        PaginatedResponse<ProductResponse> productResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, productResponse.getCode());
        Assertions.assertEquals(2, productResponse.getData().size());

        ProductResponse response1 = productResponse.getData().get(0);
        Assertions.assertEquals(PRODUCT_ID_1, response1.getId());
        Assertions.assertEquals(PRODUCT_NAME_1, response1.getName());
        Assertions.assertEquals(PRODUCT_PRICE_1, response1.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_1, response1.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_1, response1.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_1, response1.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_1, response1.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_1.getCategoryId(), response1.getCategory().getId());

        ProductResponse response2 = productResponse.getData().get(1);
        Assertions.assertEquals(PRODUCT_ID_4, response2.getId());
        Assertions.assertEquals(PRODUCT_NAME_4, response2.getName());
        Assertions.assertEquals(PRODUCT_PRICE_4, response2.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_4, response2.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_4, response2.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_4, response2.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_4, response2.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_4.getCategoryId(), response2.getCategory().getId());
    }

    @Test
    public void getProducts_recommendedWithQuery_success_returnPaginatedResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .get("/api/product/products?sort=recommended&query=" + PRODUCT_QUERY)
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(PaginatedResponse.class, ProductResponse.class);
        PaginatedResponse<ProductResponse> productResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, productResponse.getCode());
        Assertions.assertEquals(1, productResponse.getData().size());

        ProductResponse response1 = productResponse.getData().get(0);
        Assertions.assertEquals(PRODUCT_ID_1, response1.getId());
        Assertions.assertEquals(PRODUCT_NAME_1, response1.getName());
        Assertions.assertEquals(PRODUCT_PRICE_1, response1.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_1, response1.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_1, response1.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_1, response1.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_1, response1.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_1.getCategoryId(), response1.getCategory().getId());
    }

    @Test
    public void getProducts_category_success_returnPaginatedResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .get("/api/product/products?sort=" + PRODUCT_CATEGORY_4.getCategoryId())
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(PaginatedResponse.class, ProductResponse.class);
        PaginatedResponse<ProductResponse> productResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, productResponse.getCode());
        Assertions.assertEquals(1, productResponse.getData().size());

        ProductResponse response1 = productResponse.getData().get(0);
        Assertions.assertEquals(PRODUCT_ID_4, response1.getId());
        Assertions.assertEquals(PRODUCT_NAME_4, response1.getName());
        Assertions.assertEquals(PRODUCT_PRICE_4, response1.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_4, response1.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_4, response1.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_4, response1.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_4, response1.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_4.getCategoryId(), response1.getCategory().getId());
    }

    @Test
    public void getProducts_categoryWithQuery_success_returnPaginatedResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .get("/api/product/products?sort=" + PRODUCT_CATEGORY_4.getCategoryId() + "&query=" + PRODUCT_QUERY)
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(PaginatedResponse.class, ProductResponse.class);
        PaginatedResponse<ProductResponse> productResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, productResponse.getCode());
        Assertions.assertEquals(0, productResponse.getData().size());
    }

    @Test
    public void getProducts_failed_sortInvalid_returnErrorResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .get("/api/product/products?sort=asddasda")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, errorResponse.getCode());

        boolean hasMalformedRequestParameterException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(MalformedRequestParameterException.class.getSimpleName()));
        Assertions.assertTrue(hasMalformedRequestParameterException);
    }

    @Test
    public void getProduct_success_returnBaseResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .pathParam("productId", PRODUCT_ID_1)
                .get("/api/product/{productId}")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, ProductResponse.class);
        BaseResponse<ProductResponse> productResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, productResponse.getCode());

        ProductResponse response1 = productResponse.getData();
        Assertions.assertEquals(PRODUCT_ID_1, response1.getId());
        Assertions.assertEquals(PRODUCT_NAME_1, response1.getName());
        Assertions.assertEquals(PRODUCT_PRICE_1, response1.getPrice());
        Assertions.assertEquals(PRODUCT_IMAGEURL_1, response1.getImageUrl());
        Assertions.assertEquals(PRODUCT_PURCHASECOUNT_1, response1.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS_1, response1.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED_1, response1.isRecommended());
        Assertions.assertEquals(PRODUCT_CATEGORY_1.getCategoryId(), response1.getCategory().getId());
    }

    @Test
    public void getProduct_failed_productNotFound_returnErrorResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .pathParam("productId", 123)
                .get("/api/product/{productId}")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, errorResponse.getCode());

        boolean hasObjectNotFoundException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(ObjectNotFoundException.class.getSimpleName()));
        Assertions.assertTrue(hasObjectNotFoundException);
    }
}
