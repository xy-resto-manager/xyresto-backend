package blibli.future.xyresto;

import blibli.future.xyresto.dto.request.LoginRequest;
import blibli.future.xyresto.dto.request.OrderRequest;
import blibli.future.xyresto.dto.request.RegisterRequest;
import blibli.future.xyresto.dto.response.BaseResponse;
import blibli.future.xyresto.dto.response.CartResponse;
import blibli.future.xyresto.dto.response.ErrorResponse;
import blibli.future.xyresto.exception.ObjectNotFoundException;
import blibli.future.xyresto.exception.UnauthorizedAccessException;
import blibli.future.xyresto.model.Cart;
import blibli.future.xyresto.model.Category;
import blibli.future.xyresto.model.Product;
import blibli.future.xyresto.model.User;
import blibli.future.xyresto.repository.CartRepository;
import blibli.future.xyresto.repository.CategoryRepository;
import blibli.future.xyresto.repository.ProductRepository;
import blibli.future.xyresto.repository.UserRepository;
import blibli.future.xyresto.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = XYRestoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CartControllerTests {

    public static final int CATEGORY_ID_1 = 1;
    public static final String CATEGORY_NAME_1 = "Seafood";

    public static final int PRODUCT_ID_1 = 1;
    public static final String PRODUCT_NAME_1 = "Tempe Penyet";
    public static final String PRODUCT_DESCRIPTION_1 = "Tempe dipenyet sama sambel bawang uenak pol";
    public static final int PRODUCT_PRICE_1 = 20000;
    public static final String PRODUCT_IMAGEURL_1 = "/product/1";
    public static final int PRODUCT_PURCHASECOUNT_1 = 32;
    public static final List<String> PRODUCT_TRAITS_1 = Arrays.asList("SPICY", "SALTY");
    public static final boolean PRODUCT_RECOMMENDED_1 = true;
    public static Category PRODUCT_CATEGORY_1;

    public static final int PRODUCT_ID_2 = 2;
    public static final String PRODUCT_NAME_2 = "Ayam Geprek";
    public static final String PRODUCT_DESCRIPTION_2 = "Ayam digeprek wkwk - tambah keyword tempe";
    public static final int PRODUCT_PRICE_2 = 35000;
    public static final String PRODUCT_IMAGEURL_2 = "/product/2";
    public static final int PRODUCT_PURCHASECOUNT_2 = 12;
    public static final List<String> PRODUCT_TRAITS_2 = Arrays.asList("SPICY");
    public static final boolean PRODUCT_RECOMMENDED_2 = false;
    public static Category PRODUCT_CATEGORY_2;

    public static final String USER_EMAIL = "user@gmail.com";
    public static final String USER_PASSWORD = "password";
    public static final String USER_FULL_NAME = "User";

    private static final int ORDER_QUANTITY = 3;

    private String userJwtToken;

    @Value("${local.server.port}")
    private int port;
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private UserService userService;

    private OrderRequest orderRequest;
    private OrderRequest orderRequestInvalidProduct;

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
        objectMapper = new ObjectMapper();

        userRepository.deleteAll();
        categoryRepository.deleteAll();
        productRepository.deleteAll();
        cartRepository.deleteAll();

        Category category1 = Category.builder()
                .categoryId(CATEGORY_ID_1)
                .name(CATEGORY_NAME_1)
                .build();

        categoryRepository.save(category1);

        PRODUCT_CATEGORY_1 = category1;
        PRODUCT_CATEGORY_2 = category1;

        Product product1 = Product.builder()
                .productId(PRODUCT_ID_1)
                .name(PRODUCT_NAME_1)
                .description(PRODUCT_DESCRIPTION_1)
                .price(PRODUCT_PRICE_1)
                .imageUrl(PRODUCT_IMAGEURL_1)
                .purchaseCount(PRODUCT_PURCHASECOUNT_1)
                .traits(PRODUCT_TRAITS_1)
                .recommended(PRODUCT_RECOMMENDED_1)
                .category(PRODUCT_CATEGORY_1)
                .build();
        Product product2 = Product.builder()
                .productId(PRODUCT_ID_2)
                .name(PRODUCT_NAME_2)
                .description(PRODUCT_DESCRIPTION_2)
                .price(PRODUCT_PRICE_2)
                .imageUrl(PRODUCT_IMAGEURL_2)
                .purchaseCount(PRODUCT_PURCHASECOUNT_2)
                .traits(PRODUCT_TRAITS_2)
                .recommended(PRODUCT_RECOMMENDED_2)
                .category(PRODUCT_CATEGORY_2)
                .build();

        productRepository.save(product1);
        productRepository.save(product2);

        RegisterRequest userRegisterRequest = RegisterRequest.builder()
                .email(USER_EMAIL)
                .password(USER_PASSWORD)
                .fullName(USER_FULL_NAME)
                .build();
        userService.register(userRegisterRequest);
        ValidatableResponse userResponse = RestAssured.given().contentType(ContentType.JSON)
                .body(LoginRequest.builder()
                        .email(USER_EMAIL)
                        .password(USER_PASSWORD)
                        .build())
                .post("/api/user/_login")
                .then();
        userJwtToken = userResponse.extract().cookie("jwtToken");

        orderRequest = OrderRequest.builder()
                .productId(PRODUCT_ID_1)
                .quantity(ORDER_QUANTITY)
                .build();

        orderRequestInvalidProduct = OrderRequest.builder()
                .productId(123123)
                .quantity(1)
                .build();
    }

    @Test
    public void getCurrentCart_success_returnBaseResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", userJwtToken)
                .get("/api/cart/current-cart")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, CartResponse.class);
        BaseResponse<CartResponse> cartResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, cartResponse.getCode());

        Assertions.assertEquals(USER_EMAIL, cartResponse.getData().getUser().getEmail());

        Assertions.assertNotNull(cartResponse.getData().getSummary());
        Assertions.assertEquals(0, cartResponse.getData().getSummary().getTotalPrice());
        Assertions.assertEquals(0, cartResponse.getData().getSummary().getQuantity());
        Assertions.assertEquals(0, cartResponse.getData().getProducts().size());
    }

    @Test
    public void getCurrentCart_failed_notLoggedIn_returnErrorResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .get("/api/cart/current-cart")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());

        boolean hasUnauthorizedAccessException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(UnauthorizedAccessException.class.getSimpleName()));
        Assertions.assertTrue(hasUnauthorizedAccessException);
    }

    @Test
    public void editCart_success_returnBaseResponse() throws JsonProcessingException {
        // Test add product to empty cart
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", userJwtToken)
                .body(orderRequest)
                .put("/api/cart/current-cart")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, CartResponse.class);
        BaseResponse<CartResponse> cartResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, cartResponse.getCode());

        Assertions.assertEquals(USER_EMAIL, cartResponse.getData().getUser().getEmail());

        Assertions.assertNotNull(cartResponse.getData().getSummary());
        Assertions.assertEquals(PRODUCT_PRICE_1 * ORDER_QUANTITY, cartResponse.getData().getSummary().getTotalPrice());
        Assertions.assertEquals(ORDER_QUANTITY, cartResponse.getData().getSummary().getQuantity());
        Assertions.assertEquals(1, cartResponse.getData().getProducts().size());
        Assertions.assertEquals(PRODUCT_ID_1, cartResponse.getData().getProducts().get(0).getProduct().getId());

        // Test update quantity of product in cart
        orderRequest.setQuantity(ORDER_QUANTITY + 1);
        response = RestAssured.given().contentType(ContentType.JSON)
            .cookie("jwtToken", userJwtToken)
            .body(orderRequest)
            .put("/api/cart/current-cart")
            .then();

        cartResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, cartResponse.getCode());

        Assertions.assertNotNull(cartResponse.getData().getSummary());
        Assertions.assertEquals(PRODUCT_PRICE_1 * (ORDER_QUANTITY + 1), cartResponse.getData().getSummary().getTotalPrice());
        Assertions.assertEquals(ORDER_QUANTITY + 1, cartResponse.getData().getSummary().getQuantity());
        Assertions.assertEquals(1, cartResponse.getData().getProducts().size());
        Assertions.assertEquals(PRODUCT_ID_1, cartResponse.getData().getProducts().get(0).getProduct().getId());

        // Test set product in cart to quantity 0
        orderRequest.setQuantity(0);
        response = RestAssured.given().contentType(ContentType.JSON)
            .cookie("jwtToken", userJwtToken)
            .body(orderRequest)
            .put("/api/cart/current-cart")
            .then();

        cartResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, cartResponse.getCode());

        Assertions.assertNotNull(cartResponse.getData().getSummary());
        Assertions.assertEquals(0, cartResponse.getData().getSummary().getTotalPrice());
        Assertions.assertEquals(0, cartResponse.getData().getSummary().getQuantity());
        Assertions.assertEquals(0, cartResponse.getData().getProducts().size());

        // Test set product not in cart to quantity 0
        orderRequest.setProductId(PRODUCT_ID_2);
        response = RestAssured.given().contentType(ContentType.JSON)
            .cookie("jwtToken", userJwtToken)
            .body(orderRequest)
            .put("/api/cart/current-cart")
            .then();

        cartResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, cartResponse.getCode());

        Assertions.assertNotNull(cartResponse.getData().getSummary());
        Assertions.assertEquals(0, cartResponse.getData().getSummary().getTotalPrice());
        Assertions.assertEquals(0, cartResponse.getData().getSummary().getQuantity());
        Assertions.assertEquals(0, cartResponse.getData().getProducts().size());
    }

    @Test
    public void editCart_failed_notLoggedIn_returnBaseResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .body(orderRequest)
                .put("/api/cart/current-cart")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());

        boolean hasUnauthorizedAccessException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(UnauthorizedAccessException.class.getSimpleName()));
        Assertions.assertTrue(hasUnauthorizedAccessException);
    }

    @Test
    public void editCart_failed_invalidProduct_returnBaseResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", userJwtToken)
                .body(orderRequestInvalidProduct)
                .put("/api/cart/current-cart")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, errorResponse.getCode());

        boolean hasObjectNotFoundException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(ObjectNotFoundException.class.getSimpleName()));
        Assertions.assertTrue(hasObjectNotFoundException);
    }
}
