package blibli.future.xyresto;

import blibli.future.xyresto.dto.TableDTO;
import blibli.future.xyresto.dto.request.ChangeRoleRequest;
import blibli.future.xyresto.dto.request.LoginRequest;
import blibli.future.xyresto.dto.request.RegisterRequest;
import blibli.future.xyresto.dto.request.ReservationRequest;
import blibli.future.xyresto.dto.response.BaseArrayResponse;
import blibli.future.xyresto.dto.response.UserResponse;
import blibli.future.xyresto.dto.response.TableResponse;
import blibli.future.xyresto.model.Reservation;
import blibli.future.xyresto.repository.ReservationRepository;
import blibli.future.xyresto.repository.TableRepository;
import blibli.future.xyresto.repository.UserRepository;
import blibli.future.xyresto.service.AdminService;
import blibli.future.xyresto.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = XYRestoApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LayoutControllerTests {

    //User
    public static final String ADMIN_EMAIL = "admin@gmail.com";
    public static final String ADMIN_PASSWORD = "password";
    public static final String ADMIN_FULL_NAME = "Administrator";
    public static final String USER_EMAIL = "user@gmail.com";
    public static final String USER_PASSWORD = "password";
    public static final String USER_FULL_NAME = "User";

    // Table
    public static final int TABLE_NO_1 = 3;
    public static final int X_1 = 4;
    public static final int Y_1 = 4;

    public static final int TABLE_NO_2 = 4;
    public static final int X_2 = 2;
    public static final int Y_2 = 2;

    //Reservation
    public ReservationRequest reservationRequest;
    public Instant TIMESTAMP = Instant.now().plus(1, ChronoUnit.DAYS);


    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    TableRepository tableRepository;

    @Autowired
    UserService userService;

    @Autowired
    AdminService adminService;

    @Autowired
    UserRepository userRepository;

    private String adminJwtToken;
    private String userJwtToken;

    @Value("${local.server.port}")
    private int port;
    private ObjectMapper objectMapper = new ObjectMapper();


    @BeforeEach
    public void setup(){
        RestAssured.port = port;

        userRepository.deleteAll();
        tableRepository.deleteAll();
        reservationRepository.deleteAll();


        RegisterRequest adminRegisterRequest = RegisterRequest.builder()
                .email(ADMIN_EMAIL)
                .password(ADMIN_PASSWORD)
                .fullName(ADMIN_FULL_NAME)
                .build();
        UserResponse adminUserResponse = userService.register(adminRegisterRequest);
        adminService.editRoles(
                adminUserResponse.getId(),
                ChangeRoleRequest.builder()
                        .role("USER")
                        .role("ADMIN")
                        .build()
        );
        ValidatableResponse adminResponse = RestAssured.given().contentType(ContentType.JSON)
                .body(LoginRequest.builder()
                        .email(ADMIN_EMAIL)
                        .password(ADMIN_PASSWORD)
                        .build())
                .post("/api/user/_login")
                .then();
        adminJwtToken = adminResponse.extract().cookie("jwtToken");

        RegisterRequest userRegisterRequest = RegisterRequest.builder()
                .email(USER_EMAIL)
                .password(USER_PASSWORD)
                .fullName(USER_FULL_NAME)
                .build();
        userService.register(userRegisterRequest);
        ValidatableResponse userResponse = RestAssured.given().contentType(ContentType.JSON)
                .body(LoginRequest.builder()
                        .email(USER_EMAIL)
                        .password(USER_PASSWORD)
                        .build())
                .post("/api/user/_login")
                .then();
        userJwtToken = userResponse.extract().cookie("jwtToken");

        reservationRequest = ReservationRequest.builder().timestamp(TIMESTAMP.toEpochMilli()).tableId(TABLE_NO_1).build();

    }

    @Test
    public void getLayoutForUser_success_returnBaseArrayResponse() throws JsonProcessingException {

        List<TableDTO> request  = new ArrayList<>();
        request.add(TableDTO.builder().x(X_1).y(Y_1).tableId(TABLE_NO_1).build());
        request.add(TableDTO.builder().x(X_2).y(Y_2).tableId(TABLE_NO_2).build());

        ValidatableResponse createTable = RestAssured.given().contentType(ContentType.JSON).header(
                "Authorization",
                "Bearer " + adminJwtToken).body(request).put("/api/admin/layout/tables").then();

        ValidatableResponse createReservation = RestAssured.given().contentType(ContentType.JSON).header(
                "Authorization",
                "Bearer " + userJwtToken).body(reservationRequest).post("/api/reservation/reservations").then();

        Reservation reservation = reservationRepository.findByReservationId(1).orElse(null);
        if(reservation != null){
            reservation.setStatus(Reservation.Status.APPROVED);
            reservationRepository.save(reservation);
        }

        ValidatableResponse getLayout = RestAssured.given().content(ContentType.JSON).header("Authorization",
                "Bearer " + userJwtToken).queryParam("timestamp",TIMESTAMP.toEpochMilli()).get("/api/layout/tables").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseArrayResponse.class,
                TableResponse.class);

        BaseArrayResponse<TableResponse> response = objectMapper.readValue(getLayout.extract().asString(),type);


        Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());
        Assertions.assertTrue(response.getData().get(0).isBooked());
        Assertions.assertFalse(response.getData().get(1).isBooked());
    }

}