package blibli.future.xyresto;

import blibli.future.xyresto.dto.EditOrderStatusRequest;
import blibli.future.xyresto.dto.OrderedProduct;
import blibli.future.xyresto.dto.TableDTO;
import blibli.future.xyresto.dto.request.*;
import blibli.future.xyresto.dto.response.*;
import blibli.future.xyresto.exception.NotSupportedMIMETypeException;
import blibli.future.xyresto.exception.ObjectNotFoundException;
import blibli.future.xyresto.exception.UnauthorizedAccessException;
import blibli.future.xyresto.model.*;
import blibli.future.xyresto.repository.*;
import blibli.future.xyresto.service.AdminService;
import blibli.future.xyresto.service.BannerService;
import blibli.future.xyresto.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.MultiPartSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.context.WebApplicationContext;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = XYRestoApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AdminControllerTests {

    public static final String CATEGORY_NAME = "Sushi";
    public static final String CATEGORY_NAME_EDIT = "SUSHIIIII";

    public static final String PRODUCT_NAME = "Tempe Penyet";
    public static final String PRODUCT_DESCRIPTION = "Tempe dipenyet sama sambel bawang uenak pol";
    public static final int PRODUCT_PRICE = 20000;
    public static final List<String> PRODUCT_TRAITS = Arrays.asList("SPICY", "SALTY");
    public static final boolean PRODUCT_RECOMMENDED = true;

    public static final String ADMIN_EMAIL = "admin@gmail.com";
    public static final String ADMIN_PASSWORD = "password";
    public static final String ADMIN_FULL_NAME = "Administrator";
    public static final String USER_EMAIL = "user@gmail.com";
    public static final String USER_PASSWORD = "password";
    public static final String USER_FULL_NAME = "User";

    public Instant TIMESTAMP = Instant.now().plus(1, ChronoUnit.DAYS);

    public static final int TABLE_NO = 2;
    public static final int  X = 4;
    public static final int Y = 3;
    public static final int TABLE_NO_2 = 3;
    public static final int  X_2 = 4;
    public static final int Y_2 = 4;

    private String adminJwtToken;
    private String userJwtToken;

    @Value("${timeZone:GMT+7}")
    private String timezone;


    @Value("${local.server.port}")
    private int port;
    private ObjectMapper objectMapper;
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private CounterRepository counterRepository;
    @Autowired
    private BannerRepository bannerRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private TableRepository tableRepository;
    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private UserService userService;
    @Autowired
    private AdminService adminService;
    @Autowired
    private BannerService bannerService;
    private CategoryRequest categoryRequest;
    private CategoryRequest emptyCategoryRequest;
    private CategoryRequest editCategoryRequest;
    private ChangeRoleRequest changeRoleRequest;
    private ReservationRequest reservationRequest;
    private List<TableDTO> tableRequest;

    @BeforeEach
    public void setUp() {

        RestAssured.port = port;

        objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());

        bannerRepository.deleteAll();
        counterRepository.deleteAll();
        userRepository.deleteAll();
        categoryRepository.deleteAll();
        productRepository.deleteAll();
        tableRepository.deleteAll();
        reservationRepository.deleteAll();


        RegisterRequest adminRegisterRequest = RegisterRequest.builder()
                .email(ADMIN_EMAIL)
                .password(ADMIN_PASSWORD)
                .fullName(ADMIN_FULL_NAME)
                .build();
        UserResponse adminUserResponse = userService.register(adminRegisterRequest);
        adminService.editRoles(
                adminUserResponse.getId(),
                ChangeRoleRequest.builder()
                        .role("USER")
                        .role("ADMIN")
                        .build()
        );
        ValidatableResponse adminResponse = RestAssured.given().contentType(ContentType.JSON)
                .body(LoginRequest.builder()
                        .email(ADMIN_EMAIL)
                        .password(ADMIN_PASSWORD)
                        .build())
                .post("/api/user/_login")
                .then();
        adminJwtToken = adminResponse.extract().cookie("jwtToken");

        RegisterRequest userRegisterRequest = RegisterRequest.builder()
                .email(USER_EMAIL)
                .password(USER_PASSWORD)
                .fullName(USER_FULL_NAME)
                .build();
        userService.register(userRegisterRequest);
        ValidatableResponse userResponse = RestAssured.given().contentType(ContentType.JSON)
                .body(LoginRequest.builder()
                        .email(USER_EMAIL)
                        .password(USER_PASSWORD)
                        .build())
                .post("/api/user/_login")
                .then();
        userJwtToken = userResponse.extract().cookie("jwtToken");

        categoryRequest = CategoryRequest.builder()
                .name(CATEGORY_NAME)
                .build();

        emptyCategoryRequest = CategoryRequest.builder()
                .name("")
                .build();

        editCategoryRequest = CategoryRequest.builder()
                .name(CATEGORY_NAME_EDIT)
                .build();

        changeRoleRequest = ChangeRoleRequest.builder()
                .roles(Arrays.asList("USER"))
                .build();

        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity()).build();

        reservationRequest = ReservationRequest.builder()
                .timestamp(TIMESTAMP.toEpochMilli())
                .tableId(TABLE_NO)
                .build();

        tableRequest = new ArrayList<>();
        tableRequest.add(TableDTO.builder()
                .tableId(TABLE_NO)
                .x(X)
                .y(Y)
                .build());
    }

    @Test
    public void getAllUser_success_returnBaseResponse() throws JsonProcessingException {

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON)
                        .cookie("jwtToken",adminJwtToken)
                        .get("/api/admin/user/users")
                        .then();

        JavaType type = objectMapper.getTypeFactory()
                .constructParametricType(BaseResponse.class,
                objectMapper.getTypeFactory().constructCollectionType(List.class,UserResponse.class));

        BaseResponse<List<UserResponse>> res = objectMapper.readValue(validatableResponse.extract().asString(),type);
        Assertions.assertTrue(res.getData().stream().findFirst().isPresent());
        Assertions.assertEquals(ADMIN_EMAIL, res.getData().stream().findFirst().get().getEmail());

        validatableResponse =
            RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken",adminJwtToken)
                .get("/api/admin/user/users?role=admin")
                .then();

        res = objectMapper.readValue(validatableResponse.extract().asString(),type);
        Assertions.assertTrue(res.getData().stream().findFirst().isPresent());
        Assertions.assertEquals(ADMIN_EMAIL, res.getData().stream().findFirst().get().getEmail());
    }

    @Test
    public void getAllUser_failed_userIsNotAdmin_returnErrorResponse() throws JsonProcessingException {

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType("application/json")
                        .cookie("jwtToken",userJwtToken)
                        .get("/api/admin/user/users").then();

        ErrorResponse errorResponse = objectMapper.readValue(validatableResponse.extract().asString(),
                ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());
    }

    @Test
    public void editRoles_success_returnBaseResponse() throws JsonProcessingException {

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON)
                        .pathParam("userId",2)
                        .cookie("jwtToken",adminJwtToken)
                        .body(changeRoleRequest)
                        .post("/api/admin/user/users/{userId}/roles")
                        .then();
        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class,UserResponse.class);
        BaseResponse<UserResponse> response = objectMapper.readValue(validatableResponse.extract().asString(),type);

        Assertions.assertEquals(Arrays.asList("USER"), response.getData().getRoles());
    }

    @Test
    public void editRoles_failed_userIsNotAdmin_returnErrorResponse() throws JsonProcessingException {

        changeRoleRequest = ChangeRoleRequest.builder().roles(Arrays.asList("USER")).build();

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON)
                        .cookie("jwtToken",userJwtToken)
                        .pathParam("userId",2)
                        .body(changeRoleRequest)
                        .post("/api/admin/user/users/{userId}/roles")
                        .then();

        ErrorResponse errorResponse = objectMapper.readValue(validatableResponse.extract().asString(),
                ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());

    }

    @Test
    public void editRoles_failed_userIdNotFound_returnErrorResponse() throws JsonProcessingException {

        int wrongUserId = 3;

        changeRoleRequest = ChangeRoleRequest.builder().roles(Arrays.asList("USER")).build();

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON)
                        .cookie("jwtToken",adminJwtToken)
                        .pathParam("userId",wrongUserId)
                        .body(changeRoleRequest)
                        .post("/api/admin/user/users/{userId}/roles")
                        .then();

        ErrorResponse errorResponse = objectMapper.readValue(validatableResponse.extract().asString(),
                ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST,errorResponse.getCode());
    }

    @Test
    public void createBanner_success_returnBaseResponse() throws Exception {

        File fileLoc = new File(getClass().getClassLoader().getResource("uploadtest.jpeg").getFile());
        BufferedImage file = ImageIO.read(fileLoc);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(file,"png",bos);
        MockMultipartFile image = new MockMultipartFile("file","image.jpg", MediaType.IMAGE_PNG_VALUE,
                bos.toByteArray());

        mockMvc.perform(multipart("/api/admin/banner/banners").file(image).header("Authorization","Bearer " + adminJwtToken))
                .andExpect(status().isCreated());

    }

    @Test
    public void createBanner_failed_userIsNotAdmin_returnErrorResponse() throws Exception{

        File fileLoc = new File(getClass().getClassLoader().getResource("uploadtest.jpeg").getFile());
        BufferedImage file = ImageIO.read(fileLoc);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(file,"png",bos);
        MockMultipartFile image = new MockMultipartFile("file","image", MediaType.IMAGE_PNG_VALUE,
                bos.toByteArray());

        mockMvc.perform(multipart("/api/admin/banner/banners").file(image))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void createBanner_failed_extensionNotSupported_returnErrorResponse() throws Exception {

        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );
        mockMvc
                = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(SecurityMockMvcConfigurers.springSecurity()).build();
        mockMvc.perform(multipart("/api/admin/banner/banners").file(file).header("Authorization","Bearer "+ adminJwtToken))
                .andExpect(status().is4xxClientError());

        file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                null,
                "Hello, World!".getBytes()
        );
        mockMvc
                = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(SecurityMockMvcConfigurers.springSecurity()).build();
        mockMvc.perform(multipart("/api/admin/banner/banners").file(file).header("Authorization","Bearer "+ adminJwtToken))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void deleteBanner_success_returnBaseResponse() throws Exception {

        File fileLoc = new File(getClass().getClassLoader().getResource("uploadtest.jpeg").getFile());
        BufferedImage file = ImageIO.read(fileLoc);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(file,"png",bos);
        MockMultipartFile image = new MockMultipartFile("file","image.jpg", MediaType.IMAGE_PNG_VALUE,
                bos.toByteArray());
        bannerService.createBanner(image);

        ValidatableResponse validatableResponse =
                RestAssured.given()
                        .cookie("jwtToken",adminJwtToken)
                        .pathParam("bannerId",1)
                        .delete("/api/admin/banner/{bannerId}").then();

        BaseResponse<Object> response = objectMapper.readValue(validatableResponse.extract().asString(),
                BaseResponse.class);

        Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());
    }



    @Test
    public void deleteBanner_failed_bannerNotFound_returnErrorResponse() throws Exception {

        int wrongBannerId = 2;

        ValidatableResponse validatableResponse =
                RestAssured.given()
                        .cookie("jwtToken",adminJwtToken)
                        .pathParam("bannerId",wrongBannerId)
                        .delete("/api/admin/banner/{bannerId}").then();

        ErrorResponse response = objectMapper.readValue(validatableResponse.extract().asString(),
                ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, response.getCode());
    }

    @Test
    public void deleteBanner_failed_userIsNotAdmin_returnErrorResponse() throws Exception {

        File fileLoc = new File(getClass().getClassLoader().getResource("uploadtest.jpeg").getFile());
        BufferedImage file = ImageIO.read(fileLoc);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(file,"png",bos);
        MockMultipartFile image = new MockMultipartFile("file","image.jpg", MediaType.IMAGE_PNG_VALUE,
                bos.toByteArray());
        bannerService.createBanner(image);

        ValidatableResponse validatableResponse =
                RestAssured.given()
                        .cookie("jwtToken",userJwtToken)
                        .pathParam("bannerId",1)
                        .delete("/api/admin/banner/{bannerId}").then();

        ErrorResponse response = objectMapper.readValue(validatableResponse.extract().asString(),
                ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, response.getCode());
    }

    @Test
    public void createCategory_success_returnBaseResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .body(categoryRequest)
                .cookie("jwtToken", adminJwtToken)
                .post("/api/admin/product/categories")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, CategoryResponse.class);
        BaseResponse<CategoryResponse> categoryResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_CREATED, categoryResponse.getCode());

        Category category = categoryRepository.findFirstByCategoryId(1).orElse(null);
        Assertions.assertNotNull(category);
        Assertions.assertEquals(CATEGORY_NAME, category.getName());
    }

    @Test
    public void createCategory_failed_userIsNotAdmin_returnErrorResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .body(categoryRequest)
                .cookie("jwtToken", userJwtToken)
                .post("/api/admin/product/categories")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());

        boolean hasUnauthorizedAccessException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(UnauthorizedAccessException.class.getSimpleName()));
        Assertions.assertTrue(hasUnauthorizedAccessException);

        Category category = categoryRepository.findFirstByCategoryId(1).orElse(null);
        Assertions.assertNull(category);
    }

    @Test
    public void createCategory_failed_emptyName_returnErrorResponse() throws JsonProcessingException {
        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .body(emptyCategoryRequest)
                .cookie("jwtToken", adminJwtToken)
                .post("/api/admin/product/categories")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, errorResponse.getCode());

        boolean hasInvalidRequestException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals("InvalidRequestException"));
        Assertions.assertTrue(hasInvalidRequestException);

        Category category = categoryRepository.findFirstByCategoryId(1).orElse(null);
        Assertions.assertNull(category);
    }

    @Test
    public void editCategory_success_returnBaseResponse() throws JsonProcessingException {
        Category newCategory = Category.builder()
                .categoryId(1)
                .name(CATEGORY_NAME)
                .build();
        categoryRepository.save(newCategory);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .body(editCategoryRequest)
                .pathParam("categoryId", 1)
                .cookie("jwtToken", adminJwtToken)
                .put("/api/admin/product/categories/{categoryId}")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, CategoryResponse.class);
        BaseResponse<CategoryResponse> categoryResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, categoryResponse.getCode());

        Category category = categoryRepository.findFirstByCategoryId(1).orElse(null);
        Assertions.assertNotNull(category);
        Assertions.assertEquals(CATEGORY_NAME_EDIT, category.getName());
    }

    @Test
    public void editCategory_failed_userIsNotAdmin_returnErrorResponse() throws JsonProcessingException {
        Category newCategory = Category.builder()
                .categoryId(1)
                .name(CATEGORY_NAME)
                .build();
        categoryRepository.save(newCategory);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .body(editCategoryRequest)
                .pathParam("categoryId", 1)
                .cookie("jwtToken", userJwtToken)
                .put("/api/admin/product/categories/{categoryId}")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());

        boolean hasUnauthorizedAccessException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(UnauthorizedAccessException.class.getSimpleName()));
        Assertions.assertTrue(hasUnauthorizedAccessException);

        Category category = categoryRepository.findFirstByCategoryId(1).orElse(null);
        Assertions.assertNotNull(category);
        Assertions.assertEquals(CATEGORY_NAME, category.getName());
    }

    @Test
    public void editCategory_failed_categoryNotFound_returnErrorResponse() throws JsonProcessingException {
        Category newCategory = Category.builder()
                .categoryId(1)
                .name(CATEGORY_NAME)
                .build();
        categoryRepository.save(newCategory);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .body(editCategoryRequest)
                .pathParam("categoryId", 2)
                .cookie("jwtToken", adminJwtToken)
                .put("/api/admin/product/categories/{categoryId}")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, errorResponse.getCode());

        boolean hasObjectNotFoundException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(ObjectNotFoundException.class.getSimpleName()));
        Assertions.assertTrue(hasObjectNotFoundException);

        Category category = categoryRepository.findFirstByCategoryId(1).orElse(null);
        Assertions.assertNotNull(category);
        Assertions.assertEquals(CATEGORY_NAME, category.getName());

        category = categoryRepository.findFirstByCategoryId(2).orElse(null);
        Assertions.assertNull(category);
    }

    @Test
    public void editCategory_failed_emptyName_returnErrorResponse() throws JsonProcessingException {
        Category newCategory = Category.builder()
                .categoryId(1)
                .name(CATEGORY_NAME)
                .build();
        categoryRepository.save(newCategory);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .body(emptyCategoryRequest)
                .pathParam("categoryId", 1)
                .cookie("jwtToken", adminJwtToken)
                .put("/api/admin/product/categories/{categoryId}")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, errorResponse.getCode());

        boolean hasInvalidRequestException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals("InvalidRequestException"));
        Assertions.assertTrue(hasInvalidRequestException);

        Category category = categoryRepository.findFirstByCategoryId(1).orElse(null);
        Assertions.assertNotNull(category);
        Assertions.assertEquals(CATEGORY_NAME, category.getName());
    }

    @Test
    public void deleteCategory_success_returnBaseResponse() throws JsonProcessingException {
        Category newCategory = Category.builder()
                .categoryId(1)
                .name(CATEGORY_NAME)
                .build();
        categoryRepository.save(newCategory);

        Product product = Product.builder()
            .productId(1)
            .name(PRODUCT_NAME)
            .description(PRODUCT_DESCRIPTION)
            .price(PRODUCT_PRICE)
            .recommended(PRODUCT_RECOMMENDED)
            .category(newCategory)
            .build();
        productRepository.save(product);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .pathParam("categoryId", 1)
                .cookie("jwtToken", adminJwtToken)
                .delete("/api/admin/product/categories/{categoryId}")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, Object.class);
        BaseResponse<CategoryResponse> categoryResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, categoryResponse.getCode());

        Category category = categoryRepository.findFirstByCategoryId(1).orElse(null);
        Assertions.assertNull(category);

        Product productCheck = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNotNull(productCheck);
        Assertions.assertNull(productCheck.getCategory());
    }

    @Test
    public void deleteCategory_failed_userIsNotAdmin_returnBaseResponse() throws JsonProcessingException {
        Category newCategory = Category.builder()
                .categoryId(1)
                .name(CATEGORY_NAME)
                .build();
        categoryRepository.save(newCategory);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .pathParam("categoryId", 1)
                .cookie("jwtToken", userJwtToken)
                .delete("/api/admin/product/categories/{categoryId}")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());

        boolean hasUnauthorizedAccessException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(UnauthorizedAccessException.class.getSimpleName()));
        Assertions.assertTrue(hasUnauthorizedAccessException);

        Category category = categoryRepository.findFirstByCategoryId(1).orElse(null);
        Assertions.assertNotNull(category);
        Assertions.assertEquals(CATEGORY_NAME, category.getName());
    }

    @Test
    public void deleteCategory_failed_categoryNotFound_returnErrorResponse() throws JsonProcessingException {
        Category newCategory = Category.builder()
                .categoryId(1)
                .name(CATEGORY_NAME)
                .build();
        categoryRepository.save(newCategory);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .pathParam("categoryId", 2)
                .cookie("jwtToken", adminJwtToken)
                .delete("/api/admin/product/categories/{categoryId}")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, errorResponse.getCode());

        boolean hasObjectNotFoundException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(ObjectNotFoundException.class.getSimpleName()));
        Assertions.assertTrue(hasObjectNotFoundException);

        Category category = categoryRepository.findFirstByCategoryId(1).orElse(null);
        Assertions.assertNotNull(category);
        Assertions.assertEquals(CATEGORY_NAME, category.getName());

        category = categoryRepository.findFirstByCategoryId(2).orElse(null);
        Assertions.assertNull(category);
    }

    @Test
    public void createProduct_success_returnBaseResponse() throws JsonProcessingException {
        Category category = Category.builder()
                .categoryId(1)
                .name("Test")
                .build();
        categoryRepository.save(category);
        ProductRequest productRequest = ProductRequest.builder()
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .traits(PRODUCT_TRAITS)
                .category(category.getCategoryId())
                .recommended(PRODUCT_RECOMMENDED)
                .build();

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", adminJwtToken)
                .body(productRequest)
                .post("/api/admin/product/products")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, ProductResponse.class);
        BaseResponse<ProductResponse> productResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_CREATED, productResponse.getCode());

        Product product = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNotNull(product);
        Assertions.assertEquals(PRODUCT_NAME, product.getName());
        Assertions.assertEquals(PRODUCT_PRICE, product.getPrice());
        Assertions.assertNull(product.getImageUrl());
        Assertions.assertEquals(0, product.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS, product.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED, product.isRecommended());
        Assertions.assertEquals(category, product.getCategory());
    }

    @Test
    public void createProduct_failed_categoryNotFound_returnErrorResponse() throws JsonProcessingException {
        ProductRequest productRequest = ProductRequest.builder()
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .traits(PRODUCT_TRAITS)
                .category(123L)
                .recommended(PRODUCT_RECOMMENDED)
                .build();

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", adminJwtToken)
                .body(productRequest)
                .post("/api/admin/product/products")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, errorResponse.getCode());

        boolean hasObjectNotFoundException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(ObjectNotFoundException.class.getSimpleName()));
        Assertions.assertTrue(hasObjectNotFoundException);

        Product createdProduct = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNull(createdProduct);
    }

    @Test
    public void createProduct_failed_userIsNotAdmin_returnErrorResponse() throws JsonProcessingException {
        ProductRequest productRequest = ProductRequest.builder()
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .traits(PRODUCT_TRAITS)
                .category(123L)
                .recommended(PRODUCT_RECOMMENDED)
                .build();

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", userJwtToken)
                .body(productRequest)
                .post("/api/admin/product/products")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());

        boolean hasObjectNotFoundException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(UnauthorizedAccessException.class.getSimpleName()));
        Assertions.assertTrue(hasObjectNotFoundException);

        Product createdProduct = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNull(createdProduct);
    }

    @Test
    public void editProduct_success_returnBaseResponse() throws JsonProcessingException {
        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(true)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .build();
        productRepository.save(product);

        Category category = Category.builder()
                .categoryId(1)
                .name("Test")
                .build();
        categoryRepository.save(category);

        ProductRequest productRequest = ProductRequest.builder()
                .name(PRODUCT_NAME + "edit")
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(false)
                .traits(PRODUCT_TRAITS)
                .category(1L)
                .build();

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", adminJwtToken)
                .body(productRequest)
                .pathParam("productId", 1)
                .put("/api/admin/product/{productId}")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, ProductResponse.class);
        BaseResponse<ProductResponse> productResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, productResponse.getCode());

        Product editedProduct = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNotNull(editedProduct);
        Assertions.assertEquals(PRODUCT_NAME + "edit", editedProduct.getName());
        Assertions.assertEquals(PRODUCT_PRICE, editedProduct.getPrice());
        Assertions.assertNull(editedProduct.getImageUrl());
        Assertions.assertEquals(0, editedProduct.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS, editedProduct.getTraits());
        Assertions.assertFalse(editedProduct.isRecommended());
        Assertions.assertEquals(category, editedProduct.getCategory());
    }

    @Test
    public void editProduct_failed_productNotFound_returnErrorResponse() throws JsonProcessingException {
        Category category = Category.builder()
                .categoryId(1)
                .name("Test")
                .build();
        categoryRepository.save(category);

        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .category(category)
                .build();
        productRepository.save(product);

        ProductRequest productRequest = ProductRequest.builder()
                .name(PRODUCT_NAME + "edit")
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .traits(PRODUCT_TRAITS)
                .category(1L)
                .build();

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", adminJwtToken)
                .body(productRequest)
                .pathParam("productId", 5)
                .put("/api/admin/product/{productId}")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, errorResponse.getCode());

        boolean hasObjectNotFoundException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(ObjectNotFoundException.class.getSimpleName()));
        Assertions.assertTrue(hasObjectNotFoundException);

        Product editedProduct = productRepository.findFirstByProductId(5).orElse(null);
        Assertions.assertNull(editedProduct);
    }

    @Test
    public void editProduct_failed_categoryNotFound_returnErrorResponse() throws JsonProcessingException {
        Category category = Category.builder()
                .categoryId(1)
                .name("Test")
                .build();
        categoryRepository.save(category);

        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .category(category)
                .build();
        productRepository.save(product);

        ProductRequest productRequest = ProductRequest.builder()
                .name(PRODUCT_NAME + "edit")
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .traits(PRODUCT_TRAITS)
                .category(123L)
                .build();

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", adminJwtToken)
                .body(productRequest)
                .pathParam("productId", 1)
                .put("/api/admin/product/{productId}")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, errorResponse.getCode());

        boolean hasObjectNotFoundException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(ObjectNotFoundException.class.getSimpleName()));
        Assertions.assertTrue(hasObjectNotFoundException);


        Product editedProduct = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNotNull(editedProduct);
        Assertions.assertEquals(PRODUCT_NAME, editedProduct.getName());
        Assertions.assertEquals(PRODUCT_PRICE, editedProduct.getPrice());
        Assertions.assertNull(editedProduct.getImageUrl());
        Assertions.assertEquals(0, editedProduct.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS, editedProduct.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED, editedProduct.isRecommended());
        Assertions.assertEquals(category, editedProduct.getCategory());
    }

    @Test
    public void editProduct_failed_userIsNotAdmin_returnErrorResponse() throws JsonProcessingException {
        Category category = Category.builder()
                .categoryId(1)
                .name("Test")
                .build();
        categoryRepository.save(category);

        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .category(category)
                .build();
        productRepository.save(product);

        ProductRequest productRequest = ProductRequest.builder()
                .name(PRODUCT_NAME + "edit")
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .traits(PRODUCT_TRAITS)
                .category(123L)
                .build();

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", userJwtToken)
                .body(productRequest)
                .pathParam("productId", 1)
                .put("/api/admin/product/{productId}")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());

        boolean hasObjectNotFoundException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(UnauthorizedAccessException.class.getSimpleName()));
        Assertions.assertTrue(hasObjectNotFoundException);

        Product editedProduct = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNotNull(editedProduct);
        Assertions.assertEquals(PRODUCT_NAME, editedProduct.getName());
        Assertions.assertEquals(PRODUCT_PRICE, editedProduct.getPrice());
        Assertions.assertNull(editedProduct.getImageUrl());
        Assertions.assertEquals(0, editedProduct.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS, editedProduct.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED, editedProduct.isRecommended());
        Assertions.assertEquals(category, editedProduct.getCategory());
    }

    @Test
    public void deleteProduct_success_returnBaseResponse() throws JsonProcessingException {
        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .build();
        productRepository.save(product);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", adminJwtToken)
                .pathParam("productId", 1)
                .delete("/api/admin/product/{productId}")
                .then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, Object.class);
        BaseResponse<Object> productResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, productResponse.getCode());

        Product deletedProduct = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNull(deletedProduct);
    }

    @Test
    public void deleteProduct_failed_returnErrorResponse() throws JsonProcessingException {
        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .build();
        productRepository.save(product);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", adminJwtToken)
                .pathParam("productId", 2)
                .delete("/api/admin/product/{productId}")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, errorResponse.getCode());

        boolean hasObjectNotFoundException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(ObjectNotFoundException.class.getSimpleName()));
        Assertions.assertTrue(hasObjectNotFoundException);

        Product deletedProduct = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNotNull(deletedProduct);
    }

    @Test
    public void deleteProduct_failed_userIsNotAdmin_returnErrorResponse() throws JsonProcessingException {
        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .build();
        productRepository.save(product);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", userJwtToken)
                .pathParam("productId", 1)
                .delete("/api/admin/product/{productId}")
                .then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());

        boolean hasObjectNotFoundException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(UnauthorizedAccessException.class.getSimpleName()));
        Assertions.assertTrue(hasObjectNotFoundException);

        Product deletedProduct = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNotNull(deletedProduct);
    }

    @Test
    public void uploadProductImage_success_returnBaseResponse() throws IOException {
        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .build();
        productRepository.save(product);

        File file = new File(getClass().getClassLoader().getResource("uploadtest.jpeg").getFile());

        Assertions.assertTrue(file.exists());

        ValidatableResponse response = RestAssured.given()
                .contentType("multipart/form-data")
                .multiPart(new MultiPartSpecBuilder(file)
                        .controlName("file")
                        .fileName("uploadtest.jpeg")
                        .mimeType(MimeTypeUtils.IMAGE_JPEG_VALUE)
                        .build())
                .cookie("jwtToken", adminJwtToken)
                .pathParam("productId", 1)
                .post("/api/admin/product/{productId}/image")
                .then();


        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, ProductResponse.class);
        BaseResponse<ProductResponse> productResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, productResponse.getCode());

        Product editedProduct = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNotNull(editedProduct);
        Assertions.assertEquals(PRODUCT_NAME, editedProduct.getName());
        Assertions.assertEquals(PRODUCT_PRICE, editedProduct.getPrice());
        Assertions.assertEquals(0, editedProduct.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS, editedProduct.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED, editedProduct.isRecommended());

        Assertions.assertNotNull(editedProduct.getImageUrl());

        byte[] image = RestAssured.given()
                .get(editedProduct.getImageUrl())
                .asByteArray();

        Assertions.assertArrayEquals(Files.readAllBytes(file.toPath()), image);
    }

    @Test
    public void uploadProductImage_failed_notAnImage_returnErrorResponse() throws IOException {
        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .build();
        productRepository.save(product);

        File file = new File(getClass().getClassLoader().getResource("notanimage.txt").getFile());

        Assertions.assertTrue(file.exists());

        ValidatableResponse response = RestAssured.given()
                .contentType("multipart/form-data")
                .multiPart(new MultiPartSpecBuilder(file)
                        .controlName("file")
                        .fileName("notanimage.txt")
                        .mimeType(MimeTypeUtils.TEXT_PLAIN_VALUE)
                        .build())
                .cookie("jwtToken", adminJwtToken)
                .pathParam("productId", 1)
                .post("/api/admin/product/{productId}/image")
                .then();


        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, errorResponse.getCode());

        boolean hasNotSupportedMIMETypeException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(NotSupportedMIMETypeException.class.getSimpleName()));
        Assertions.assertTrue(hasNotSupportedMIMETypeException);

        Product editedProduct = productRepository.findFirstByProductId(1).orElse(null);
        Assertions.assertNotNull(editedProduct);
        Assertions.assertEquals(PRODUCT_NAME, editedProduct.getName());
        Assertions.assertEquals(PRODUCT_PRICE, editedProduct.getPrice());
        Assertions.assertEquals(0, editedProduct.getPurchaseCount());
        Assertions.assertEquals(PRODUCT_TRAITS, editedProduct.getTraits());
        Assertions.assertEquals(PRODUCT_RECOMMENDED, editedProduct.isRecommended());

        Assertions.assertNull(editedProduct.getImageUrl());
    }


    @Test
    public void editLayout_success_returnBaseArrayResponse() throws JsonProcessingException {

        ValidatableResponse validatableResponse = RestAssured.given().contentType(ContentType.JSON).header("Authorization",
                "Bearer " + adminJwtToken).body(tableRequest).put("/api/admin/layout/tables").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseArrayResponse.class,TableDTO.class);

        BaseArrayResponse<TableDTO> response = objectMapper.readValue(validatableResponse.extract().asString(),type);

        Assertions.assertEquals(TABLE_NO, response.getData().get(0).getTableId());
        Assertions.assertEquals(X, response.getData().get(0).getX());
        Assertions.assertEquals(Y, response.getData().get(0).getY());

        tableRequest = new ArrayList<>();
        tableRequest.add(TableDTO.builder()
            .tableId(TABLE_NO)
            .x(X_2)
            .y(Y)
            .build());

        validatableResponse = RestAssured.given().contentType(ContentType.JSON).header("Authorization",
            "Bearer " + adminJwtToken).body(tableRequest).put("/api/admin/layout/tables").then();
        response = objectMapper.readValue(validatableResponse.extract().asString(),type);

        Assertions.assertEquals(TABLE_NO, response.getData().get(0).getTableId());
        Assertions.assertEquals(X_2, response.getData().get(0).getX());
        Assertions.assertEquals(Y, response.getData().get(0).getY());
    }

    @Test
    public void editLayout_success_deletedTable_returnBaseArrayResponse() throws JsonProcessingException {

        // Set initTableId is 3
        tableRequest.get(0).setTableId(3);
        ValidatableResponse initTable = RestAssured.given().contentType(ContentType.JSON).header("Authorization",
                "Bearer " + adminJwtToken).body(tableRequest).put("/api/admin/layout/tables").then();
        //
        reservationRequest.setTableId(3);
        ValidatableResponse initReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        //Update New layout last tableId is 2
        tableRequest.get(0).setTableId(TABLE_NO);
        ValidatableResponse validatableResponse = RestAssured.given().contentType(ContentType.JSON).header("Authorization",
                "Bearer " + adminJwtToken).body(tableRequest).put("/api/admin/layout/tables").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseArrayResponse.class,TableDTO.class);
        BaseArrayResponse<TableDTO> response = objectMapper.readValue(validatableResponse.extract().asString(),type);
        Reservation reservation = reservationRepository.findByReservationId(1).orElse(null);

        Assertions.assertEquals(TABLE_NO, response.getData().get(0).getTableId());
        Assertions.assertEquals(X, response.getData().get(0).getX());
        Assertions.assertEquals(Y, response.getData().get(0).getY());
        Assertions.assertEquals(Reservation.Status.DECLINED, reservation.getStatus());
    }

    @Test
    public void editLayout_failed_userIsNotAdmin_returnErrorResponse() throws JsonProcessingException {

        ValidatableResponse validatableResponse = RestAssured.given().contentType(ContentType.JSON).header("Authorization",
                "Bearer " + userJwtToken).body(tableRequest).put("/api/admin/layout/tables").then();

        ErrorResponse response = objectMapper.readValue(validatableResponse.extract().asString(),
                ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, response.getCode());
    }

    @Test // PR
    public void editLayout_failed_duplicateId_returnErrorResponse() throws JsonProcessingException {

       tableRequest.add(TableDTO.builder()
               .tableId(TABLE_NO)
               .x(X)
               .y(Y)
               .build());

        ValidatableResponse validatableResponse = RestAssured.given().contentType(ContentType.JSON).header("Authorization",
                "Bearer " + adminJwtToken).body(tableRequest).put("/api/admin/layout/tables").then();

        ErrorResponse response = objectMapper.readValue(validatableResponse.extract().asString(),ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.getCode());
    }

    @Test
    public void getLayout_success_returnBaseArrayResponse() throws JsonProcessingException {


        List<Table> request  = new ArrayList<>();
        request.add(Table.builder().x(X).y(Y).tableId(TABLE_NO).build());
        tableRepository.saveAll(request);

        ValidatableResponse initReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        Reservation reservation = reservationRepository.findByReservationId(1).orElse(null);
        reservation.setStatus(Reservation.Status.APPROVED);
        reservationRepository.save(reservation);

        ValidatableResponse validatableResponse = RestAssured.given().header("Authorization",
                "Bearer " + adminJwtToken).get("/api/admin/layout/tables").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseArrayResponse.class,TableResponse.class);

        BaseArrayResponse<TableResponse> response = objectMapper.readValue(validatableResponse.extract().asString(),type);

        Assertions.assertEquals(X, response.getData().get(0).getTable().getX());
        Assertions.assertTrue(response.getData().get(0).isBooked());
    }

    @Test
    public void getAllReservation_success_returnPaginatedResponse() throws JsonProcessingException {

        Category category = Category.builder()
                .categoryId(1)
                .name("Test")
                .build();
        categoryRepository.save(category);

        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .category(category)
                .build();
        productRepository.save(product);

        List<OrderRequest> orderRequests = new ArrayList<>();
        orderRequests.add(OrderRequest.builder().productId(1).quantity(1).build());

        reservationRequest.setOrders(orderRequests);


        ValidatableResponse addTable = RestAssured.given().contentType(ContentType.JSON).header("Authorization",
                "Bearer " + adminJwtToken).body(tableRequest).put("/api/admin/layout/tables").then();

        ValidatableResponse addReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON).header("Authorization","Bearer " + adminJwtToken)
                        .queryParam("page",1).queryParam("timestamp",TIMESTAMP.toEpochMilli()).queryParam("status","PENDING").get(
                                "/api/admin" +
                        "/reservation" +
                        "/reservations").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(PaginatedResponse.class,
                ReservationResponse.class);

        PaginatedResponse<ReservationResponse> response =
                objectMapper.readValue(validatableResponse.extract().asString(),type);

        Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());
        Assertions.assertEquals(TIMESTAMP.toEpochMilli(), response.getData().get(0).getTimestamp());
        Assertions.assertEquals(TABLE_NO, response.getData().get(0).getTableId());

        validatableResponse =
            RestAssured.given().contentType(ContentType.JSON)
                .header("Authorization","Bearer " + adminJwtToken)
                .queryParam("page",1)
                .get("/api/admin/reservation/reservations")
                .then();

        response = objectMapper.readValue(validatableResponse.extract().asString(),type);

        Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());
        Assertions.assertEquals(TIMESTAMP.toEpochMilli(), response.getData().get(0).getTimestamp());
        Assertions.assertEquals(TABLE_NO, response.getData().get(0).getTableId());

        validatableResponse =
            RestAssured.given().contentType(ContentType.JSON)
                .header("Authorization","Bearer " + adminJwtToken)
                .queryParam("page",1)
                .queryParam("timestamp",TIMESTAMP.toEpochMilli())
                .get("/api/admin/reservation/reservations")
                .then();

        response = objectMapper.readValue(validatableResponse.extract().asString(),type);

        Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());
        Assertions.assertEquals(TIMESTAMP.toEpochMilli(), response.getData().get(0).getTimestamp());
        Assertions.assertEquals(TABLE_NO, response.getData().get(0).getTableId());

        validatableResponse =
            RestAssured.given().contentType(ContentType.JSON)
                .header("Authorization","Bearer " + adminJwtToken)
                .queryParam("page",1)
                .queryParam("status","PENDING")
                .get("/api/admin/reservation/reservations")
                .then();

        response = objectMapper.readValue(validatableResponse.extract().asString(),type);

        Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());
        Assertions.assertEquals(TIMESTAMP.toEpochMilli(), response.getData().get(0).getTimestamp());
        Assertions.assertEquals(TABLE_NO, response.getData().get(0).getTableId());
    }

    @Test
    public void editReservationStatus_success_returnBaseResponse() throws JsonProcessingException {

        List<Table> request  = new ArrayList<>();
        request.add(Table.builder().x(X).y(Y).tableId(TABLE_NO).build());
        request.add(Table.builder().x(X_2).y(Y_2).tableId(TABLE_NO_2).build());
        tableRepository.saveAll(request);

        // Reservation to edit status
        ValidatableResponse addReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();
        JavaType reservationType = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, ReservationResponse.class);
        BaseResponse<ReservationResponse> reservation = objectMapper.readValue(addReservation.extract().asString(), reservationType);

        // Reservation that gets declined because of same timestamp
        ValidatableResponse addReservation2 =
            RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                "Bearer " + userJwtToken).post("/api/reservation/reservations").then();
        BaseResponse<ReservationResponse> reservation2 = objectMapper.readValue(addReservation2.extract().asString(), reservationType);

        // Reservation that is not affected because of different table ID
        reservationRequest.setTableId(TABLE_NO_2);
        ValidatableResponse addReservation3 =
            RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                "Bearer " + userJwtToken).post("/api/reservation/reservations").then();
        BaseResponse<ReservationResponse> reservation3 = objectMapper.readValue(addReservation3.extract().asString(), reservationType);

        Reservation res = reservationRepository.findByReservationId(reservation.getData().getId()).orElse(null);
        Assertions.assertNotNull(res);
        Assertions.assertEquals(Reservation.Status.PENDING, res.getStatus());
        Reservation res2 = reservationRepository.findByReservationId(reservation2.getData().getId()).orElse(null);
        Assertions.assertNotNull(res2);
        Assertions.assertEquals(Reservation.Status.PENDING, res2.getStatus());
        Reservation res3 = reservationRepository.findByReservationId(reservation3.getData().getId()).orElse(null);
        Assertions.assertNotNull(res3);
        Assertions.assertEquals(Reservation.Status.PENDING, res3.getStatus());

        EditReservationStatusRequest editReservationStatusRequest =
                EditReservationStatusRequest.builder().status(Reservation.Status.APPROVED).build();

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON).header("Authorization","Bearer " + adminJwtToken)
                        .body(editReservationStatusRequest).put("/api/admin/reservation/reservations/1").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, Object.class);
        BaseResponse<Object> response = objectMapper.readValue(validatableResponse.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());

        res = reservationRepository.findByReservationId(reservation.getData().getId()).orElse(null);
        Assertions.assertNotNull(res);
        Assertions.assertEquals(Reservation.Status.APPROVED, res.getStatus());
        res2 = reservationRepository.findByReservationId(reservation2.getData().getId()).orElse(null);
        Assertions.assertNotNull(res2);
        Assertions.assertEquals(Reservation.Status.DECLINED, res2.getStatus());
        res3 = reservationRepository.findByReservationId(reservation3.getData().getId()).orElse(null);
        Assertions.assertNotNull(res3);
        Assertions.assertEquals(Reservation.Status.PENDING, res3.getStatus());

        editReservationStatusRequest.setStatus(Reservation.Status.DINING);

        validatableResponse =
            RestAssured.given().contentType(ContentType.JSON).header("Authorization","Bearer " + adminJwtToken)
                .body(editReservationStatusRequest).put("/api/admin/reservation/reservations/1").then();

        response = objectMapper.readValue(validatableResponse.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());

        res = reservationRepository.findByReservationId(reservation.getData().getId()).orElse(null);
        Assertions.assertNotNull(res);
        Assertions.assertEquals(Reservation.Status.DINING, res.getStatus());

        editReservationStatusRequest.setStatus(Reservation.Status.FINISHED);
        validatableResponse =
            RestAssured.given().contentType(ContentType.JSON).header("Authorization","Bearer " + adminJwtToken)
                .body(editReservationStatusRequest).put("/api/admin/reservation/reservations/1").then();
    }

    @Test
    public void editReservationStatus_failed_userIsNotAdmin_returnBaseResponse() throws JsonProcessingException {

        List<Table> request  = new ArrayList<>();
        request.add(Table.builder().x(X).y(Y).tableId(TABLE_NO).build());
        tableRepository.saveAll(request);

        ValidatableResponse addReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        EditReservationStatusRequest editReservationStatusRequest =
                EditReservationStatusRequest.builder().status(Reservation.Status.APPROVED).build();

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON).header("Authorization","Bearer " + userJwtToken)
                        .body(editReservationStatusRequest).put("/api/admin/reservation/reservations/1").then();

        ErrorResponse response = objectMapper.readValue(validatableResponse.extract().asString(),
                ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN,response.getCode());
    }


    @Test
    public void editOrderRequest_success_returnBaseResponse() throws JsonProcessingException {

        Category category = Category.builder()
                .categoryId(1)
                .name("Test")
                .build();
        categoryRepository.save(category);

        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .category(category)
                .build();
        productRepository.save(product);

        List<OrderRequest> orderRequests = new ArrayList<>();
        orderRequests.add(OrderRequest.builder().productId(1).quantity(1).build());

        List<Table> request  = new ArrayList<>();
        request.add(Table.builder().x(X).y(Y).tableId(TABLE_NO).build());
        tableRepository.saveAll(request);


        reservationRequest.setOrders(orderRequests);

        ValidatableResponse addReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();

        EditOrderStatusRequest editOrderStatusRequest =
                EditOrderStatusRequest.builder().productIds(Arrays.asList(1L)).build();

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON).header("Authorization","Bearer " + adminJwtToken)
                        .body(editOrderStatusRequest).put("/api/admin/reservation/reservations/1/orders").then();

        Reservation reservation =
                reservationRepository.findByReservationId(1).orElseThrow(() -> new ObjectNotFoundException(Reservation.class));

        BaseResponse response = objectMapper.readValue(validatableResponse.extract().asString(),
                BaseResponse.class);

        Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());
    }

    @Test
    public void editOrderRequest_failed_userIsNotAdmin_returnBaseResponse() throws JsonProcessingException {

        Category category = Category.builder()
                .categoryId(1)
                .name("Test")
                .build();
        categoryRepository.save(category);

        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .recommended(PRODUCT_RECOMMENDED)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .category(category)
                .build();
        productRepository.save(product);

        List<Table> request  = new ArrayList<>();
        request.add(Table.builder().x(X).y(Y).tableId(TABLE_NO).build());
        tableRepository.saveAll(request);

        List<OrderRequest> orderRequests = new ArrayList<>();
        orderRequests.add(OrderRequest.builder().productId(1).quantity(1).build());
        reservationRequest.setOrders(orderRequests);

        ValidatableResponse addReservation =
                RestAssured.given().contentType(ContentType.JSON).body(reservationRequest).header("Authorization" ,
                        "Bearer " + userJwtToken).post("/api/reservation/reservations").then();



        EditOrderStatusRequest editOrderStatusRequest =
                EditOrderStatusRequest.builder().productIds(Arrays.asList(1L)).build();

        ValidatableResponse validatableResponse =
                RestAssured.given().contentType(ContentType.JSON).header("Authorization","Bearer " + userJwtToken)
                        .body(editOrderStatusRequest).put("/api/admin/reservation/reservations/1/orders").then();

        Reservation reservation =
                reservationRepository.findByReservationId(1).orElseThrow(() -> new ObjectNotFoundException(Reservation.class));


        ErrorResponse response = objectMapper.readValue(validatableResponse.extract().asString(),
                ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN,response.getCode());
    }

    @Test
    public void getDailyReport_success_returnBaseResponse() throws JsonProcessingException {
        Instant date = ZonedDateTime.now(ZoneId.of(timezone))
                .with(ChronoField.HOUR_OF_DAY, 10)
                .with(ChronoField.MINUTE_OF_HOUR, 0)
                .with(ChronoField.SECOND_OF_MINUTE, 0)
                .toInstant();
        Instant testDate = date.minus(1, ChronoUnit.HOURS);

        Category category = Category.builder()
                .categoryId(1)
                .name(CATEGORY_NAME)
                .build();
        categoryRepository.save(category);

        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .imageUrl(null)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .recommended(PRODUCT_RECOMMENDED)
                .category(category)
                .build();
        productRepository.save(product);

        Table table = Table.builder()
                .tableId(1)
                .x(0)
                .y(0)
                .build();
        tableRepository.save(table);

        User user = userRepository.findByEmail(ADMIN_EMAIL).orElse(null);
        Assertions.assertNotNull(user);
        Reservation reservation = Reservation.builder()
                .reservationId(1)
                .table(table)
                .timestamp(date)
                .product(new OrderedProduct(product, 12, false))
                .user(user)
                .status(Reservation.Status.FINISHED)
                .build();
        reservationRepository.save(reservation);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", adminJwtToken)
                .param("timestamp", testDate.toEpochMilli())
                .get("/api/admin/report/reports").then();

        JavaType type = objectMapper.getTypeFactory().constructParametricType(BaseResponse.class, ReportResponse.class);
        BaseResponse<ReportResponse> reportResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, reportResponse.getCode());
        Assertions.assertEquals(PRODUCT_PRICE * 12, reportResponse.getData().getTotal());

        response = RestAssured.given().contentType(ContentType.JSON)
            .cookie("jwtToken", adminJwtToken)
            .get("/api/admin/report/reports").then();

        reportResponse = objectMapper.readValue(response.extract().asString(), type);

        Assertions.assertEquals(HttpStatus.SC_OK, reportResponse.getCode());
        Assertions.assertEquals(PRODUCT_PRICE * 12, reportResponse.getData().getTotal());
    }

    @Test
    public void getDailyReport_failed_userIsNotAdmin() throws JsonProcessingException {
        Instant date = ZonedDateTime.now(ZoneId.of(timezone))
                .with(ChronoField.HOUR_OF_DAY, 10)
                .with(ChronoField.MINUTE_OF_HOUR, 0)
                .with(ChronoField.SECOND_OF_MINUTE, 0)
                .toInstant().plus(1, ChronoUnit.DAYS);
        Instant testDate = date.minus(1, ChronoUnit.HOURS);

        Category category = Category.builder()
                .categoryId(1)
                .name(CATEGORY_NAME)
                .build();
        categoryRepository.save(category);

        Product product = Product.builder()
                .productId(1)
                .name(PRODUCT_NAME)
                .description(PRODUCT_DESCRIPTION)
                .price(PRODUCT_PRICE)
                .imageUrl(null)
                .purchaseCount(0)
                .traits(PRODUCT_TRAITS)
                .recommended(PRODUCT_RECOMMENDED)
                .category(category)
                .build();
        productRepository.save(product);

        Table table = Table.builder()
                .tableId(1)
                .x(0)
                .y(0)
                .build();
        tableRepository.save(table);

        User user = userRepository.findByEmail(ADMIN_EMAIL).orElse(null);
        Assertions.assertNotNull(user);
        Reservation reservation = Reservation.builder()
                .reservationId(1)
                .table(table)
                .timestamp(date)
                .product(new OrderedProduct(product, 12, false))
                .user(user)
                .status(Reservation.Status.FINISHED)
                .build();
        reservationRepository.save(reservation);

        ValidatableResponse response = RestAssured.given().contentType(ContentType.JSON)
                .cookie("jwtToken", userJwtToken)
                .param("timestamp", testDate.toEpochMilli())
                .get("/api/admin/report/reports").then();

        ErrorResponse errorResponse = objectMapper.readValue(response.extract().asString(), ErrorResponse.class);

        Assertions.assertEquals(HttpStatus.SC_FORBIDDEN, errorResponse.getCode());

        boolean hasUnauthorizedAccessException = errorResponse.getErrors().stream().anyMatch(exceptionResponse -> exceptionResponse.getError().equals(UnauthorizedAccessException.class.getSimpleName()));
        Assertions.assertTrue(hasUnauthorizedAccessException);
    }

}
