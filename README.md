# XY Resto Manager - Backend

## API Specifications

- [Authentication](docs/authentication.md)
- [Banner](docs/banner.md)
- [Cart](docs/cart.md)
- [Category](docs/category.md)
- [Layout](docs/layout.md)
- [Product](docs/product.md)
- [Report](docs/report.md)
- [Reservation](docs/reservation.md)
- [User](docs/user.md)
